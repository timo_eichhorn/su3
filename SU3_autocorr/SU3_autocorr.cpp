// Pure SU(3) theory
// Debug Flags: -DDEBUG_MODE_TERMINAL, -DFIXED_SEED

// #define EIGEN_USE_MKL_ALL

// Non-standard library headers
#include "metadynamics.hpp"
#include "pcg_random.hpp"
#include <unsupported/Eigen/MatrixFunctions>
#include <Eigen/Dense>
//----------------------------------------
// Standard library headers
#include <omp.h>
//----------------------------------------
// Standard C++ headers
#include <algorithm>
#include <array>
#include <chrono>
#include <complex>
// #include <experimental/iterator>
#include <filesystem>
#include <fstream>
// #include <queue>
#include <random>
#include <string>
#include <utility>
#include <vector>
#include <iomanip>
#include <iostream>
#include <iterator>
//----------------------------------------
// Standard C headers
#include <cmath>
#include <ctime>

//-----

// std::string program_version = "SU(3)_version_1.3";

//-----

inline constexpr int Nt {12};
inline constexpr int Nx {12};
inline constexpr int Ny {12};
inline constexpr int Nz {12};

template<typename T>
inline constexpr std::complex<T> i(0, 1);
template<typename T>
inline constexpr T pi {static_cast<T>(3.14159265358979323846L)};

using floatT = double;
// TODO: Set default epsilon for iterative stuff?
// if constexpr(std::is_same(floatT, float)::value)
// {

// }

//-----

std::string LatticeSizeString;                              // Lattice size (string)
int n_run;                                                  // Number of runs
double n_run_inverse;                                       // Inverse number of runs
int expectation_period;                                     // Number of updates between calculation of expectation values
inline constexpr int n_smear {7};                           // Number of smearing steps (total amount of smearing steps is actually 1 + (n_smear - 1) * n_smear_skip)
inline constexpr int n_smear_skip {5};                      // Number of smearing steps to skip between measurements
inline constexpr floatT rho_stout {0.12};                   // Stout smearing parameter
inline constexpr int n_metro {0};                           // Number of Metropolis sweeps per total update sweep
inline constexpr int multi_hit {8};                         // Number of hits per site in Metropolis algorithm
inline constexpr int n_heatbath {1};                        // Number of heatbath sweeps per total update sweep
inline constexpr int n_hmc {0};                             // Number of integration steps per HMC update
inline constexpr int n_orelax {4};                          // Number of overrelaxation sweeps per total update sweep
inline constexpr bool metadynamics_enabled {false};          // Enable metadynamics updates or not
inline constexpr bool metapotential_updated {false};         // If true, update the metapotential with every update, if false, simulate with a static metapotential
inline double metro_norm {1.0};                             // Norm for Metropolis update. CAUTION: Needs to be set to correct value inside Configuration() function
inline double metro_target_acceptance {0.5};                // Target acceptance rate for Metropolis update, values around 50% seem to work well, but TRY OUT!
// inline constexpr double metro_norm {1.0 / (Nt * Nx * Ny * Nz * 4.0 * multi_hit)};
// inline constexpr double or_norm {1.0/(Nt * Nx * Ny * Nz * 4.0 * n_run)};
inline constexpr double full_norm {1.0 / (Nt * Nx * Ny * Nz)};
inline constexpr double spatial_norm {1.0 / (Nx * Ny * Nz)};
double DeltaH;                                              // Energy change during HMC trajectory (declared globally so we can print it independently as observable)
int append;                                                 // Directory name appendix
std::string appendString;                                   // Directory name appendix (string)
std::string directoryname_pre;                              // Directory name (prefix)
std::string directoryname;                                  // Directory name
std::string logfilepath;                                    // Filepath (log)
std::string parameterfilepath;                              // Filepath (parameters)
std::string wilsonfilepath;                                 // Filepath (Wilson loops)
std::string metapotentialfilepath;                          // Filepath (metapotential)
auto start {std::chrono::system_clock::now()};              // Start time
floatT beta;                                                // Coupling
std::string betaString;                                     // Coupling (string)
std::ofstream datalog;                                      // Output stream to save data
std::ofstream wilsonlog;                                    // Output stream to save data (Wilson loops)
//std::random_device randomdevice;                          // Creates random device to seed PRNGs
pcg_extras::seed_seq_from<std::random_device> seed_source;  // Seed source to seed PRNGs
#ifdef FIXED_SEED                                           // PRNG for random coordinates and probabilites
pcg64 generator_rand(1);
#else
pcg64 generator_rand(seed_source);
#endif
std::vector<pcg64> prng_vector;                             // Vector of PRNGs for parallel usage
std::vector<std::normal_distribution<floatT>> ndist_vector; // Vector of normal distributions for parallel usage in HMC
uint_fast64_t acceptance_count     {0};                     // Metropolis acceptance rate for new configurations
uint_fast64_t acceptance_count_or  {0};                     // Overrelaxation acceptance rate
uint_fast64_t acceptance_count_hmc {0};                     // HMC acceptance rate

//-----

using std::cin;
using std::cout;
using std::endl;
using std::to_string;
using std::conj;
using std::array;
using std::vector;

// template<typename T, size_t Nx, size_t Nt>
// using GaugeField = array<array<array<T, 2>, Nx>, Nt>;

// template<typename T>
// using 3x3mat = Eigen::Matrix<std::complex<T>, 3, 3>;

// using 3x3mat_f = Eigen::Matrix<std::complex<floatT>, 3, 3>;
// using 3x3mat_d = Eigen::Matrix<std::complex<double>, 3, 3>;

// template<typename T, size_t Nz, size_t Ny, size_t Nx, size_t Nt>
// using Gl_Lattice = array<array<array<array<array<3x3mat, 4>, Nz>, Ny>, Nx>, Nt>;

// template<typename T, size_t Nz, size_t Ny, size_t Nx, size_t Nt>
// using Spatial_tensor = array<array<array<array<array<array<3x3mat, 3>, 3>, Nz>, Ny>, Nx>, Nt>;

// template<typename T, size_t Nz, size_t Ny, size_t Nx, size_t Nt>
// using Full_tensor = array<array<array<array<array<array<3x3mat, 4>, 4>, Nz>, Ny>, Nx>, Nt>;

// typedef Eigen::Matrix<std::complex<double>, 3, 3> Matrix_SU3_double;
// NxN_matrix is the same type as SUN_matrix, the different names are only meant to distinguish between
// SU(N) group elements and NxN matrices mathematically
using Matrix_2x2     = Eigen::Matrix<std::complex<floatT>, 2, 2>;
using Matrix_SU2     = Matrix_2x2;
using Matrix_3x3     = Eigen::Matrix<std::complex<floatT>, 3, 3>;
using Matrix_SU3     = Matrix_3x3;
using Gl_Lattice     = array<array<array<array<array<Matrix_SU3, 4>, Nz>, Ny>, Nx>, Nt>;
using Local_tensor   = array<array<Matrix_SU3, 4>, 4>;
using Spatial_tensor = array<array<array<array<array<array<Matrix_SU3, 3>, 3>, Nz>, Ny>, Nx>, Nt>;
using Full_tensor    = array<array<array<array<array<array<Matrix_SU3, 4>, 4>, Nz>, Ny>, Nx>, Nt>;

//-----

std::unique_ptr<Gl_Lattice> Gluon         {std::make_unique<Gl_Lattice>()};
std::unique_ptr<Gl_Lattice> Gluonsmeared1 {std::make_unique<Gl_Lattice>()};
std::unique_ptr<Gl_Lattice> Gluonsmeared2 {std::make_unique<Gl_Lattice>()};
std::unique_ptr<Gl_Lattice> Gluonchain    {std::make_unique<Gl_Lattice>()};
std::unique_ptr<Full_tensor> F_tensor     {std::make_unique<Full_tensor>()};
std::unique_ptr<Full_tensor> Q_tensor     {std::make_unique<Full_tensor>()};

//-----
// Better complex numbers?

// Trick to allow type promotion below
template <typename T>
struct identity_t { typedef T type; };

// Make working with std::complex<> numbers suck less... allow promotion.
#define COMPLEX_OPS(OP)                                                 \
  template <typename _Tp>                                               \
  std::complex<_Tp>                                                     \
  operator OP(std::complex<_Tp> lhs, const typename identity_t<_Tp>::type & rhs) \
  {                                                                     \
    return lhs OP rhs;                                                  \
  }                                                                     \
  template <typename _Tp>                                               \
  std::complex<_Tp>                                                     \
  operator OP(const typename identity_t<_Tp>::type & lhs, const std::complex<_Tp> & rhs) \
  {                                                                     \
    return lhs OP rhs;                                                  \
  }
COMPLEX_OPS(+)
COMPLEX_OPS(-)
COMPLEX_OPS(*)
COMPLEX_OPS(/)
#undef COMPLEX_OPS

// For some reason there is no inbuilt sign function???

template <typename T>
[[nodiscard]]
constexpr int sign(T x)
{
    return (x > T(0)) - (x < T(0));
}

//-----
// Overload << for vectors and arrays?

// template <typename T>
// std::ostream& operator<<(ostream& out, const std::vector<T>& container)
// {
//     out << "Container dump begins: ";
//     std::copy(container.cbegin(), container.cend(), std::ostream_iterator<T>(out, " "));
//     out << "\n";
//     return out;
// }

// template <typename T>
// std::ostream& operator<<(ostream& out, const std::array<T>& container)
// {
//     out << "Container dump begins: ";
//     std::copy(container.cbegin(), container.cend(), std::ostream_iterator<T>(out, " "));
//     out << "\n";
//     // std::copy(container.cbegin(), std::prev(container.cend()), std::ostream_iterator<T>(correlationlog, ","));
//     // correlationlog << Correlation_Function.back() << "\n";
//     return out;
// }

//-------------------------------------------------------------------------------------

//-----
// Function to get user input with error handling
// TODO: Constrain target to writeable range or something like that

template<typename T>
void ValidatedIn(const std::string& message, T& target)
{
    // Keep count of tries and abort after too many tries (e.g. important when using nohup)
    size_t count {0};
    while (std::cout << message << "\n" && !(std::cin >> target) && count < 10)
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Invalid input.\n";
        ++count;
    }
}

//-----
// Receives simulation parameters from user input

void Configuration()
{
    cout << "\n\nSU(3) theory simulation\n";
    cout << "Current version: " << program_version;
    cout << "\n\n----------------------------------------\n\n";
    // Get simulation parameters from user input
    ValidatedIn("Please enter beta: ", beta);
    ValidatedIn("Please enter n_run: ", n_run);
    ValidatedIn("Please enter expectation_period: ", expectation_period);
    n_run_inverse = 1.0 / static_cast<double>(n_run);
    if (n_metro != 0 && multi_hit != 0)
    {
        metro_norm = 1.0 / (Nt * Nx * Ny * Nz * 4.0 * n_metro * multi_hit);
    }
    cout << "\n" << "Gauge field precision: " << typeid(floatT).name() << "\n";
    cout << "beta is " << beta << ".\n";
    cout << "n_run is " << n_run << " and expectation_period is " << expectation_period << ".\n";
    cout << "n_metro is " << n_metro << ".\n";
    cout << "multi_hit is " << multi_hit << ".\n";
    cout << "metro_target_acceptance is " << metro_target_acceptance << ".\n";
    cout << "n_heatbath is " << n_heatbath << ".\n";
    cout << "n_hmc is " << n_hmc << ".\n";
    cout << "n_orelax is " << n_orelax << ".\n";
    cout << "metadynamics_enabled is " << metadynamics_enabled << ".\n";
    cout << "metapotential_updated is " << metapotential_updated << ".\n"; 
    //cout << "Overall, there are " <<  << " lattice sites.\n\n";
}

//-----
// Writes simulation parameters to files

void SaveParameters(std::string filename, const std::string& starttimestring)
{
    datalog.open(filename, std::fstream::out | std::fstream::app);
    datalog << program_version << "\n";
    datalog << "logfile\n\n";
    #ifdef DEBUG_MODE_TERMINAL
    datalog << "DEBUG_MODE_TERMINAL\n";
    #endif
    datalog << starttimestring << "\n";
    datalog << "START_PARAMS\n";
    datalog << "Gauge field precision = " << typeid(floatT).name() << "\n";
    datalog << "Nt = " << Nt << "\n";
    datalog << "Nx = " << Nx << "\n";
    datalog << "Ny = " << Ny << "\n";
    datalog << "Nz = " << Nz << "\n";
    datalog << "beta = " << beta << "\n";
    datalog << "n_run = " << n_run << "\n";
    datalog << "expectation_period = " << expectation_period << "\n";
    datalog << "n_smear = " << n_smear << "\n";
    datalog << "n_smear_skip = " << n_smear_skip << "\n";
    datalog << "rho_stout = " << rho_stout << "\n";
    datalog << "n_metro = " << n_metro << "\n";
    datalog << "multi_hit = " << multi_hit << "\n";
    datalog << "metro_target_acceptance = " << metro_target_acceptance << "\n";
    datalog << "n_heatbath = " << n_heatbath << "\n";
    datalog << "n_hmc = " << n_hmc << "\n";
    datalog << "n_orelax = " << n_orelax << "\n";
    datalog << "metadynamics_enabled = " << metadynamics_enabled << "\n";
    datalog << "metapotential_updated = " << metapotential_updated << "\n"; 
    datalog << "END_PARAMS\n" << endl;
    datalog.close();
    datalog.clear();
}

//-----
// Creates directories and files to store data

void CreateFiles()
{
    LatticeSizeString = to_string(Nx) + "x" + to_string(Ny) + "x" + to_string(Nz) + "x" + to_string(Nt);
    betaString = to_string(beta);
    directoryname_pre = "SU(3)_N=" + LatticeSizeString + "_beta=" + betaString;
    directoryname = directoryname_pre;
    append = 1;
    while (std::filesystem::exists(directoryname) == true)
    {
        appendString = to_string(append);
        directoryname = directoryname_pre + " (" + appendString + ")";
        ++append;
    }
    std::filesystem::create_directory(directoryname);
    cout << "\n\n" << "Created directory \"" << directoryname << "\".\n";
    logfilepath = directoryname + "/log.txt";
    parameterfilepath = directoryname + "/parameters.txt";
    wilsonfilepath = directoryname + "/wilson.txt";
    metapotentialfilepath = directoryname + "/metapotential.txt";
    cout << "Filepath (log):\t\t" << logfilepath << "\n";
    cout << "Filepath (parameters):\t" << parameterfilepath << "\n";
    cout << "Filepath (wilson):\t" << wilsonfilepath << "\n";
    cout << "Filepath (metadyn):\t" << metapotentialfilepath << "\n";
    #ifdef DEBUG_MODE_TERMINAL
    cout << "DEBUG_MODE_TERMINAL\n\n";
    #endif

    //-----
    // Writes parameters to files

    std::time_t start_time {std::chrono::system_clock::to_time_t(start)};
    std::string start_time_string {std::ctime(&start_time)};

    // logfile

    SaveParameters(logfilepath, start_time_string);

    // parameterfile

    SaveParameters(parameterfilepath, start_time_string);

    // wilsonfile

    SaveParameters(wilsonfilepath, start_time_string);
}

//-----
// Print final parameters to a specified ostream

template<typename floatT>
void PrintFinal(std::ostream& log, const uint_fast64_t acceptance_count, const uint_fast64_t acceptance_count_or, const uint_fast64_t acceptance_count_hmc, const floatT epsilon, const std::time_t& end_time, const std::chrono::duration<double>& elapsed_seconds)
{
    double or_norm {1.0};
    if constexpr(n_orelax != 0)
    {
        or_norm = 1.0 / (Nt * Nx * Ny * Nz * 4.0 * n_run * n_orelax);
    }
    double hmc_norm {1.0};
    if constexpr(n_hmc != 0)
    {
        hmc_norm = 1.0 / n_run;
    }
    log << "Metro target acceptance: " << metro_target_acceptance << "\n";
    log << "Metro acceptance: " << acceptance_count * metro_norm << "\n";
    log << "OR acceptance: " << acceptance_count_or * or_norm << "\n";
    log << "HMC acceptance: " << acceptance_count_hmc * hmc_norm << "\n";
    log << "epsilon: " << epsilon << "\n";
    log << std::ctime(&end_time) << "\n";
    log << "Required time: " << elapsed_seconds.count() << "s\n";
}

//-----
// Initialize multiple instances of PRNGs in vector for parallel usage

// void InitializePRNGs(vector<pcg64>& prng_vector)
// {
//     prng_vector.clear();
//     cout << "Beginning" << endl;
//     cout << "max threads:" << omp_get_max_threads() << endl;
//     array<array<int, 10>, 4> prng_test;
//     // array<int, 100> prng_test;
//     std::uniform_int_distribution<int> distribution_choice(1, 8);
//     for (int i = 0; i < omp_get_max_threads(); ++i)
//     {
//         // prng_vector.emplace_back(generator_rand);
//         pcg_extras::seed_seq_from<std::random_device> seed_source_temp;
//         #ifdef FIXED_SEED
//         pcg64 generator_rand_temp(i);
//         #else
//         pcg64 generator_rand_temp(seed_source_temp);
//         #endif
//         prng_vector.emplace_back(generator_rand_temp);
//     }

//     #pragma omp parallel for
//     for (int i = 0; i < 4; ++i)
//     {
//         pcg64& current_prng = prng_vector[omp_get_thread_num()];
//         for (int j = 0; j < 10; ++j)
//         {
//             // cout << "i: " << i << " j: " << j << endl;
//             prng_test[i][j] = distribution_choice(current_prng);
//         }
//     }
//     for (int i = 0; i < 4; ++i)
//     {
//         for (int j = 0; j < 10; ++j)
//         {
//             cout << "i:" << i << " j:" << j << " " << prng_test[i][j] << endl;
//         }
//     }
//     // cout << prng_vector.size() << endl;
//     // #pragma omp parallel for
//     // for (int i = 0; i < 100; ++i)
//     // {
//     //     pcg64& current_prng = prng_vector[omp_get_thread_num()];
//     //     prng_test[i] = distribution_choice(current_prng);
//     // }

//     // for (int i = 0; i < 100; ++i)
//     // {
//     //     cout << prng_test[i] << endl;
//     // }
// }

[[nodiscard]]
vector<pcg64> CreatePRNGs(const int thread_num = 0)
{
    vector<pcg64> temp_vec;
    #if defined(_OPENMP)
        int max_thread_num {omp_get_max_threads()};
    #else
        int max_thread_num {1};
    #endif
    cout << "Maximum number of threads: " << max_thread_num << endl;
    #if defined(_OPENMP)
        if (thread_num != 0)
        {
            max_thread_num = thread_num;
            omp_set_num_threads(thread_num);
        }
    #endif
    if (max_thread_num != 1)
    {
        cout << "Creating PRNG vector with " << max_thread_num << " PRNGs.\n" << endl;
    }
    else
    {
        cout << "Creating PRNG vector with " << max_thread_num << " PRNG.\n" << endl;
    }
    for (int thread_count = 0; thread_count < max_thread_num; ++thread_count)
    {
        #ifdef FIXED_SEED
        pcg64 generator_rand_temp(thread_count);
        temp_vec.emplace_back(generator_rand_temp);
        // temp_vec.emplace_back(generator_rand_temp(thread_count));
        #else
        pcg_extras::seed_seq_from<std::random_device> seed_source_temp;
        pcg64 generator_rand_temp(seed_source_temp);
        temp_vec.emplace_back(generator_rand_temp);
        // temp_vec.emplace_back(generator_rand_temp(seed_source_temp));
        #endif
    }
    return temp_vec;
}

//-----
// Create vector of normal_distribution generators with mean 0 and standard deviation 1 for HMC

[[nodiscard]]
vector<std::normal_distribution<floatT>> CreateNormal(const int thread_num = 0)
{
    vector<std::normal_distribution<floatT>> temp_vec;
    #if defined(_OPENMP)
        int max_thread_num {omp_get_max_threads()};
    #else
        int max_thread_num {1};
    #endif
    cout << "Maximum number of threads: " << max_thread_num << endl;
    #if defined(_OPENMP)
        if (thread_num != 0)
        {
            max_thread_num = thread_num;
            omp_set_num_threads(thread_num);
        }
    #endif
    if (max_thread_num != 1)
    {
        cout << "Creating vector of normal_distributions with " << max_thread_num << " normal_distributions.\n" << endl;
    }
    else
    {
        cout << "Creating vector of normal_distributions with " << max_thread_num << " normal_distributions.\n" << endl;
    }
    for (int thread_count = 0; thread_count < max_thread_num; ++thread_count)
    {
        std::normal_distribution<floatT> temp_dist{0, 1};
        temp_vec.emplace_back(temp_dist);
    }
    return temp_vec;
}

//-----
// Set gauge fields to identity

void SetGluonToOne(Gl_Lattice& Gluon)
{
    #pragma omp parallel for
    for (auto &ind0 : Gluon)
    for (auto &ind1 : ind0)
    for (auto &ind2 : ind1)
    for (auto &ind3 : ind2)
    for (auto &GluonMatrix : ind3)
    {
        GluonMatrix.setIdentity();
    }
    cout << "Gauge Fields set to identity!" << endl;
}

//-----
// Creates maximally dislocated instanton configuration with charge Q_i

// void InstantonStart(Gl_Lattice& Gluon, const int Q, const int mu, const int nu, const int sigma, const int tau)
void InstantonStart(Gl_Lattice& Gluon, const int Q)
{
    // First set everything to unity
    SetGluonToOne(Gluon);
    // Commuting su(2) matrices embedded into su(3)
    // Generally, any traceless, hermitian matrix works here
    // The condition that the matrices commute enables us to construct charge +/- 1 instantons
    Matrix_3x3 sig;
    sig << 1,  0, 0,
           0, -1, 0,
           0,  0, 0;
    Matrix_3x3 tau;
    tau << 1, 0,  0,
           0, 0,  0,
           0, 0, -1;
    // Unit matrices in sigma and tau subspace 
    Matrix_3x3 id_sig {sig * sig};
    Matrix_3x3 id_tau {tau * tau};
    // Orthogonal complement
    Matrix_3x3 comp_sig {Matrix_3x3::Identity() - id_sig};
    Matrix_3x3 comp_tau {Matrix_3x3::Identity() - id_tau};
    // Consider a two-dimensional slice in the t-x plane
    floatT Field_t {static_cast<floatT>(2.0) * pi<floatT> * std::abs(Q) / (Nt * Nx)};
    floatT Field_x {static_cast<floatT>(2.0) * pi<floatT> * std::abs(Q) / Nx};
    // Assign link values in t-direction
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[t][x][y][z][1] = comp_sig + std::cos(Field_t * t) * id_sig + i<floatT> * std::sin(Field_t * t) * sig;
    }
    // Assign link values on last time-slice in x-direction
    #pragma omp parallel for
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[Nt - 1][x][y][z][0] = comp_sig + std::cos(Field_x * x) * id_sig - i<floatT> * std::sin(Field_x * x) * sig;
    }
    //-----
    // Consider a two-dimensional slice in the y-z plane
    floatT Field_y, Field_z;
    if (Q == 0)
    {
        Field_y = static_cast<floatT>(0.0);
        Field_z = static_cast<floatT>(0.0);
    }
    else
    {
        Field_y = static_cast<floatT>(2.0) * pi<floatT> * Q / (std::abs(Q) * Ny * Nz);
        Field_z = static_cast<floatT>(2.0) * pi<floatT> * Q / (std::abs(Q) * Nz);
    }
    // Assign link values in y-direction
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[t][x][y][z][3] = comp_tau + std::cos(Field_y * y) * id_tau + i<floatT> * std::sin(Field_y * y) * tau;
    }
    // Assign link values on last y-slice in z-direction
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[t][x][Ny - 1][z][2] = comp_tau + std::cos(Field_z * z) * id_tau - i<floatT> * std::sin(Field_z * z) * tau;
    }
}

//-----
// TODO: Still WIP
// Creates a local instanton configuration with charge ?

void LocalInstantonStart(Gl_Lattice& Gluon)
{
    // First set everything to unity
    SetGluonToOne(Gluon);
    // Generators of SU(2)/Pauli matrices embedded into SU(3) (up to negative determinant)
    // Generally, any traceless, hermitian matrix works here
    // The condition that the matrices commute enables us to construct charge +/- 1 instantons
    // TODO: This is incorrect, the factors only apply to the stuff inside SU(2), the last entry is always 1!
    // TODO: Directly initialize?
    Matrix_3x3 sig1;
    sig1 << 0, 1, 0,
            1, 0, 0,
            0, 0, 1;
    Matrix_3x3 sig2;
    sig2 << 0        , -i<floatT>, 0,
            i<floatT>,  0        , 0,
            0        ,  0        , 1;
    Matrix_3x3 sig3;
    sig3 << 1,  0, 0,
            0, -1, 0,
            0,  0, 1;
    // Only links in the elementary hypercube between (0,0,0,0) and (1,1,1,1) take on non-trivial values
    // TODO: See above!
    for (int t = 0; t < 2; ++t)
    for (int x = 0; x < 2; ++x)
    for (int y = 0; y < 2; ++y)
    for (int z = 0; z < 2; ++z)
    {
        int coord_sum {t + x + y + z};
        // We only consider ther 32 links INSIDE the hypercube between (0,0,0,0) and (1,1,1,1)
        // Do not go into mu direction if x_mu != 0
        if (x == 0)
        {
            Gluon[t][x][y][z][1] = i<floatT> * std::pow(-1, coord_sum) * sig1;
            Gluon[t][x][y][z][1](2, 2) = 1.0;
        }
        if (y == 0)
        {
            Gluon[t][x][y][z][2] = i<floatT> * std::pow(-1, coord_sum) * sig2;
            Gluon[t][x][y][z][2](2, 2) = 1.0;
        }
        if (z == 0)
        {
            Gluon[t][x][y][z][3] = i<floatT> * std::pow(-1, coord_sum) * sig3;
            Gluon[t][x][y][z][3](2, 2) = 1.0;
        }
    }
}

//-----
// TODO: Still WIP
// Create BPST instanton embedded into SU(3)

// void BPSTInstantonStart(Gl_Lattice& Gluon)
// {
//     // First set everything to unity
//     SetGluonToOne(Gluon);
//     // Generators of SU(2) embedded into su(3) since we want to exponentiate later
//     // TODO: Manually exponentiate?
//     Matrix_3x3 sig1;
//     sig1 << 0, 1, 0,
//             1, 0, 0,
//             0, 0, 0;
//     Matrix_3x3 sig2;
//     sig2 << 0        , -i<floatT>, 0,
//             i<floatT>,  0        , 0,
//             0        ,  0        , 0;
//     Matrix_3x3 sig3;
//     sig3 << 1,  0, 0,
//             0, -1, 0,
//             0,  0, 0;
//     // Group element
//     // Matrix_SU3 g;
//     // g = t * Matrix_SU3::Identity() + i<floatT> * (x * sig1 + y * sig2 + z * sig3);
//     // Since we have periodic boundaries, we do not directly translate the gauge potential to the lattice
//     // Instead, we first perform a gauge transformation using g(x) to obtain a gauge potential that vanishes
//     // as the distance to the center of the instanton goes towards infinity
//     for (int t = 0; t < Nt; ++t)
//     for (int x = 0; x < Nx; ++x)
//     for (int y = 0; y < Ny; ++y)
//     for (int z = 0; z < Nz; ++z)
//     {
//         floatT r {std::sqrt(t*t + x*x + y*y + z*z)};
//         // Scale factor
//         floatT scale_factor {r * r / (r * r + rho * rho)};
//         Matrix_SU3 g {t * Matrix_SU3::Identity() + i<floatT> * (x * sig1 + y * sig2 + z * sig3)};
//         // Matrix_SU3 prefactor {scale_factor * r * g.inverse()};
//         // Explicitly go through mu = 0, 1, 2, 3
//         Matrix_SU3 g_inv0 {1.0/r * Matrix_SU3::Identity() - t/(r * r * r) * (t * Matrix_SU3::Identity() + i<floatT> * (x * sig1 + y * sig2 + z * sig3))};
//         Matrix_SU3 g_inv1 {i<floatT>/r * sig1             - x/(r * r * r) * (t * Matrix_SU3::Identity() + i<floatT> * (x * sig1 + y * sig2 + z * sig3))};
//         Matrix_SU3 g_inv2 {i<floatT>/r * sig2             - y/(r * r * r) * (t * Matrix_SU3::Identity() + i<floatT> * (x * sig1 + y * sig2 + z * sig3))};
//         Matrix_SU3 g_inv3 {i<floatT>/r * sig3             - z/(r * r * r) * (t * Matrix_SU3::Identity() + i<floatT> * (x * sig1 + y * sig2 + z * sig3))};
//         Gluon[t][x][y][z][0] = scale_factor * g_deriv * g_inv0 - g_deriv * g_inv0;
//         Gluon[t][x][y][z][1] =;
//         Gluon[t][x][y][z][2] =;
//         Gluon[t][x][y][z][3] =;
//         r * g.inverse() * (- x_mu * g)
//     }
// }

//-----
// Generates random 3 x 3 matrices

[[nodiscard]]
Matrix_SU3 RandomSU3(std::uniform_int_distribution<int>& distribution_choice, std::uniform_real_distribution<floatT>& distribution_unitary)
{
    Matrix_SU3 tmp;
    int choice {distribution_choice(generator_rand)};
    floatT phi {distribution_unitary(generator_rand)};

    switch(choice)
    {
        case 1:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, i<floatT> * s_phi, 0.0,
                   i<floatT> * s_phi, c_phi, 0.0,
                   0.0, 0.0, 1.0;
        }
        break;
        case 2:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, -s_phi, 0.0,
                   s_phi, c_phi, 0.0,
                   0.0, 0.0, 1.0;
        }
        break;
        case 3:
        {
            std::complex<floatT> exp_I_phi {std::exp(i<floatT> * phi)};
            tmp << exp_I_phi, 0.0, 0.0,
                   0.0, conj(exp_I_phi), 0.0,
                   0.0, 0.0, 1.0;
        }
        break;
        case 4:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, 0.0, i<floatT> * s_phi,
                   0.0, 1.0, 0.0,
                   i<floatT> * s_phi, 0.0, c_phi;
        }
        break;
        case 5:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, 0.0, s_phi,
                   0.0, 1.0, 0.0,
                   -s_phi, 0.0, c_phi;
        }
        break;
        case 6:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << 1.0, 0.0, 0.0,
                   0.0, c_phi, i<floatT> * s_phi,
                   0.0, i<floatT> * s_phi, c_phi;
        }
        break;
        case 7:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << 1.0, 0.0, 0.0,
                   0.0, c_phi, -s_phi,
                   0.0, s_phi, c_phi;
        }
        break;
        case 8:
        {
            floatT phi_tilde {phi / static_cast<floatT>(std::sqrt(3))};
            std::complex<floatT> exp_phi_tilde {std::exp(i<floatT> * phi_tilde)};
            tmp << exp_phi_tilde, 0.0, 0.0,
                   0.0, exp_phi_tilde, 0.0,
                   // 0.0, 0.0, (std::complex<floatT>(1.0, 0))/(exp_phi_tilde * exp_phi_tilde);
                   // 0.0, 0.0, 1.0/(exp_phi_tilde * exp_phi_tilde);
                   0.0, 0.0, 1.0/(exp_phi_tilde * exp_phi_tilde);
        }
        break;
    }
    return tmp;
}

[[nodiscard]]
Matrix_SU3 RandomSU3Parallel(const int choice, const floatT phi)
{
    Matrix_SU3 tmp;

    switch(choice)
    {
        case 1:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, i<floatT> * s_phi, 0.0,
                   i<floatT> * s_phi, c_phi, 0.0,
                   0.0, 0.0, 1.0;
        }
        break;
        case 2:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, -s_phi, 0.0,
                   s_phi, c_phi, 0.0,
                   0.0, 0.0, 1.0;
        }
        break;
        case 3:
        {
            std::complex<floatT> exp_I_phi {std::exp(i<floatT> * phi)};
            tmp << exp_I_phi, 0.0, 0.0,
                   0.0, conj(exp_I_phi), 0.0,
                   0.0, 0.0, 1.0;
        }
        break;
        case 4:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, 0.0, i<floatT> * s_phi,
                   0.0, 1.0, 0.0,
                   i<floatT> * s_phi, 0.0, c_phi;
        }
        break;
        case 5:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << c_phi, 0.0, s_phi,
                   0.0, 1.0, 0.0,
                   -s_phi, 0.0, c_phi;
        }
        break;
        case 6:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << 1.0, 0.0, 0.0,
                   0.0, c_phi, i<floatT> * s_phi,
                   0.0, i<floatT> * s_phi, c_phi;
        }
        break;
        case 7:
        {
            floatT s_phi {std::sin(phi)};
            floatT c_phi {std::cos(phi)};
            tmp << 1.0, 0.0, 0.0,
                   0.0, c_phi, -s_phi,
                   0.0, s_phi, c_phi;
        }
        break;
        case 8:
        {
            floatT phi_tilde {phi / static_cast<floatT>(std::sqrt(3))};
            std::complex<floatT> exp_phi_tilde {std::exp(i<floatT> * phi_tilde)};
            tmp << exp_phi_tilde, 0.0, 0.0,
                   0.0, exp_phi_tilde, 0.0,
                   0.0, 0.0, 1.0/(exp_phi_tilde * exp_phi_tilde);
        }
        break;
    }
    return tmp;
}

//-----
// Calculates staple at given coordinates
// Compared to the definition used by Gattringer & Lang, this version is the adjoint

[[nodiscard]]
Matrix_3x3 Staple(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu) noexcept
{
    Matrix_3x3 st;

    // int tp = (t + 1)%Nt;
    // int tm = (t - 1 + Nt)%Nt;

    // int xp = (x + 1)%Nx;
    // int xm = (x - 1 + Nx)%Nx;

    // int yp = (y + 1)%Ny;
    // int ym = (y - 1 + Ny)%Ny;

    // int zp = (z + 1)%Nz;
    // int zm = (z - 1 + Nz)%Nz;

    switch(mu)
    {
        case 0:
        {
            int tp {(t + 1)%Nt};
            int xp {(x + 1)%Nx};
            int xm {(x - 1 + Nx)%Nx};
            int yp {(y + 1)%Ny};
            int ym {(y - 1 + Ny)%Ny};
            int zp {(z + 1)%Nz};
            int zm {(z - 1 + Nz)%Nz};
            st.noalias() = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][0] * Gluon[tp][x][y][z][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1]
                         + Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][0] * Gluon[tp][x][y][z][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2]
                         + Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][0] * Gluon[tp][x][y][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3];
            // st.noalias() = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][0] * Gluon[tp][x][y][z][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1];
            // st.noalias() += Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][0] * Gluon[tp][x][y][z][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2];
            // st.noalias() += Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][0] * Gluon[tp][x][y][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3];
        }
        break;

        case 1:
        {
            int tp {(t + 1)%Nt};
            int tm {(t - 1 + Nt)%Nt};
            int xp {(x + 1)%Nx};
            int yp {(y + 1)%Ny};
            int ym {(y - 1 + Ny)%Ny};
            int zp {(z + 1)%Nz};
            int zm {(z - 1 + Nz)%Nz};
            st.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][1] * Gluon[tm][xp][y][z][0]
                         + Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][1] * Gluon[t][xp][y][z][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2]
                         + Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][1] * Gluon[t][xp][y][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3];
            // st.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][1] * Gluon[tm][xp][y][z][0];
            // st.noalias() += Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][1] * Gluon[t][xp][y][z][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2];
            // st.noalias() += Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][1] * Gluon[t][xp][y][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3];
        }
        break;

        case 2:
        {
            int tp {(t + 1)%Nt};
            int tm {(t - 1 + Nt)%Nt};
            int xp {(x + 1)%Nx};
            int xm {(x - 1 + Nx)%Nx};
            int yp {(y + 1)%Ny};
            int zp {(z + 1)%Nz};
            int zm {(z - 1 + Nz)%Nz};
            st.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][2] * Gluon[tm][x][yp][z][0]
                         + Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][2] * Gluon[t][xm][yp][z][1]
                         + Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][2] * Gluon[t][x][yp][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3];
            // st.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][2] * Gluon[tm][x][yp][z][0];
            // st.noalias() += Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][2] * Gluon[t][xm][yp][z][1];
            // st.noalias() += Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][2] * Gluon[t][x][yp][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3];
        }
        break;

        case 3:
        {
            int tp {(t + 1)%Nt};
            int tm {(t - 1 + Nt)%Nt};
            int xp {(x + 1)%Nx};
            int xm {(x - 1 + Nx)%Nx};
            int yp {(y + 1)%Ny};
            int ym {(y - 1 + Ny)%Ny};
            int zp {(z + 1)%Nz};
            st.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][3] * Gluon[tm][x][y][zp][0]
                         + Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][3] * Gluon[t][xm][y][zp][1]
                         + Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][3] * Gluon[t][x][ym][zp][2];
            // st.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][3] * Gluon[tm][x][y][zp][0];
            // st.noalias() += Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][3] * Gluon[t][xm][y][zp][1];
            // st.noalias() += Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][3] * Gluon[t][x][ym][zp][2];
        }
        break;
    }
    return st;
}

//-----

// TODO: Does __restrict__ help in any way?
// floatT SLocal(const Matrix_SU3&__restrict__ U, const Matrix_SU3&__restrict__ st)

[[nodiscard]]
floatT SLocal(const Matrix_SU3& U, const Matrix_3x3& st)
{
    // return beta/3.0 * std::real((Matrix_SU3::Identity() - U * st.adjoint()).trace());
    return beta/static_cast<floatT>(3.0) * (static_cast<floatT>(3.0) - std::real((U * st.adjoint()).trace()));
}

// TODO: Does this help in any way?
// [[nodiscard]]
// floatT SLocalDiff(const Matrix_3x3& Udiff, const Matrix_3x3& st)
// {
//     return -beta/static_cast<floatT>(3.0) * std::real((Udiff * st.adjoint()).trace());
// }

//-----
// Calculates plaquette at given coordinates
// TODO: Rewrite this? Is this even correct? Why does it return a zero matrix for nu = 0?

[[nodiscard]]
Matrix_SU3 Plaquette(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu, const int nu)
{
    Matrix_SU3 pl;

    int tp {(t + 1)%Nt};
    int xp {(x + 1)%Nx};
    int yp {(y + 1)%Ny};
    int zp {(z + 1)%Nz};

    switch(nu)
    {
        case 0:
        {
            pl << 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0;
        }
        break;

        case 1:
        {
            pl.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint();
        }
        break;

        case 2:
        {
            if (mu == 0)
            {
                pl.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint();
            }
            if (mu == 1)
            {
                pl.noalias() = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint();
            }
        }
        break;

        case 3:
        {
            if (mu == 0)
            {
                pl.noalias() = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint();
            }
            if (mu == 1)
            {
                pl.noalias() = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint();
            }
            if (mu == 2)
            {
                pl.noalias() = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint();
            }
        }
        break;
    }
    return pl;
}

//-----
// Polyakov loops

[[nodiscard]]
std::complex<double> PolyakovLoop(const Gl_Lattice& Gluon) noexcept
{
    std::complex<floatT> P {0.0, 0.0};
    // #pragma omp parallel for reduction(+:P)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        // std::complex<floatT> tmp {1.0, 0.0};
        Matrix_SU3 tmp {Matrix_SU3::Identity()};
        for (int t = 0; t < Nt; ++t)
        {
            tmp *= Gluon[t][x][y][z][0];
        }
        P += tmp.trace();
    }
    return P * spatial_norm;
}

//-----
// Square Wilson loops of length Nmu
// TODO: Does it make sense to precompute Gluonchain, or is it more efficient to locally calculate the terms?
//       For instance, we could left multiply with the adjoint/inverse and right multiply with a new link, which
//       might be computationally advantageous for larger chain lengths (only 2 multiplications instead of N)

template<int Nmu_start, int Nmu_end, bool reset>
[[nodiscard]]
double WilsonLoop(const Gl_Lattice& Gluon, Gl_Lattice& Gluonchain) noexcept
{
    double W {0.0};
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        if constexpr(reset)
        {
            for (int mu = 0; mu < 4; ++mu)
            {
                Gluonchain[t][x][y][z][mu].setIdentity();
            }
        }
        for (int n = Nmu_start; n < Nmu_end; ++n)
        {
            Gluonchain[t][x][y][z][0] *= Gluon[(t + n)%Nt][x][y][z][0];
            Gluonchain[t][x][y][z][1] *= Gluon[t][(x + n)%Nx][y][z][1];
            Gluonchain[t][x][y][z][2] *= Gluon[t][x][(y + n)%Ny][z][2];
            Gluonchain[t][x][y][z][3] *= Gluon[t][x][y][(z + n)%Nz][3];
        }
    }
    #pragma omp parallel for reduction(+:W)
    for (int t = 0; t < Nt; ++t)
    {
        int tp {(t + Nmu_end)%Nt};
        for (int x = 0; x < Nx; ++x)
        {
            int xp {(x + Nmu_end)%Nx};
            for (int y = 0; y < Ny; ++y)
            {
                int yp {(y + Nmu_end)%Ny};
                for (int z = 0; z < Nz; ++z)
                {
                    int zp {(z + Nmu_end)%Nz};
                    // W += (Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][1] * Gluonchain[t][xp][y][z][0].adjoint() * Gluonchain[t][x][y][z][1].adjoint()).cast<std::complex<double>>();
                    // W += (Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][2] * Gluonchain[t][x][yp][z][0].adjoint() * Gluonchain[t][x][y][z][2].adjoint()).cast<std::complex<double>>();
                    // W += (Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][3] * Gluonchain[t][x][y][zp][0].adjoint() * Gluonchain[t][x][y][z][3].adjoint()).cast<std::complex<double>>();
                    // W += (Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][2] * Gluonchain[t][x][yp][z][1].adjoint() * Gluonchain[t][x][y][z][2].adjoint()).cast<std::complex<double>>();
                    // W += (Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][3] * Gluonchain[t][x][y][zp][1].adjoint() * Gluonchain[t][x][y][z][3].adjoint()).cast<std::complex<double>>();
                    // W += (Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][3] * Gluonchain[t][x][y][zp][2].adjoint() * Gluonchain[t][x][y][z][3].adjoint()).cast<std::complex<double>>();

                    W += std::real((Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][1] * Gluonchain[t][xp][y][z][0].adjoint() * Gluonchain[t][x][y][z][1].adjoint()).trace());
                    W += std::real((Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][2] * Gluonchain[t][x][yp][z][0].adjoint() * Gluonchain[t][x][y][z][2].adjoint()).trace());
                    W += std::real((Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][3] * Gluonchain[t][x][y][zp][0].adjoint() * Gluonchain[t][x][y][z][3].adjoint()).trace());
                    W += std::real((Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][2] * Gluonchain[t][x][yp][z][1].adjoint() * Gluonchain[t][x][y][z][2].adjoint()).trace());
                    W += std::real((Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][3] * Gluonchain[t][x][y][zp][1].adjoint() * Gluonchain[t][x][y][z][3].adjoint()).trace());
                    W += std::real((Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][3] * Gluonchain[t][x][y][zp][2].adjoint() * Gluonchain[t][x][y][z][3].adjoint()).trace());

                    // W += std::real((Gluonchain[t1][x1][y1][z1][0] * Gluonchain[t2][x1][y1][z1][1] * Gluonchain[t1][x2][y1][z1][0].adjoint() * Gluonchain[t1][x1][y1][z1][1].adjoint()).trace());
                    // W += std::real((Gluonchain[t1][x1][y1][z1][0] * Gluonchain[t2][x1][y1][z1][2] * Gluonchain[t1][x1][y2][z1][0].adjoint() * Gluonchain[t1][x1][y1][z1][2].adjoint()).trace());
                    // W += std::real((Gluonchain[t1][x1][y1][z1][0] * Gluonchain[t2][x1][y1][z1][3] * Gluonchain[t1][x1][y1][z2][0].adjoint() * Gluonchain[t1][x1][y1][z1][3].adjoint()).trace());
                    // W += std::real((Gluonchain[t1][x1][y1][z1][1] * Gluonchain[t1][x2][y1][z1][2] * Gluonchain[t1][x1][y2][z1][1].adjoint() * Gluonchain[t1][x1][y1][z1][2].adjoint()).trace());
                    // W += std::real((Gluonchain[t1][x1][y1][z1][1] * Gluonchain[t1][x2][y1][z1][3] * Gluonchain[t1][x1][y1][z2][1].adjoint() * Gluonchain[t1][x1][y1][z1][3].adjoint()).trace());
                    // W += std::real((Gluonchain[t1][x1][y1][z1][2] * Gluonchain[t1][x1][y2][z1][3] * Gluonchain[t1][x1][y1][z2][2].adjoint() * Gluonchain[t1][x1][y1][z1][3].adjoint()).trace());

                }
            }
        }
    }
    // return 1.0 - std::real(W.trace())/18.0 * full_norm;
    return 1.0 - W/18.0 * full_norm;
}

//-----
// Product in one direction

// template<int Nmu>
// [[nodiscard]]
// Matrix_SU3 LinkChain(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu) noexcept
// {
//     Matrix_SU3 chain {Matrix_SU3::Identity()};
//     switch(mu)
//     {
//         case 0:
//         {
//             for (int n = 0; n < Nmu; ++n)
//             {
//                 chain *= Gluon[(t + n)%Nt][x][y][z][0];
//             }
//         }
//         break;
//         case 1:
//         {
//             for (int n = 0; n < Nmu; ++n)
//             {
//                 chain *= Gluon[t][(x + n)%Nx][y][z][1];
//             }
//         }
//         break;
//         case 2:
//         {
//             for (int n = 0; n < Nmu; ++n)
//             {
//                 chain *= Gluon[t][x][(y + n)%Ny][z][2];
//             }
//         }
//         break;
//         case 3:
//         {
//             for (int n = 0; n < Nmu; ++n)
//             {
//                 chain *= Gluon[t][x][y][(z + n)%Nz][3];
//             }
//         }
//         break;
//     }
//     return chain;
// }

//-----
// Single rectangular Wilson loop
// TODO: Write function

// template<int Nmu, int Nnu>
// [[nodiscard]]
// Matrix_SU3 WilsonLoopRectangle(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu, const int nu) noexcept
// {
//     Matrix_SU3 tmp1 {Matrix_SU3::Identity()};
//     Matrix_SU3 tmp2 {Matrix_SU3::Identity()};
//     Matrix_SU3 tmp3 {Matrix_SU3::Identity()};
//     Matrix_SU3 tmp4 {Matrix_SU3::Identity()};
//     for (int n = 0; n < Nmu; ++n)
//     {
//         tmp1 *= Gluon[t][x][y][z][mu];
//     }
//     return tmp1 * tmp2 * tmp3 * tmp4;
// }

// template<int >
// [[nodiscard]]
// double WilsonLoopGeneral(const Gl_Lattice& Gluon, Gl_Lattice& Gluonchain) noexcept
// {
//     double W {0.0};
//     #pragma omp parallel for
//     for (int i )
// }

//-----
// Returns unnormalized Wilson gauge action

[[nodiscard]]
double GaugeAction(const Gl_Lattice& Gluon)
{
    double S {0.0};

    #pragma omp parallel for reduction(+:S)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int nu = 1; nu < 4; ++nu)
    {
        for (int mu = 0; mu < nu; ++mu)
        {
            S += std::real(Plaquette(Gluon, t, x, y, z, mu, nu).trace());
        }
    }
    return beta * (6.0 * Nt * Nx * Ny * Nz - 1.0/3.0 * S);
}

//-----
// Returns normalized Wilson gauge action/Wilson gauge action per site

[[nodiscard]]
double GaugeActionNormalized(const Gl_Lattice& Gluon)
{
    double S {0.0};
    // Matrix_SU3_double pl;

    #pragma omp parallel for reduction(+:S)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int nu = 1; nu < 4; ++nu)
    {
        for (int mu = 0; mu < nu; ++mu)
        {
            // pl = Plaquette(Gluon, t, x, y, z, mu, nu);
            // S += 1.0/3.0 * std::real((Matrix_SU3::Identity() - pl).trace());
            // S += pl;
            // S += std::real(pl.trace());
            S += std::real(Plaquette(Gluon, t, x, y, z, mu, nu).trace());
            // pl += (Plaquette(Gluon, t, x, y, z, mu, nu)).cast<std::complex<double>>();
        }
    }
    // S = S/(6 * Nt * Nx * Ny * Nz);
    // return 1.0 - std::real(pl.trace())/18.0 * full_norm;
    return 1.0 - S/18.0 * full_norm;
}

//-----
// Returns unnormalized Lüscher-Weisz gauge action
// TODO: Implement

// [[nodiscard]]
// double LWGaugeAction(const Gl_Lattice& Gluon)
// {

// }

//-----
// Projects matrices back on SU(3) via Gram-Schmidt

void ProjectionSU3(Gl_Lattice& Gluon)
{
    for (auto& ind0 : Gluon)
    for (auto& ind1 : ind0)
    for (auto& ind2 : ind1)
    for (auto& ind3 : ind2)
    for (auto& GluonMatrix : ind3)
    {
        floatT norm0 {static_cast<floatT>(1.0)/std::sqrt(std::real(GluonMatrix(0, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(0, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(0, 2) * conj(GluonMatrix(0, 2))))};
        for (int n = 0; n < 3; ++n)
        {
            GluonMatrix(0, n) = norm0 * GluonMatrix(0, n);
        }
        std::complex<floatT> psi {GluonMatrix(1, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(0, 2))};
        for (int n = 0; n < 3; ++n)
        {
            GluonMatrix(1, n) =  GluonMatrix(1, n) - psi * GluonMatrix(0, n);
        }
        floatT norm1 {static_cast<floatT>(1.0)/std::sqrt(std::real(GluonMatrix(1, 0) * conj(GluonMatrix(1, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(1, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(1, 2))))};
        for (int n = 0; n < 3; ++n)
        {
            GluonMatrix(1, n) = norm1 * GluonMatrix(1, n);
        }
        GluonMatrix(2, 0) = conj(GluonMatrix(0, 1) * GluonMatrix(1, 2) - GluonMatrix(0, 2) * GluonMatrix(1, 1));
        GluonMatrix(2, 1) = conj(GluonMatrix(0, 2) * GluonMatrix(1, 0) - GluonMatrix(0, 0) * GluonMatrix(1, 2));
        GluonMatrix(2, 2) = conj(GluonMatrix(0, 0) * GluonMatrix(1, 1) - GluonMatrix(0, 1) * GluonMatrix(1, 0));
    }
}

// Projects a single link back on SU(3) via Gram-Schmidt

void ProjectionSU3Single(Matrix_SU3& GluonMatrix)
{
    floatT norm0 {static_cast<floatT>(1.0)/std::sqrt(std::real(GluonMatrix(0, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(0, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(0, 2) * conj(GluonMatrix(0, 2))))};
    for (int n = 0; n < 3; ++n)
    {
        GluonMatrix(0, n) = norm0 * GluonMatrix(0, n);
    }
    std::complex<floatT> psi {GluonMatrix(1, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(0, 2))};
    for (int n = 0; n < 3; ++n)
    {
        GluonMatrix(1, n) =  GluonMatrix(1, n) - psi * GluonMatrix(0, n);
    }
    floatT norm1 {static_cast<floatT>(1.0)/std::sqrt(std::real(GluonMatrix(1, 0) * conj(GluonMatrix(1, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(1, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(1, 2))))};
    for (int n = 0; n < 3; ++n)
    {
        GluonMatrix(1, n) = norm1 * GluonMatrix(1, n);
    }
    GluonMatrix(2, 0) = conj(GluonMatrix(0, 1) * GluonMatrix(1, 2) - GluonMatrix(0, 2) * GluonMatrix(1, 1));
    GluonMatrix(2, 1) = conj(GluonMatrix(0, 2) * GluonMatrix(1, 0) - GluonMatrix(0, 0) * GluonMatrix(1, 2));
    GluonMatrix(2, 2) = conj(GluonMatrix(0, 0) * GluonMatrix(1, 1) - GluonMatrix(0, 1) * GluonMatrix(1, 0));
}

//-----
// KenneyLaubProjection used for direct SU(N) overrelaxation updates

void KenneyLaubProjection(Matrix_SU3& GluonMatrix)
{
    // Based on section 3 in arXiv:1701.00726
    // Use Kenney-Laub iteration to get the unitary part of the polar decomposition of the matrix
    // TODO: Reverse order of polar decomposition and determinant normalization?
    //       => First normalize with determinant, then iterate?
    Matrix_SU3 X;
    do
    {
        X = GluonMatrix.adjoint() * GluonMatrix;
        GluonMatrix = GluonMatrix/static_cast<floatT>(3.0) * (Matrix_SU3::Identity() + static_cast<floatT>(8.0/3.0) * (GluonMatrix.adjoint() * GluonMatrix + static_cast<floatT>(1.0/3.0) * Matrix_SU3::Identity()).inverse());
    }
    // Iterate as long as the Frobenius norm of the difference between the unit matrix and X = GluonMatrix.adjoint() * GluonMatrix is greater than 1e-6
    while ((Matrix_SU3::Identity() - X).norm() > static_cast<floatT>(1e-6));
    // Then normalize to set determinant equal to 1
    GluonMatrix = GluonMatrix * (static_cast<floatT>(1.0)/std::pow(GluonMatrix.determinant(), static_cast<floatT>(1.0/3.0)));
    // TODO: Use std::cbrt instead of std::pow? std::cbrt doesn't seem to work on std::complex<T>...
    // GluonMatrix = GluonMatrix * (1.f/std::cbrt(GluonMatrix.determinant()));
}

// TODO: Test this
// std::complex<double> cuberoot(std::complex<double> z)
// {
//     if (z.real() < 0)
//     {
//         return -pow(-z, 1.0/3.0);
//     }
//     else
//     {
//         return pow(z, 1.0/3.0);
//     }
//  }

//-----
// Cayley map to transform Lie algebra elements to the associated Lie group
// Can be used as an alernative to the exponential map in the HMC

[[nodiscard]]
Matrix_SU3 CayleyMap(Matrix_3x3 Mat)
{
    return (Matrix_SU3::Identity() - Mat).inverse() * (Matrix_SU3::Identity() + Mat);
}

//-----
// Test unitarity of a matrix (up to a certain precision)

[[nodiscard]]
bool TestUnitarity(const Matrix_SU3& Mat, const floatT prec = 1e-6)
{
    // By default .norm() uses the Frobenius norm, i.e., the square root of the sum of all squared matrix entries
    if ((Matrix_SU3::Identity() - Mat.adjoint() * Mat).norm() > prec)
    {
        return false;
    }
    else
    {
        return true;
    }
}

[[nodiscard]]
bool TestUnitarityAll(const Gl_Lattice& Gluon, const floatT prec = 1e-6)
{
    bool IsUnitary {true};
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        if (TestUnitarity(Gluon[t][x][y][z][mu], prec) != true)
        {
            IsUnitary = false;
            return IsUnitary;
        }
    }
    return IsUnitary;
}

//-----
// Test how close the determinant of a matrix is to 1 (up to a certain precision)

[[nodiscard]]
bool TestSpecial(const Matrix_SU3& Mat, const floatT prec = 1e-6)
{
    if (std::abs(Mat.determinant() - static_cast<floatT>(1.0)) > prec)
    {
        return false;
    }
    else
    {
        return true;
    }
}

[[nodiscard]]
bool TestSpecialAll(const Gl_Lattice& Gluon, const floatT prec = 1e-6)
{
    bool IsSpecial {true};
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        if (TestSpecial(Gluon[t][x][y][z][mu], prec) != true)
        {
            IsSpecial = false;
            return IsSpecial;
        }
    }
    return IsSpecial;
}

//-----
// Test if a matrix is a SU(3) group element (up to a certain precision)

[[nodiscard]]
bool TestSU3(const Matrix_SU3& Mat, const floatT prec = 1e-6)
{
    if ((Matrix_SU3::Identity() - Mat.adjoint() * Mat).norm() > prec && std::abs(Mat.determinant() - static_cast<floatT>(1.0)) > prec)
    {
        return false;
    }
    else
    {
        return true;
    }
}

[[nodiscard]]
bool TestSU3All(const Gl_Lattice& Gluon, const floatT prec = 1e-6)
{
    bool InGroup {true};
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        if (TestSU3(Gluon[t][x][y][z][mu], prec) != true)
        {
            InGroup = false;
            return InGroup;
        }
    }
    return InGroup;
}

//-----
// Test if a matrix is a su(3) algebra element (up to a certain precision)

[[nodiscard]]
bool Testsu3(const Matrix_SU3& Mat, const floatT prec = 1e-6)
{
    if ((Mat - Mat.adjoint()).norm() > prec && std::abs(Mat.trace()) > prec)
    {
        return false;
    }
    else
    {
        return true;
    }
}

[[nodiscard]]
bool Testsu3All(const Gl_Lattice& Gluon, const floatT prec = 1e-6)
{
    bool InAlgebra {true};
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        if (Testsu3(Gluon[t][x][y][z][mu], prec) != true)
        {
            InAlgebra = false;
            return InAlgebra;
        }
    }
    return InAlgebra;
}

//-----
// Stout smearing of gluon fields in all 4 directions

void StoutSmearing4D(const Gl_Lattice& Gluon_unsmeared, Gl_Lattice& Gluon_smeared, const floatT smear_param = 0.12)
{
    Matrix_3x3 Sigma;
    Matrix_3x3 A;
    Matrix_3x3 B;
    Matrix_3x3 C;

    // #pragma omp parallel for collapse(4) private(Sigma, A, B, C, D)
    #pragma omp parallel for private(Sigma, A, B, C)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        for (int mu = 0; mu < 4; ++mu)
        {
            Sigma.noalias() = Staple(Gluon_unsmeared, t, x, y, z, mu);
            A.noalias() = Sigma * Gluon_unsmeared[t][x][y][z][mu].adjoint();
            B.noalias() = A - A.adjoint();
            C.noalias() = static_cast<floatT>(0.5) * B - static_cast<floatT>(1.0/6.0) * B.trace() * Matrix_3x3::Identity();
            Gluon_smeared[t][x][y][z][mu] = (smear_param * C).exp() * Gluon_unsmeared[t][x][y][z][mu];
            ProjectionSU3Single(Gluon_smeared[t][x][y][z][mu]);
        }
    }
}

// TODO: This is potentially dangerous, since we need to make sure we use the correct Gluon array afterwards,
//       which depends on N. For even N, we need to use Gluon1, for odd N we need to use Gluon2!

void StoutSmearingN(Gl_Lattice& Gluon1, Gl_Lattice& Gluon2, const int N, const floatT smear_param = 0.12)
{
    for (int smear_count = 0; smear_count < N; ++smear_count)
    {
        if (smear_count % 2 == 0)
        {
            StoutSmearing4D(Gluon1, Gluon2, smear_param);
        }
        else
        {
            StoutSmearing4D(Gluon2, Gluon1, smear_param);
        }
    }
}

//-----
// Wilson flow using some integrator
// TODO: For now with fixed step-size, later implement adaptive step size?

// void WilsonFlow(const Gl_Lattice& Gluon, Gl_Lattice& Gluon_flowed, const double epsilon)
// {
//     Gluon_flowed = Gluon;
//     std::unique_pointer<Gl_Lattice> temp1 = std::make_unique<Gl_Lattice>();
//     for (int t_current = 0; t_current < t; t_current += epsilon)
//     {
//         for (int mu = 0; mu < 4; ++mu)
//         for (int t = 0; t < Nt; ++t)
//         for (int x = 0; x < Nx; ++x)
//         for (int y = 0; y < Ny; ++y)
//         for (int z = 0; z < Nz; ++z)
//         {
//             temp1[t][x][y][z] = epsilon * ;
//         }
//         // TODO: Implement me
//         // TODO: Save the sum of Z_1 and Z_0 to use twice
//         Gluon_flowed = (3.0/4.0 * Z_2 - 8.0/9.0 * Z_1 + 17.0/36.0 * Z_0).exp() * (8.0/9.0 * Z_1 - 17.0/36.0 * Z_0).exp() * (1.0/4.0 * Z_0).exp() * Gluon_flowed;
//     }
// }

void WilsonFlowForward(Gl_Lattice& Gluon, const double epsilon, const int n_flow)
{
    Matrix_3x3 st;
    Matrix_3x3 A;
    Matrix_3x3 B;
    Matrix_3x3 C;

    #if defined(_OPENMP)
    // Parallel version
    for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    {
        for (int eo = 0; eo < 2; ++eo)
        for (int mu = 0; mu < 4; ++mu)
        {
            #pragma omp parallel for private(st, A, B, C)
            for (int t = 0; t < Nt; ++t)
            for (int x = 0; x < Nx; ++x)
            for (int y = 0; y < Ny; ++y)
            {
                int offset {((t + x + y) & 1) ^ eo};
                for (int z = offset; z < Nz; z+=2)
                {
                    st.noalias() = Staple(Gluon, t, x, y, z, mu);
                    A.noalias() = st * Gluon[t][x][y][z][mu].adjoint();
                    B.noalias() = A - A.adjoint();
                    C.noalias() = static_cast<floatT>(0.5) * B - static_cast<floatT>(1.0/6.0) * B.trace() * Matrix_3x3::Identity();
                    Gluon[t][x][y][z][mu] = (epsilon * C).exp() * Gluon[t][x][y][z][mu];
                    //-----
                    ProjectionSU3Single(Gluon[t][x][y][z][mu]);
                }
            }
        }
    }
    #else
    // Sequential version
    for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    {
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int y = 0; y < Ny; ++y)
        for (int z = 0; z < Nz; ++z)
        {
            for (int mu = 0; mu < 4; ++mu)
            {
                st.noalias() = Staple(Gluon, t, x, y, z, mu);
                A.noalias() = st * Gluon[t][x][y][z][mu].adjoint();
                B.noalias() = A - A.adjoint();
                C.noalias() = static_cast<floatT>(0.5) * B - static_cast<floatT>(1.0/6.0) * B.trace() * Matrix_3x3::Identity();
                Gluon[t][x][y][z][mu] = (epsilon * C).exp() * Gluon[t][x][y][z][mu];
                //-----
                ProjectionSU3Single(Gluon[t][x][y][z][mu]);
            }
        }
    }
    #endif
}

// Seems to be somewhat invertible if precision = 1e-12, but a precision of 1e-8 is definitely not sufficient

void WilsonFlowBackward(Gl_Lattice& Gluon, Gl_Lattice& Gluon_temp, const double epsilon, const int n_flow, const floatT precision = 1e-14)
{
    Gluon_temp = Gluon;
    Matrix_3x3 st;
    Matrix_3x3 A;
    Matrix_3x3 B;
    Matrix_3x3 C;
    Matrix_SU3 old_link;

    #if defined (_OPENMP)
    // Parallel version
    for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    {
        for (int eo = 1; eo >= 0; --eo)
        for (int mu = 3; mu >= 0; --mu)
        {
            #pragma omp parallel for private(st, A, B, C, old_link)
            for (int t = Nt - 1; t >= 0; --t)
            for (int x = Nx - 1; x >= 0; --x)
            for (int y = Ny - 1; y >= 0; --y)
            {
                int offset {((t + x + y) & 1) ^ eo};
                for (int z = Nz - 1 - (1 + Nz%2 + offset)%2; z >= offset; z-=2)
                {
                    do
                    {
                        old_link = Gluon_temp[t][x][y][z][mu];
                        ProjectionSU3Single(old_link);
                        st.noalias() = Staple(Gluon_temp, t, x, y, z, mu);
                        A.noalias() = st * Gluon_temp[t][x][y][z][mu].adjoint();
                        B.noalias() = A - A.adjoint();
                        C.noalias() = static_cast<floatT>(0.5) * B - static_cast<floatT>(1.0/6.0) * B.trace() * Matrix_3x3::Identity();
                        Gluon_temp[t][x][y][z][mu] = (epsilon * C).exp() * Gluon[t][x][y][z][mu];
                        //-----
                        ProjectionSU3Single(Gluon_temp[t][x][y][z][mu]);
                    }
                    while ((Gluon_temp[t][x][y][z][mu] - old_link).norm() > precision);
                }
            }
        }
        // Copy to original array
        Gluon = Gluon_temp;
    }
    #else
    // Sequential version
    for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    {
        for (int t = Nt - 1; t >= 0; --t)
        for (int x = Nx - 1; x >= 0; --x)
        for (int y = Ny - 1; y >= 0; --y)
        for (int z = Nz - 1; z >= 0; --z)
        {
            for (int mu = 3; mu >= 0; --mu)
            {
                do
                {
                    old_link = Gluon_temp[t][x][y][z][mu];
                    ProjectionSU3Single(old_link);
                    st.noalias() = Staple(Gluon_temp, t, x, y, z, mu);
                    A.noalias() = st * Gluon_temp[t][x][y][z][mu].adjoint();
                    B.noalias() = A - A.adjoint();
                    C.noalias() = static_cast<floatT>(0.5) * B - static_cast<floatT>(1.0/6.0) * B.trace() * Matrix_3x3::Identity();
                    Gluon_temp[t][x][y][z][mu] = (epsilon * C).exp() * Gluon[t][x][y][z][mu];
                    //-----
                    ProjectionSU3Single(Gluon_temp[t][x][y][z][mu]);
                }
                while ((Gluon_temp[t][x][y][z][mu] - old_link).norm() > precision);
            }
        }
        // Copy to original array
        Gluon = Gluon_temp;
    }
    #endif
}

//-----
// Calculates clover term

// template<int Nmu>
// void Clover(const Gl_Lattice& Gluon, Full_tensor& Q)
void Clover(const Gl_Lattice& Gluon, Gl_Lattice& Gluonchain, Full_tensor& Clov, const int Nmu)
{
    // Gl_Lattice Gluonchain;
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        for (int mu = 0; mu < 4; ++mu)
        {
            Gluonchain[t][x][y][z][mu].setIdentity();
        }
        for (int n = 0; n < Nmu; ++ n)
        {
            Gluonchain[t][x][y][z][0] *= Gluon[(t + n)%Nt][x][y][z][0];
            Gluonchain[t][x][y][z][1] *= Gluon[t][(x + n)%Nx][y][z][1];
            Gluonchain[t][x][y][z][2] *= Gluon[t][x][(y + n)%Ny][z][2];
            Gluonchain[t][x][y][z][3] *= Gluon[t][x][y][(z + n)%Nz][3];
        }
    }

    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        int tm = (t - Nmu + Nt)%Nt;
        int xm = (x - Nmu + Nx)%Nx;
        int ym = (y - Nmu + Ny)%Ny;
        int zm = (z - Nmu + Nz)%Nz;
        int tp = (t + Nmu)%Nt;
        int xp = (x + Nmu)%Nx;
        int yp = (y + Nmu)%Ny;
        int zp = (z + Nmu)%Nz;

        Clov[t][x][y][z][0][0].setZero();
        Clov[t][x][y][z][0][1] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint()
                               + Gluon[t][x][y][z][1] * Gluon[tm][xp][y][z][0].adjoint() * Gluon[tm][x][y][z][1].adjoint() * Gluon[tm][x][y][z][0]
                               + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][xm][y][z][1].adjoint() * Gluon[tm][xm][y][z][0] * Gluon[t][xm][y][z][1]
                               + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1] * Gluon[t][x][y][z][0].adjoint();
        Clov[t][x][y][z][1][0] = Clov[t][x][y][z][0][1].adjoint();

        Clov[t][x][y][z][0][2] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint()
                               + Gluon[t][x][y][z][2] * Gluon[tm][x][yp][z][0].adjoint() * Gluon[tm][x][y][z][2].adjoint() * Gluon[tm][x][y][z][0]
                               + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][ym][z][2].adjoint() * Gluon[tm][x][ym][z][0] * Gluon[t][x][ym][z][2]
                               + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2] * Gluon[t][x][y][z][0].adjoint();
        Clov[t][x][y][z][2][0] = Clov[t][x][y][z][0][2].adjoint();

        Clov[t][x][y][z][0][3] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint()
                               + Gluon[t][x][y][z][3] * Gluon[tm][x][y][zp][0].adjoint() * Gluon[tm][x][y][z][3].adjoint() * Gluon[tm][x][y][z][0]
                               + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][zm][3].adjoint() * Gluon[tm][x][y][zm][0] * Gluon[t][x][y][zm][3]
                               + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3] * Gluon[t][x][y][z][0].adjoint();
        Clov[t][x][y][z][3][0] = Clov[t][x][y][z][0][3].adjoint();

        Clov[t][x][y][z][1][1].setZero();
        Clov[t][x][y][z][1][2] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint()
                               + Gluon[t][x][y][z][2] * Gluon[t][xm][yp][z][1].adjoint() * Gluon[t][xm][y][z][2].adjoint() * Gluon[t][xm][y][z][1]
                               + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][ym][z][2].adjoint() * Gluon[t][xm][ym][z][1] * Gluon[t][x][ym][z][2]
                               + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2] * Gluon[t][x][y][z][1].adjoint();
        Clov[t][x][y][z][2][1] = Clov[t][x][y][z][1][2].adjoint();

        Clov[t][x][y][z][1][3] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint()
                               + Gluon[t][x][y][z][3] * Gluon[t][xm][y][zp][1].adjoint() * Gluon[t][xm][y][z][3].adjoint() * Gluon[t][xm][y][z][1]
                               + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][zm][3].adjoint() * Gluon[t][xm][y][zm][1] * Gluon[t][x][y][zm][3]
                               + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3] * Gluon[t][x][y][z][1].adjoint();
        Clov[t][x][y][z][3][1] = Clov[t][x][y][z][1][3].adjoint();

        Clov[t][x][y][z][2][2].setZero();
        Clov[t][x][y][z][2][3] = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint()
                               + Gluon[t][x][y][z][3] * Gluon[t][x][ym][zp][2].adjoint() * Gluon[t][x][ym][z][3].adjoint() * Gluon[t][x][ym][z][2]
                               + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][zm][3].adjoint() * Gluon[t][x][ym][zm][2] * Gluon[t][x][y][zm][3]
                               + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3] * Gluon[t][x][y][z][2].adjoint();
        Clov[t][x][y][z][3][2] = Clov[t][x][y][z][2][3].adjoint();
        Clov[t][x][y][z][3][3].setZero();
    }
}

//-----
// Calculates the field strength tensor using clover definition

void Fieldstrengthtensor(const Gl_Lattice& Gluon, Gl_Lattice& Gluonchain, Full_tensor& F, Full_tensor& Q, const int Nmu)
{
    Clover(Gluon, Gluonchain, Q, Nmu);

    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    for (int nu = 0; nu < 4; ++nu)
    {
        // F[t][x][y][z][mu][nu] = std::complex<floatT> (0.0, - 0.125) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
        // F[t][x][y][z][mu][nu] = std::complex<floatT> (0.0, - 1.0 / (8 * Nmu * Nmu)) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
        // F[t][x][y][z][mu][nu] = -i<floatT>/(8.0 * Nmu * Nmu) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
        F[t][x][y][z][mu][nu] = -i<floatT>/(8.f * Nmu * Nmu) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
    }

    // TODO: Rewrite like this
    // Local_tensor Q;
    // #pragma omp parallel for
    // for (int t = 0; t < Nt; ++t)
    // for (int x = 0; x < Nx; ++x)
    // for (int y = 0; y < Ny; ++y)
    // for (int z = 0; z < Nz; ++z)
    // {
    //     // Insert clover calculation here
    //     for (int mu = 0; mu < 4; ++mu)
    //     for (int nu = 0; nu < 4; ++nu)
    //     {
    //         // F[t][x][y][z][mu][nu] = std::complex<floatT> (0.0, - 0.125) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
    //         // F[t][x][y][z][mu][nu] = std::complex<floatT> (0.0, - 1.0 / (8 * Nmu * Nmu)) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
    //         // F[t][x][y][z][mu][nu] = -i<floatT>/(8.0 * Nmu * Nmu) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
    //         F[t][x][y][z][mu][nu] = -i<floatT>/8.f * (Q[mu][nu] - Q[nu][mu]);
    //     }
    // }
}

//-----
// Calculates energy density from field strength tensor

[[nodiscard]]
double Energy_density(const Gl_Lattice& Gluon, Gl_Lattice& Gluonchain, Full_tensor& F, Full_tensor& Q, const int Nmu)
{
    cout << "\nBeginning of function" << endl;
    double e_density {0.0};

    cout << "\nCalculating field strength tensor" << endl;
    Fieldstrengthtensor(Gluon, Gluonchain, F, Q, Nmu);
    cout << "\nCalculated field strength tensor" << endl;

    #pragma omp parallel for reduction(+:e_density)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    for (int nu = 0; nu < 4; ++nu)
    {
        e_density += std::real((F[t][x][y][z][mu][nu] * F[t][x][y][z][mu][nu]).trace());
    }
    // e_density *= 1.0/(2.0 * Nx * Ny * Nz * Nt * Nmu * Nmu);
    e_density *= 1.0/(2.0 * 9.0 * Nx * Ny * Nz * Nt);
    // cout << e_density << "\n";
    return e_density;
}

//-----
// Calculate field-theoretic topological charge using field-strength tensor

// [[nodiscard]]
// double TopChargeGluonicOld(const Gl_Lattice& Gluon, Full_tensor& F)
// {
//     double Q {0.0};
//     #pragma omp parallel for reduction(+:Q)
//     for (int t = 0; t < Nt; ++t)
//     for (int x = 0; x < Nx; ++x)
//     for (int y = 0; y < Ny; ++y)
//     for (int z = 0; z < Nz; ++z)
//     {
//         Q += std::real((F[t][x][y][z][0][1] * F[t][x][y][z][2][3] + F[t][x][y][z][0][2] * F[t][x][y][z][3][1] + F[t][x][y][z][0][3] * F[t][x][y][z][1][2]).trace());
//     }
//     return 1.0 / (4.0 * pi<double> * pi<double>) * Q;
// }

[[nodiscard]]
double TopChargeGluonic(const Gl_Lattice& Gluon)
{
    double Q {0.0};
    #pragma omp parallel for reduction(+:Q)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Local_tensor Clov;
        Local_tensor F;
        int tm = (t - 1 + Nt)%Nt;
        int xm = (x - 1 + Nx)%Nx;
        int ym = (y - 1 + Ny)%Ny;
        int zm = (z - 1 + Nz)%Nz;
        int tp = (t + 1)%Nt;
        int xp = (x + 1)%Nx;
        int yp = (y + 1)%Ny;
        int zp = (z + 1)%Nz;
        // Calculate clover term
        // TODO: Rewrite using plaquette function?
        Clov[0][0].setZero();
        Clov[0][1] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint()
                   + Gluon[t][x][y][z][1] * Gluon[tm][xp][y][z][0].adjoint() * Gluon[tm][x][y][z][1].adjoint() * Gluon[tm][x][y][z][0]
                   + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][xm][y][z][1].adjoint() * Gluon[tm][xm][y][z][0] * Gluon[t][xm][y][z][1]
                   + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1] * Gluon[t][x][y][z][0].adjoint();
        Clov[1][0] = Clov[0][1].adjoint();

        Clov[0][2] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint()
                   + Gluon[t][x][y][z][2] * Gluon[tm][x][yp][z][0].adjoint() * Gluon[tm][x][y][z][2].adjoint() * Gluon[tm][x][y][z][0]
                   + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][ym][z][2].adjoint() * Gluon[tm][x][ym][z][0] * Gluon[t][x][ym][z][2]
                   + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2] * Gluon[t][x][y][z][0].adjoint();
        Clov[2][0] = Clov[0][2].adjoint();

        Clov[0][3] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint()
                   + Gluon[t][x][y][z][3] * Gluon[tm][x][y][zp][0].adjoint() * Gluon[tm][x][y][z][3].adjoint() * Gluon[tm][x][y][z][0]
                   + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][zm][3].adjoint() * Gluon[tm][x][y][zm][0] * Gluon[t][x][y][zm][3]
                   + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3] * Gluon[t][x][y][z][0].adjoint();
        Clov[3][0] = Clov[0][3].adjoint();

        Clov[1][1].setZero();
        Clov[1][2] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint()
                   + Gluon[t][x][y][z][2] * Gluon[t][xm][yp][z][1].adjoint() * Gluon[t][xm][y][z][2].adjoint() * Gluon[t][xm][y][z][1]
                   + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][ym][z][2].adjoint() * Gluon[t][xm][ym][z][1] * Gluon[t][x][ym][z][2]
                   + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2] * Gluon[t][x][y][z][1].adjoint();
        Clov[2][1] = Clov[1][2].adjoint();

        Clov[1][3] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint()
                   + Gluon[t][x][y][z][3] * Gluon[t][xm][y][zp][1].adjoint() * Gluon[t][xm][y][z][3].adjoint() * Gluon[t][xm][y][z][1]
                   + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][zm][3].adjoint() * Gluon[t][xm][y][zm][1] * Gluon[t][x][y][zm][3]
                   + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3] * Gluon[t][x][y][z][1].adjoint();
        Clov[3][1] = Clov[1][3].adjoint();

        Clov[2][2].setZero();
        Clov[2][3] = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint()
                   + Gluon[t][x][y][z][3] * Gluon[t][x][ym][zp][2].adjoint() * Gluon[t][x][ym][z][3].adjoint() * Gluon[t][x][ym][z][2]
                   + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][zm][3].adjoint() * Gluon[t][x][ym][zm][2] * Gluon[t][x][y][zm][3]
                   + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3] * Gluon[t][x][y][z][2].adjoint();
        Clov[3][2] = Clov[2][3].adjoint();
        Clov[3][3].setZero();
        // TODO: Use symmetry of F_mu,nu
        for (int mu = 0; mu < 4; ++mu)
        for (int nu = 0; nu < 4; ++nu)
        {
            F[mu][nu] = -i<floatT>/8.f * (Clov[mu][nu] - Clov[nu][mu]);
        }
        Q += std::real((F[0][1] * F[2][3] + F[0][2] * F[3][1] + F[0][3] * F[1][2]).trace());
    }
    return 1.0 / (4.0 * pi<double> * pi<double>) * Q;
}

[[nodiscard]]
double TopChargeGluonicSymm(const Gl_Lattice& Gluon)
{
    double Q {0.0};
    #pragma omp parallel for reduction(+:Q)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        std::array<Matrix_3x3, 6> Clov;
        // TODO: SU3 or 3x3? Entries of F lie in the adjoint bundle, so probably not SU3?
        std::array<Matrix_3x3, 6> F;
        int tm = (t - 1 + Nt)%Nt;
        int xm = (x - 1 + Nx)%Nx;
        int ym = (y - 1 + Ny)%Ny;
        int zm = (z - 1 + Nz)%Nz;
        int tp = (t + 1)%Nt;
        int xp = (x + 1)%Nx;
        int yp = (y + 1)%Ny;
        int zp = (z + 1)%Nz;
        // Calculate clover term using Q_mu,nu = Q_nu,mu^{dagger}
        // TODO: Rewrite using plaquette function?
        // Clov[0][0].setZero();
        Clov[0] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint()
                + Gluon[t][x][y][z][1] * Gluon[tm][xp][y][z][0].adjoint() * Gluon[tm][x][y][z][1].adjoint() * Gluon[tm][x][y][z][0]
                + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][xm][y][z][1].adjoint() * Gluon[tm][xm][y][z][0] * Gluon[t][xm][y][z][1]
                + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1] * Gluon[t][x][y][z][0].adjoint();
        // Clov[1][0] = Clov[0][1].adjoint();

        Clov[1] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint()
                + Gluon[t][x][y][z][2] * Gluon[tm][x][yp][z][0].adjoint() * Gluon[tm][x][y][z][2].adjoint() * Gluon[tm][x][y][z][0]
                + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][ym][z][2].adjoint() * Gluon[tm][x][ym][z][0] * Gluon[t][x][ym][z][2]
                + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2] * Gluon[t][x][y][z][0].adjoint();
        // Clov[2][0] = Clov[0][2].adjoint();

        Clov[2] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint()
                + Gluon[t][x][y][z][3] * Gluon[tm][x][y][zp][0].adjoint() * Gluon[tm][x][y][z][3].adjoint() * Gluon[tm][x][y][z][0]
                + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][zm][3].adjoint() * Gluon[tm][x][y][zm][0] * Gluon[t][x][y][zm][3]
                + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3] * Gluon[t][x][y][z][0].adjoint();
        // Clov[3][0] = Clov[0][3].adjoint();

        // Clov[1][1].setZero();
        Clov[3] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint()
                + Gluon[t][x][y][z][2] * Gluon[t][xm][yp][z][1].adjoint() * Gluon[t][xm][y][z][2].adjoint() * Gluon[t][xm][y][z][1]
                + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][ym][z][2].adjoint() * Gluon[t][xm][ym][z][1] * Gluon[t][x][ym][z][2]
                + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2] * Gluon[t][x][y][z][1].adjoint();
        // Clov[2][1] = Clov[1][2].adjoint();

        Clov[4] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint()
                + Gluon[t][x][y][z][3] * Gluon[t][xm][y][zp][1].adjoint() * Gluon[t][xm][y][z][3].adjoint() * Gluon[t][xm][y][z][1]
                + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][zm][3].adjoint() * Gluon[t][xm][y][zm][1] * Gluon[t][x][y][zm][3]
                + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3] * Gluon[t][x][y][z][1].adjoint();
        // Clov[3][1] = Clov[1][3].adjoint();

        // Clov[2][2].setZero();
        Clov[5] = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint()
                + Gluon[t][x][y][z][3] * Gluon[t][x][ym][zp][2].adjoint() * Gluon[t][x][ym][z][3].adjoint() * Gluon[t][x][ym][z][2]
                + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][zm][3].adjoint() * Gluon[t][x][ym][zm][2] * Gluon[t][x][y][zm][3]
                + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3] * Gluon[t][x][y][z][2].adjoint();
        // Clov[3][2] = Clov[2][3].adjoint();
        // Clov[3][3].setZero();
        // Version that uses the symmetry of F_mu,nu
        // for (int mu = 0; mu < 4; ++mu)
        // for (int nu = mu + 1; nu < 4; ++nu)
        // {
        //     F[mu][nu] = -i<floatT>/8.f * (Clov[mu][nu] - Clov[mu][nu].adjoint());
        // }
        //-----
        // // F[0][1]
        // F[0] = -i<floatT>/8.f * (Clov[0] - Clov[0].adjoint());
        // // F[0][2]
        // F[1] = -i<floatT>/8.f * (Clov[1] - Clov[1].adjoint());
        // // F[0][3]
        // F[2] = -i<floatT>/8.f * (Clov[2] - Clov[2].adjoint());
        // // F[1][2]
        // F[3] = -i<floatT>/8.f * (Clov[3] - Clov[3].adjoint());
        // // F[1][3]
        // F[4] = -i<floatT>/8.f * (Clov[4] - Clov[4].adjoint());
        // // F[2][3]
        // F[5] = -i<floatT>/8.f * (Clov[5] - Clov[5].adjoint());
        // Q += std::real((F[0] * F[5] - F[1] * F[4] + F[2] * F[3]).trace());
        //-----
        // F[0][1]
        F[0] = (Clov[0] - Clov[0].adjoint());
        // F[0][2]
        F[1] = (Clov[1] - Clov[1].adjoint());
        // F[0][3]
        F[2] = (Clov[2] - Clov[2].adjoint());
        // F[1][2]
        F[3] = (Clov[3] - Clov[3].adjoint());
        // F[1][3]
        F[4] = (Clov[4] - Clov[4].adjoint());
        // F[2][3]
        F[5] = (Clov[5] - Clov[5].adjoint());
        Q += std::real((F[0] * F[5] - F[1] * F[4] + F[2] * F[3]).trace());
    }
    return -1.0 / (64.0 * 4.0 * pi<double> * pi<double>) * Q;
}

[[nodiscard]]
double TopChargeGluonicUnimproved(const Gl_Lattice& Gluon)
{
    double Q {0.0};
    #pragma omp parallel for reduction(+:Q)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Local_tensor Clov;
        Local_tensor F;
        int tp = (t + 1)%Nt;
        int xp = (x + 1)%Nx;
        int yp = (y + 1)%Ny;
        int zp = (z + 1)%Nz;
        // Calculate clover term
        // TODO: Rewrite using plaquette function?
        Clov[0][0].setZero();
        Clov[0][1] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint();
        Clov[1][0] = Clov[0][1].adjoint();

        Clov[0][2] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint();
        Clov[2][0] = Clov[0][2].adjoint();

        Clov[0][3] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint();
        Clov[3][0] = Clov[0][3].adjoint();

        Clov[1][1].setZero();
        Clov[1][2] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint();
        Clov[2][1] = Clov[1][2].adjoint();

        Clov[1][3] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint();
        Clov[3][1] = Clov[1][3].adjoint();

        Clov[2][2].setZero();
        Clov[2][3] = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint();
        Clov[3][2] = Clov[2][3].adjoint();
        Clov[3][3].setZero();
        for (int mu = 0; mu < 4; ++mu)
        for (int nu = 0; nu < 4; ++nu)
        {
            F[mu][nu] = -i<floatT> * (Clov[mu][nu] - Clov[nu][mu]);
        }
        Q += std::real((F[0][1] * F[2][3] + F[0][2] * F[3][1] + F[0][3] * F[1][2]).trace());
    }
    // TODO: Normalization of 16.0 instead of 4.0 should yield correct results, but it is probably unnecessary to calcualte a clover term
    // like above. Instead, simply take the imaginary part of plaquettes and automatically get correct normalization?
    // For comparison of definitions see: https://arxiv.org/pdf/1708.00696.pdf
    return 1.0 / (16.0 * pi<double> * pi<double>) * Q;
}

// TODO: Write this

// [[nodiscard]]
// double TopChargeGluonicImproved(const Gl_Lattice& Gluon)
// {
//     double Q {0.0};
//     #pragma omp parallel for reduction(+:Q)
//     for (int t = 0; t < Nt; ++t)
//     for (int x = 0; x < Nx; ++x)
//     for (int y = 0; y < Ny; ++y)
//     for (int z = 0; z < Nz; ++z)
//     {
//         std::array<Matrix_3x3, 6> Clov;
//         // TODO: SU3 or 3x3? Entries of F lie in the adjoint bundle, so probably not SU3?
//         std::array<Matrix_3x3, 6> F;
//         int tm = (t - 1 + Nt)%Nt;
//         int xm = (x - 1 + Nx)%Nx;
//         int ym = (y - 1 + Ny)%Ny;
//         int zm = (z - 1 + Nz)%Nz;
//         int tp = (t + 1)%Nt;
//         int xp = (x + 1)%Nx;
//         int yp = (y + 1)%Ny;
//         int zp = (z + 1)%Nz;
//         // Calculate clover term using Q_mu,nu = Q_nu,mu^{dagger}
//         // TODO: Rewrite using plaquette function?
//         // Clov[0][0].setZero();
//         Clov[0] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint()
//                 + Gluon[t][x][y][z][1] * Gluon[tm][xp][y][z][0].adjoint() * Gluon[tm][x][y][z][1].adjoint() * Gluon[tm][x][y][z][0]
//                 + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][xm][y][z][1].adjoint() * Gluon[tm][xm][y][z][0] * Gluon[t][xm][y][z][1]
//                 + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1] * Gluon[t][x][y][z][0].adjoint();
//         // Clov[1][0] = Clov[0][1].adjoint();

//         Clov[1] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint()
//                 + Gluon[t][x][y][z][2] * Gluon[tm][x][yp][z][0].adjoint() * Gluon[tm][x][y][z][2].adjoint() * Gluon[tm][x][y][z][0]
//                 + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][ym][z][2].adjoint() * Gluon[tm][x][ym][z][0] * Gluon[t][x][ym][z][2]
//                 + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2] * Gluon[t][x][y][z][0].adjoint();
//         // Clov[2][0] = Clov[0][2].adjoint();

//         Clov[2] = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint()
//                 + Gluon[t][x][y][z][3] * Gluon[tm][x][y][zp][0].adjoint() * Gluon[tm][x][y][z][3].adjoint() * Gluon[tm][x][y][z][0]
//                 + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][zm][3].adjoint() * Gluon[tm][x][y][zm][0] * Gluon[t][x][y][zm][3]
//                 + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3] * Gluon[t][x][y][z][0].adjoint();
//         // Clov[3][0] = Clov[0][3].adjoint();

//         // Clov[1][1].setZero();
//         Clov[3] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint()
//                 + Gluon[t][x][y][z][2] * Gluon[t][xm][yp][z][1].adjoint() * Gluon[t][xm][y][z][2].adjoint() * Gluon[t][xm][y][z][1]
//                 + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][ym][z][2].adjoint() * Gluon[t][xm][ym][z][1] * Gluon[t][x][ym][z][2]
//                 + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2] * Gluon[t][x][y][z][1].adjoint();
//         // Clov[2][1] = Clov[1][2].adjoint();

//         Clov[4] = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint()
//                 + Gluon[t][x][y][z][3] * Gluon[t][xm][y][zp][1].adjoint() * Gluon[t][xm][y][z][3].adjoint() * Gluon[t][xm][y][z][1]
//                 + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][zm][3].adjoint() * Gluon[t][xm][y][zm][1] * Gluon[t][x][y][zm][3]
//                 + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3] * Gluon[t][x][y][z][1].adjoint();
//         // Clov[3][1] = Clov[1][3].adjoint();

//         // Clov[2][2].setZero();
//         Clov[5] = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint()
//                 + Gluon[t][x][y][z][3] * Gluon[t][x][ym][zp][2].adjoint() * Gluon[t][x][ym][z][3].adjoint() * Gluon[t][x][ym][z][2]
//                 + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][zm][3].adjoint() * Gluon[t][x][ym][zm][2] * Gluon[t][x][y][zm][3]
//                 + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3] * Gluon[t][x][y][z][2].adjoint();
//         // Clov[3][2] = Clov[2][3].adjoint();
//         // Clov[3][3].setZero();
//         //-----
//         // F[0][1]
//         F[0] = (Clov[0] - Clov[0].adjoint());
//         // F[0][2]
//         F[1] = (Clov[1] - Clov[1].adjoint());
//         // F[0][3]
//         F[2] = (Clov[2] - Clov[2].adjoint());
//         // F[1][2]
//         F[3] = (Clov[3] - Clov[3].adjoint());
//         // F[1][3]
//         F[4] = (Clov[4] - Clov[4].adjoint());
//         // F[2][3]
//         F[5] = (Clov[5] - Clov[5].adjoint());
//         Q += std::real((F[0] * F[5] - F[1] * F[4] + F[2] * F[3]).trace());
//     }
//     return -1.0 / (64.0 * 4.0 * pi<double> * pi<double>) * Q;
// }

//-----
// Metropolis update routine

void MetropolisUpdate(Gl_Lattice& Gluon, const int n_sweep, uint_fast64_t& acceptance_count, floatT& epsilon, std::uniform_real_distribution<floatT>& distribution_prob, std::uniform_int_distribution<int>& distribution_choice, std::uniform_real_distribution<floatT>& distribution_unitary)
{
    acceptance_count = 0;

    // std::chrono::duration<double> staple_time {0.0};
    // std::chrono::duration<double> local_time {0.0};
    // std::chrono::duration<double> multihit_time {0.0};
    // std::chrono::duration<double> accept_reject_time {0.0};
    for (int sweep_count = 0; sweep_count < n_sweep; ++sweep_count)
    for (int eo = 0; eo < 2; ++eo)
    for (int mu = 0; mu < 4; ++mu)
    {
        // #pragma omp parallel for reduction(+:acceptance_count) shared(prng_vector) private(st, old_link, new_link, s, sprime) firstprivate(eo, mu)
        #pragma omp parallel for reduction(+:acceptance_count) shared(prng_vector)
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int y = 0; y < Ny; ++y)
        {
            int offset {((t + x + y) & 1) ^ eo};
            for (int z = offset; z < Nz; z+=2)
            {
                // auto start_staple = std::chrono::high_resolution_clock::now();
                Matrix_3x3 st {Staple(Gluon, t, x, y, z, mu)};
                // auto end_staple = std::chrono::high_resolution_clock::now();
                // staple_time += end_staple - start_staple;

                // auto start_local = std::chrono::high_resolution_clock::now();
                Matrix_SU3 old_link {Gluon[t][x][y][z][mu]};
                floatT s {SLocal(old_link, st)};
                // auto end_local = std::chrono::high_resolution_clock::now();
                // local_time += end_local - start_local;
                // std::array<int, multi_hit>    prng_choice_vec;
                // std::array<floatT, multi_hit> prng_unitary_vec;
                // std::array<floatT, multi_hit> prng_prob_vec;
                // for (int n_hit = 0; n_hit < multi_hit; ++n_hit)
                // {
                //     prng_choice_vec[n_hit] = distribution_choice(prng_vector[omp_get_thread_num()]);
                //     prng_unitary_vec[n_hit] = distribution_unitary(prng_vector[omp_get_thread_num()]);
                //     prng_prob_vec[n_hit] = distribution_prob(prng_vector[omp_get_thread_num()]);
                // }
                for (int n_hit = 0; n_hit < multi_hit; ++n_hit)
                {
                    #if defined(_OPENMP)
                    // int choice = prng_choice_vec[n_hit];
                    // floatT phi = prng_unitary_vec[n_hit];
                    int choice {distribution_choice(prng_vector[omp_get_thread_num()])};
                    floatT phi {distribution_unitary(prng_vector[omp_get_thread_num()])};
                    Matrix_SU3 new_link {old_link * RandomSU3Parallel(choice, phi)};
                    #else
                    // auto start_multihit = std::chrono::high_resolution_clock::now();
                    int choice {distribution_choice(generator_rand)};
                    floatT phi {distribution_unitary(generator_rand)};
                    Matrix_SU3 new_link {old_link * RandomSU3Parallel(choice, phi)};
                    // auto end_multihit = std::chrono::high_resolution_clock::now();
                    // multihit_time += end_multihit - start_multihit;
                    #endif

                    // auto start_accept_reject = std::chrono::high_resolution_clock::now();
                    floatT sprime {SLocal(new_link, st)};
                    double p {std::exp(-sprime + s)};
                    // TODO: Does this help in any way? Also try out for Orelax
                    // double p {std::exp(SLocalDiff(old_link - new_link, st))};
                    #if defined(_OPENMP)
                    double q {distribution_prob(prng_vector[omp_get_thread_num()])};
                    // double q = prng_prob_vec[n_hit];
                    #else
                    double q {distribution_prob(generator_rand)};
                    #endif

                    // Ugly hack to avoid branches in parallel region
                    // CAUTION: We would want to check if q <= p, since for beta = 0 everything should be accepted
                    // Unfortunately signbit(0) returns false... Is there way to fix this?
                    // bool accept {std::signbit(q - p)};
                    // Gluon[t][x][y][z][mu] = accept * new_link + (!accept) * old_link;
                    // old_link = accept * new_link + (!accept) * old_link;
                    // s = accept * sprime + (!accept) * s;
                    // acceptance_count += accept;
                    if (q <= p)
                    {
                        Gluon[t][x][y][z][mu] = new_link;
                        old_link = new_link;
                        s = sprime;
                        acceptance_count += 1;
                    }
                    // auto end_accept_reject = std::chrono::high_resolution_clock::now();
                    // accept_reject_time += end_accept_reject - start_accept_reject;
                }
                ProjectionSU3Single(Gluon[t][x][y][z][mu]);
            }
        }
    }
    // TODO: Test which acceptance rate is best. Initially had 0.8 as target, but 0.5 seems to thermalize much faster!
    // Adjust PRNG width to target mean acceptance rate of 0.5
    epsilon += (acceptance_count * metro_norm - static_cast<floatT>(metro_target_acceptance)) * static_cast<floatT>(0.2);
    // cout << "staple_time: " << staple_time.count() << "\n";
    // cout << "local_time: " << local_time.count() << "\n";
    // cout << "multihit_time: " << multihit_time.count() << "\n";
    // cout << "accept_reject_time: " << accept_reject_time.count() << endl;
}

void OverrelaxationDirect(Gl_Lattice& Gluon, const int n_sweep, uint_fast64_t& acceptance_count_or, std::uniform_real_distribution<floatT>& distribution_prob)
{
    for (int sweep_count = 0; sweep_count < n_sweep; ++sweep_count)
    for (int mu = 0; mu < 4; ++mu)
    for (int eo = 0; eo < 2; ++eo)
    {
        #pragma omp parallel for reduction(+:acceptance_count_or)
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int y = 0; y < Ny; ++y)
        {
            int offset {((t + x + y) & 1) ^ eo};
            for (int z = offset; z < Nz; z+=2)
            {
                Matrix_3x3 st {Staple(Gluon, t, x, y, z, mu)};
                // Use normalized staple
                Matrix_SU3 or_matrix {static_cast<floatT>(1.0/6.0) * st};
                KenneyLaubProjection(or_matrix);
                Matrix_SU3 old_link {Gluon[t][x][y][z][mu]};
                Matrix_SU3 new_link {or_matrix * old_link.adjoint() * or_matrix};

                floatT s {SLocal(old_link, st)};
                floatT sprime {SLocal(new_link, st)};
                double p {std::exp(-sprime + s)};
                #if defined(_OPENMP)
                double q {distribution_prob(prng_vector[omp_get_thread_num()])};
                #else
                double q {distribution_prob(generator_rand)};
                #endif

                if (q <= p)
                {
                    Gluon[t][x][y][z][mu] = new_link;
                    s = sprime;
                    acceptance_count_or += 1;
                }
            }
        }
    }
}

// Placeholder functions

struct SU2_comp
{
    std::complex<floatT> e11, e12;

    SU2_comp() {};

    SU2_comp(std::complex<floatT> e11_in, std::complex<floatT> e12_in) : e11(e11_in), e12(e12_in) {}

    friend SU2_comp operator+(const SU2_comp& mat1, const SU2_comp& mat2)
    {
        return {mat1.e11 + mat2.e11, mat1.e12 + mat2.e12};
    }
    friend SU2_comp operator-(const SU2_comp& mat1, const SU2_comp& mat2)
    {
        return {mat1.e11 - mat2.e11, mat1.e12 - mat2.e12};
    }
    friend SU2_comp operator*(const SU2_comp& mat1, const SU2_comp& mat2)
    {
        return {mat1.e11 * mat2.e11 - mat1.e12 * std::conj(mat2.e12), mat1.e11 * mat2.e12 + mat1.e12 * std::conj(mat2.e11)};
    }

    // TODO: Is this necessary or is it sufficient to define the scalar multiplication for complex numbers?
    friend SU2_comp operator*(const floatT a, const SU2_comp& mat)
    {
        return {a * mat.e11, a * mat.e12};
    }
    friend SU2_comp operator*(const SU2_comp& mat, const floatT a)
    {
        return {mat.e11 * a, mat.e12 * a};
    }
    friend SU2_comp operator*(const std::complex<floatT> a, const SU2_comp& mat)
    {
        return {a * mat.e11, a * mat.e12};
    }
    friend SU2_comp operator*(const SU2_comp& mat, const std::complex<floatT> a)
    {
        return {mat.e11 * a, mat.e12 * a};
    }
    // TODO: Implement scalar division operator / and operator /=?

    SU2_comp &operator=(const SU2_comp& mat)
    {
        e11 = mat.e11;
        e12 = mat.e12;
        return *this;
    }
    SU2_comp &operator+=(const SU2_comp& mat)
    {
        e11 += mat.e11;
        e12 += mat.e12;
        return *this;
    }
    SU2_comp &operator-=(const SU2_comp& mat)
    {
        e11 -= mat.e11;
        e12 -= mat.e12;
        return *this;
    }
    SU2_comp &operator*=(const SU2_comp& mat)
    {
        *this = *this * mat;
        return *this;
    }
    SU2_comp &operator*=(const floatT a)
    {
        *this = *this * a;
        return *this;
    }
    SU2_comp &operator*=(const std::complex<floatT> a)
    {
        *this = *this * a;
        return *this;
    }

    SU2_comp adjoint() const
    {
        return {std::conj(e11), -e12};
    }

    floatT det_sq() const
    {
        return std::real(e11) * std::real(e11) + std::imag(e11) * std::imag(e11) + std::real(e12) * std::real(e12) + std::imag(e12) * std::imag(e12);
    }
};

// Directly embed SU(2) matrix into SU(3) matrix

Matrix_3x3 Embed01(const SU2_comp& mat)
{
    Matrix_3x3 mat_embedded;
    mat_embedded <<             mat.e11,            mat.e12, 0,
                    -std::conj(mat.e12), std::conj(mat.e11), 0,
                                      0,                  0, 1;
    return mat_embedded;
}

Matrix_3x3 Embed02(const SU2_comp& mat)
{
    Matrix_3x3 mat_embedded;
    mat_embedded <<             mat.e11, 0,            mat.e12,
                                      0, 1,                  0,
                    -std::conj(mat.e12), 0, std::conj(mat.e11);
    return mat_embedded;
}

Matrix_3x3 Embed12(const SU2_comp& mat)
{
    Matrix_3x3 mat_embedded;
    mat_embedded << 1,                   0,                  0,
                    0,             mat.e11,            mat.e12,
                    0, -std::conj(mat.e12), std::conj(mat.e11);
    return mat_embedded;
}

// Extract SU(2) matrix from SU(3) matrix via projection (we obviously can't directly extract subblocks since that wouldn't generally be in SU(2))
// TODO: Write function that directly extracts the product? Might lead to better performance since we need fewer multiplications

SU2_comp Extract01(const Matrix_3x3& mat)
{
    std::complex<floatT> temp1 {static_cast<floatT>(0.5) * (mat(0, 0) + std::conj(mat(1, 1)))};
    std::complex<floatT> temp2 {static_cast<floatT>(0.5) * (mat(0, 1) - std::conj(mat(1, 0)))};
    return {temp1, temp2};
}

// TODO: For symmetry reasons, make this (2, 0) instead of (0, 2)?
SU2_comp Extract02(const Matrix_3x3& mat)
{
    std::complex<floatT> temp1 {static_cast<floatT>(0.5) * (mat(0, 0) + std::conj(mat(2, 2)))};
    std::complex<floatT> temp2 {static_cast<floatT>(0.5) * (mat(0, 2) - std::conj(mat(2, 0)))};
    return {temp1, temp2};
}

SU2_comp Extract12(const Matrix_3x3& mat)
{
    std::complex<floatT> temp1 {static_cast<floatT>(0.5) * (mat(1, 1) + std::conj(mat(2, 2)))};
    std::complex<floatT> temp2 {static_cast<floatT>(0.5) * (mat(1, 2) - std::conj(mat(2, 1)))};
    return {temp1, temp2};
}

// TODO: Test if this is faster. Preliminary test pretty much show no difference, probably due to Eigen's expression templates

// SU2_comp Extract01_new(const Matrix_3x3& mat1, const Matrix_3x3 mat2)
// {
//     std::complex<floatT> prod_00 {mat1(0, 0) * mat2(0, 0) + mat1(0, 1) * mat2(1, 0) + mat1(0, 2) * mat2(2, 0)};
//     std::complex<floatT> prod_01 {mat1(0, 0) * mat2(0, 1) + mat1(0, 1) * mat2(1, 1) + mat1(0, 2) * mat2(2, 1)};
//     std::complex<floatT> prod_10 {mat1(1, 0) * mat2(0, 0) + mat1(1, 1) * mat2(1, 0) + mat1(1, 2) * mat2(2, 0)};
//     std::complex<floatT> prod_11 {mat1(1, 0) * mat2(0, 1) + mat1(1, 1) * mat2(1, 1) + mat1(1, 2) * mat2(2, 1)};
//     std::complex<floatT> temp1   {static_cast<floatT>(0.5) * (prod_00 + std::conj(prod_11))};
//     std::complex<floatT> temp2   {static_cast<floatT>(0.5) * (prod_01 - std::conj(prod_10))};
//     return {temp1, temp2};
// }

// SU2_comp Extract02_new(const Matrix_3x3& mat1, const Matrix_3x3 mat2)
// {
//     std::complex<floatT> prod_00 {mat1(0, 0) * mat2(0, 0) + mat1(0, 1) * mat2(1, 0) + mat1(0, 2) * mat2(2, 0)};
//     std::complex<floatT> prod_02 {mat1(0, 0) * mat2(0, 2) + mat1(0, 1) * mat2(1, 2) + mat1(0, 2) * mat2(2, 2)};
//     std::complex<floatT> prod_20 {mat1(2, 0) * mat2(0, 0) + mat1(2, 1) * mat2(1, 0) + mat1(2, 2) * mat2(2, 0)};
//     std::complex<floatT> prod_22 {mat1(2, 0) * mat2(0, 2) + mat1(2, 1) * mat2(1, 2) + mat1(2, 2) * mat2(2, 2)};
//     std::complex<floatT> temp1   {static_cast<floatT>(0.5) * (prod_00 + std::conj(prod_22))};
//     std::complex<floatT> temp2   {static_cast<floatT>(0.5) * (prod_02 - std::conj(prod_20))};
//     return {temp1, temp2};
// }

// SU2_comp Extract12_new(const Matrix_3x3& mat1, const Matrix_3x3 mat2)
// {
//     std::complex<floatT> prod_11 {mat1(1, 0) * mat2(0, 1) + mat1(1, 1) * mat2(1, 1) + mat1(1, 2) * mat2(2, 1)};
//     std::complex<floatT> prod_12 {mat1(1, 0) * mat2(0, 2) + mat1(1, 1) * mat2(1, 2) + mat1(1, 2) * mat2(2, 2)};
//     std::complex<floatT> prod_21 {mat1(2, 0) * mat2(0, 1) + mat1(2, 1) * mat2(1, 1) + mat1(2, 2) * mat2(2, 1)};
//     std::complex<floatT> prod_22 {mat1(2, 0) * mat2(0, 2) + mat1(2, 1) * mat2(1, 2) + mat1(2, 2) * mat2(2, 2)};
//     std::complex<floatT> temp1   {static_cast<floatT>(0.5) * (prod_11 + std::conj(prod_22))};
//     std::complex<floatT> temp2   {static_cast<floatT>(0.5) * (prod_12 - std::conj(prod_21))};
//     return {temp1, temp2};
// }

//-----
// Overrelaxation update for SU(2)

SU2_comp OverrelaxationSU2(const SU2_comp& A)
{
    floatT a_norm {static_cast<floatT>(1.0) / std::sqrt(A.det_sq())};
    SU2_comp V {a_norm * A};
    return (V * V).adjoint();

}

//-----
// Overrelaxation update for SU(3) using Cabibbo-Marinari method

void OverrelaxationSubgroup(Gl_Lattice& Gluon, const int n_sweep)
{
    for (int sweep_count = 0; sweep_count < n_sweep; ++sweep_count)
    for (int mu = 0; mu < 4; ++mu)
    for (int eo = 0; eo < 2; ++eo)
    {
        // #pragma omp parallel for
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int y = 0; y < Ny; ++y)
        {
            int offset {((t + x + y) & 1) ^ eo};
            for (int z = offset; z < Nz; z+=2)
            {
                Matrix_3x3 W;
                SU2_comp subblock;
                // Note: Our staple definition corresponds to the daggered staple in Gattringer & Lang, therefore use adjoint
                Matrix_3x3 st_adj {(Staple(Gluon, t, x, y, z, mu)).adjoint()};
                //-----
                // Update (0, 1) subgroup
                // W = Gluon[t][x][y][z][mu] * st_adj;
                // std::cout << "Action before: " << SLocal(Gluon[t][x][y][z][mu], st_adj.adjoint()) << endl;
                subblock = Extract01(Gluon[t][x][y][z][mu] * st_adj);
                Gluon[t][x][y][z][mu] = Embed01(OverrelaxationSU2(subblock)) * Gluon[t][x][y][z][mu];
                // std::cout << "Action after: " << SLocal(Gluon[t][x][y][z][mu], st_adj.adjoint()) << endl;
                //-----
                // Update (0, 2) subgroup
                // W = Gluon[t][x][y][z][mu] * st_adj;
                // std::cout << "Action before: " << SLocal(Gluon[t][x][y][z][mu], st_adj.adjoint()) << endl;
                subblock = Extract02(Gluon[t][x][y][z][mu] * st_adj);
                Gluon[t][x][y][z][mu] = Embed02(OverrelaxationSU2(subblock)) * Gluon[t][x][y][z][mu];
                // std::cout << "Action after: " << SLocal(Gluon[t][x][y][z][mu], st_adj.adjoint()) << endl;
                //-----
                // Update (1, 2) subgroup
                // W = Gluon[t][x][y][z][mu] * st_adj;
                // std::cout << "Action before: " << SLocal(Gluon[t][x][y][z][mu], st_adj.adjoint()) << endl;
                subblock = Extract12(Gluon[t][x][y][z][mu] * st_adj);
                Gluon[t][x][y][z][mu] = Embed12(OverrelaxationSU2(subblock)) * Gluon[t][x][y][z][mu];
                // std::cout << "Action after: " << SLocal(Gluon[t][x][y][z][mu], st_adj.adjoint()) << endl;
                //-----
                // Project link to SU(3)
                ProjectionSU3Single(Gluon[t][x][y][z][mu]);
            }
        }
    }
}

//-----
// Heatbath update for SU(2)

SU2_comp HeatbathSU2(const SU2_comp& A, const floatT prefactor, std::uniform_real_distribution<floatT>& distribution_uniform, const int max_iteration = 10)
{
    // Determinant of staple as norm to project staple back to SU(2)
    floatT a_norm {static_cast<floatT>(1.0) / std::sqrt(A.det_sq())};
    SU2_comp V {a_norm * A};
    SU2_comp mat_su2;
    floatT r1, r2, r3, x1, x2, x3, lambda_sq, r0;
    int count {0};
    do
    {
        // Generate random number lambda_sq following a polynomially modified Gaussian distribution (cf. Gattringer & Lang (4.43))
        // floatT r1 {static_cast<floatT>(1.0) - distribution_uniform(prng_vector[omp_get_thread_num()])};
        // floatT x1 {std::log(r1)};
        // floatT r2 {static_cast<floatT>(1.0) - distribution_uniform(prng_vector[omp_get_thread_num()])};
        // floatT x2 {std::cos(static_cast<floatT>(2.0) * pi<floatT> * r2)};
        // floatT r3 {static_cast<floatT>(1.0) - distribution_uniform(prng_vector[omp_get_thread_num()])};
        // floatT x3 {std::log(r3)};
        r1 = static_cast<floatT>(1.0) - distribution_uniform(prng_vector[omp_get_thread_num()]);
        x1 = std::log(r1);
        r2 = static_cast<floatT>(1.0) - distribution_uniform(prng_vector[omp_get_thread_num()]);
        x2 = std::cos(static_cast<floatT>(2.0) * pi<floatT> * r2);
        r3 = static_cast<floatT>(1.0) - distribution_uniform(prng_vector[omp_get_thread_num()]);
        x3 = std::log(r3);
        // Factor 0.25, so for N_col = 2 we get a factor 0.5, while for N_col = 3 we get a factor 0.75
        // floatT lambda_sq {static_cast<floatT>(-0.25 * prefactor * a_norm) * (x1 + x2 * x2 * x3)};
        lambda_sq = static_cast<floatT>(-0.25 * prefactor * a_norm) * (x1 + x2 * x2 * x3);
        //-----
        // Correct for factor sqrt(1 - lambda_sq) in probability distribution via accept-reject step
        // floatT r0 {distribution_uniform(prng_vector[omp_get_thread_num()])};
        r0 = distribution_uniform(prng_vector[omp_get_thread_num()]);
        if (count > max_iteration)
        {
            return {1.0, 0.0};
        }
        ++count;
    }
    while (r0 * r0 + lambda_sq >= static_cast<floatT>(1.0));
    // if (count > 3)
    // {
    //     std::cout << count << std::endl;
    // }

    // if (r0 * r0 + lambda_sq < static_cast<floatT>(1.0))
    // {
        // Calculate zeroth coefficient of our SU(2) matrix in quaternionic representation
        floatT x0 {static_cast<floatT>(1.0) - static_cast<floatT>(2.0) * lambda_sq};
        // Calculate absolute value of our random vector
        floatT abs_x {std::sqrt(static_cast<floatT>(1.0) - x0 * x0)};
        // Generate angular variables, i.e., random vector with length abs_x (simply generating three uniformly distributed values in the unit range and
        // normalizing them does not work, since the resulting distribution is not uniform, but biased against vectors close to the coordinate axes)
        // Instead, we generate a random vector with length abs_x in spherical coordinates
        // Since the functional determinant contains a factor sin(theta), directly generating the coordinates does not give a uniform distribution
        // Therefore, we generate cos(theta) in [-1, 1] using sqrt(1 - rand^2)
        // TODO: We want a random number in the closed interval [-1, 1], but the standard distribution only covers the half open interval [-1, 1) or
        // (-1, 1]. Therefore, do something like this: std::uniform_real_distribution<floatT> distribution_uniform(-1.0, std::nextafter(1.0, 2.0))
        // floatT r1 {static_cast<floatT>(1.0) - static_cast<floatT>(2.0) * distribution_uniform(prng_vector[omp_get_thread_num()])};
        // Random number in interval [0, 1)
        floatT phi {distribution_uniform(prng_vector[omp_get_thread_num()])};
        // Random number in interval (-1, 1]
        floatT cos_theta {static_cast<floatT>(1.0) - static_cast<floatT>(2.0) * distribution_uniform(prng_vector[omp_get_thread_num()])};
        floatT vec_norm {abs_x * std::sqrt(static_cast<floatT>(1.0) - cos_theta * cos_theta)};
        //-----
        x1 = vec_norm * std::cos(static_cast<floatT>(2.0) * pi<floatT> * phi);
        x2 = vec_norm * std::sin(static_cast<floatT>(2.0) * pi<floatT> * phi);
        x3 = abs_x * cos_theta;
        //-----
        // mat_su2 = SU2_comp {std::complex<floatT> (x0, x1), std::complex<floatT> (x2, x3)} * V.adjoint();
        return {SU2_comp {std::complex<floatT> (x0, x1), std::complex<floatT> (x2, x3)} * V.adjoint()};
    // }
    // else
    // {
        // mat_su2 = SU2_comp {1.0, 0.0};
    // }
    // return mat_su2;
}

//-----
// Heatbath update for SU(3) using Cabibbo-Marinari method
// TODO: Extend to SU(N) with Ncol?

void HeatbathSU3(Gl_Lattice& Gluon, const int n_sweep, std::uniform_real_distribution<floatT>& distribution_uniform)
{
    // For SU(2), the prefactor is 0.5 / beta
    // For SU(3), the prefactor is 0.75 / beta
    // floatT prefactor {static_cast<floatT>(0.75) / beta}; // N_c/(2 * 2)
    int N_col {3};
    floatT prefactor {static_cast<floatT>(N_col) / beta};
    for (int sweep_count = 0; sweep_count < n_sweep; ++sweep_count)
    for (int mu = 0; mu < 4; ++mu)
    for (int eo = 0; eo < 2; ++eo)
    {
        #pragma omp parallel for
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int y = 0; y < Ny; ++y)
        {
            int offset {((t + x + y) & 1) ^ eo};
            for (int z = offset; z < Nz; z+=2)
            {
                Matrix_3x3 W;
                SU2_comp subblock;
                // Note: Our staple definition corresponds to the daggered staple in Gattringer & Lang, therefore use adjoint
                Matrix_3x3 st_adj {(Staple(Gluon, t, x, y, z, mu)).adjoint()};
                //-----
                // Update (0, 1) subgroup
                // W = Gluon[t][x][y][z][mu] * st_adj;
                subblock = Extract01(Gluon[t][x][y][z][mu] * st_adj);
                Gluon[t][x][y][z][mu] = Embed01(HeatbathSU2(subblock, prefactor, distribution_uniform)) * Gluon[t][x][y][z][mu];
                //-----
                // Update (0, 2) subgroup
                // W = Gluon[t][x][y][z][mu] * st_adj;
                subblock = Extract02(Gluon[t][x][y][z][mu] * st_adj);
                Gluon[t][x][y][z][mu] = Embed02(HeatbathSU2(subblock, prefactor, distribution_uniform)) * Gluon[t][x][y][z][mu];
                //-----
                // Update (1, 2) subgroup
                // W = Gluon[t][x][y][z][mu] * st_adj;
                subblock = Extract12(Gluon[t][x][y][z][mu] * st_adj);
                Gluon[t][x][y][z][mu] = Embed12(HeatbathSU2(subblock, prefactor, distribution_uniform)) * Gluon[t][x][y][z][mu];
                //-----
                // Project link to SU(3)
                ProjectionSU3Single(Gluon[t][x][y][z][mu]);
            }
        }
    }
}

//-----
// Generate random momenta for HMC

void RandomMomentum(Gl_Lattice& Momentum)
{
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        // Generate 8 random numbers as basis coefficients
        floatT phi1 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi2 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi3 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi4 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi5 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi6 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi7 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        floatT phi8 {ndist_vector[omp_get_thread_num()](prng_vector[omp_get_thread_num()])};
        // Random momentum in su(3)
        Matrix_3x3 A;
        A << std::complex<floatT>(phi3 + phi8/sqrt(3.0),0.0),std::complex<floatT>(phi1,-phi2),std::complex<floatT>(phi4,-phi5),
             std::complex<floatT>(phi1,phi2),                std::complex<floatT>(-phi3 + phi8/sqrt(3.0),0.0),std::complex<floatT>(phi6,-phi7),
             std::complex<floatT>(phi4,phi5),                std::complex<floatT>(phi6,phi7),std::complex<floatT>(-2.0*phi8/sqrt(3.0),0.0);
        Momentum[t][x][y][z][mu] = static_cast<floatT>(0.5) * A;
    }
    // std::cout << "Random momenta lie in algebra: " << Testsu3All(Momentum, 1e-12) << std::endl;
}

//-----
// Reverse momenta for HMC reversibility test

void ReverseMomenta(Gl_Lattice& Momentum)
{
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        Momentum[t][x][y][z][mu] = -Momentum[t][x][y][z][mu];
    }
    // std::cout << "Momenta lie in algebra: " << Testsu3All(Momentum, 1e-12) << std::endl;
}

//-----
// Update momenta for HMC

void UpdateMomenta(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const floatT epsilon)
{
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        Matrix_3x3 st {Staple(Gluon, t, x, y, z, mu)};
        Matrix_3x3 tmp {st * Gluon[t][x][y][z][mu].adjoint() - Gluon[t][x][y][z][mu] * st.adjoint()};
        Momentum[t][x][y][z][mu] -= epsilon * i<floatT> * beta / static_cast<floatT>(12.0) * (tmp - static_cast<floatT>(1.0/3.0) * tmp.trace() * Matrix_3x3::Identity());
    }
    // std::cout << "Momenta lie in algebra: " << Testsu3All(Momentum, 1e-12) << std::endl;
}

//-----
// Update gauge fields for HMC

void UpdateFields(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const floatT epsilon)
{
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        // Gluon[t][x][y][z][mu] += epsilon * Momentum[t][x][y][z][mu];
        Matrix_3x3 tmp_mat {(i<floatT> * epsilon * Momentum[t][x][y][z][mu]).exp()};
        // ProjectionSU3Single(tmp_mat);
        Gluon[t][x][y][z][mu] = tmp_mat * Gluon[t][x][y][z][mu];
        ProjectionSU3Single(Gluon[t][x][y][z][mu]);
        // Gluon[t][x][y][z][mu] = (-epsilon * Momentum[t][x][y][z][mu]).exp() * Gluon[t][x][y][z][mu];
        // Gluon[t][x][y][z][mu] = CayleyMap(-0.5 * epsilon * Momentum[t][x][y][z][mu]) * Gluon[t][x][y][z][mu];
        // Gluon[t][x][y][z][mu] = (-i<floatT> * epsilon * Momentum[t][x][y][z][mu]).exp();
    }
    // std::cout << "new test: " << TestSU3All(Gluon, 1e-8) << std::endl;
    // std::cout << "new test: " << Gluon[1][3][4][7][2].determinant() << "\n" << Gluon[1][3][4][7][2] * Gluon[1][3][4][7][2].adjoint() << "\n" << std::endl;
    // std::cout << "Fields lie in group: " << TestSU3All(Gluon, 1e-12) << std::endl;
}

[[nodiscard]]
double Hamiltonian(const Gl_Lattice& Gluon, const Gl_Lattice& Momentum)
{
    double potential_energy {GaugeAction(Gluon)};
    double kinetic_energy {0.0};
    // TODO: Momentum * Momentum.adjoint() or Momentum^2? Also is there a prefactor 0.5 or not?
    #pragma omp parallel for reduction(+:kinetic_energy)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        kinetic_energy += std::real((Momentum[t][x][y][z][mu] * Momentum[t][x][y][z][mu]).trace());
    }
    // std::cout << "kinetic_energy: " << kinetic_energy << " potential_energy: " << potential_energy << std::endl;
    return potential_energy + kinetic_energy;
}

namespace HMC
{
    // Leapfrog integrator for HMC
    void Leapfrog(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const int n_step)
    {
        // Calculate stepsize epsilon from n_step
        floatT epsilon {static_cast<floatT>(1.0)/n_step};
        // Perform integration
        // Momentum updates are merged in the loop
        UpdateMomenta(Gluon, Momentum, 0.5 * epsilon);
        for (int step_count = 0; step_count < n_step - 1; ++step_count)
        {
            UpdateFields(Gluon, Momentum, epsilon);
            UpdateMomenta(Gluon, Momentum, epsilon);
        }
        UpdateFields(Gluon, Momentum, epsilon);
        UpdateMomenta(Gluon, Momentum, 0.5 * epsilon);
    }
    //-----
    // Omelyan-Mryglod-Folk second order minimum norm integrator (improved leapfrog)
    // cf. hep-lat/0505020
    // NOTE: This version doesn't use merged momentum updates and is slightly less efficient than the one below
    void OMF_2_slow(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const int n_step)
    {
        // Calculate stepsize epsilon from n_step
        floatT epsilon {static_cast<floatT>(1.0)/n_step};
        // Integrator constants
        double alpha {0.1931833275037836 * epsilon};
        double beta  {0.5 * epsilon};
        double gamma {(1.0 - 2.0 * 0.1931833275037836) * epsilon};
        // Perform integration
        for (int step_count = 0; step_count < n_step; ++step_count)
        {
            UpdateMomenta(Gluon, Momentum, alpha);
            UpdateFields(Gluon, Momentum, beta);
            UpdateMomenta(Gluon, Momentum, gamma);
            UpdateFields(Gluon, Momentum, beta);
            UpdateMomenta(Gluon, Momentum, alpha);
        }
    }
    //-----
    // Omelyan-Mryglod-Folk second order minimum norm integrator (improved leapfrog)
    // cf. hep-lat/0505020
    void OMF_2(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const int n_step)
    {
        // Calculate stepsize epsilon from n_step
        floatT epsilon {static_cast<floatT>(1.0)/n_step};
        // Integrator constants
        double alpha {0.1931833275037836 * epsilon};
        double beta  {0.5 * epsilon};
        double gamma {(1.0 - 2.0 * 0.1931833275037836) * epsilon};
        // Perform integration
        // Momentum updates are merged in the loop
        UpdateMomenta(Gluon, Momentum, alpha);
        UpdateFields(Gluon, Momentum, beta);
        UpdateMomenta(Gluon, Momentum, gamma);
        UpdateFields(Gluon, Momentum, beta);
        for (int step_count = 0; step_count < n_step - 1; ++step_count)
        {
            UpdateMomenta(Gluon, Momentum, 2.0 * alpha);
            UpdateFields(Gluon, Momentum, beta);
            UpdateMomenta(Gluon, Momentum, gamma);
            UpdateFields(Gluon, Momentum, beta);
        }
        UpdateMomenta(Gluon, Momentum, alpha);
    }
    //-----
    // Omelyan-Mryglod-Folk fourth order minimum norm integrator
    // cf. hep-lat/0505020
    // NOTE: This version doesn't use merged momentum updates and is slightly less efficient than the one below
    void OMF_4_slow(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const int n_step)
    {
        // Calculate stepsize epsilon from n_step
        floatT epsilon {static_cast<floatT>(1.0)/n_step};
        // Integrator constants
        double alpha {0.08398315262876693 * epsilon};
        double beta  {0.2539785108410595 * epsilon};
        double gamma {0.6822365335719091 * epsilon};
        double delta {-0.03230286765269967 * epsilon};
        double mu    {(0.5 - 0.6822365335719091 - 0.08398315262876693) * epsilon};
        double nu    {(1.0 - 2.0 * -0.03230286765269967 - 2.0 * 0.2539785108410595) * epsilon};
        // Perform integration
        // Momentum updates are not merged in the loop
        for (int step_count = 0; step_count < n_step; ++step_count)
        {
            UpdateMomenta(Gluon, Momentum, alpha);
            UpdateFields(Gluon, Momentum, beta);
            UpdateMomenta(Gluon, Momentum, gamma);
            UpdateFields(Gluon, Momentum, delta);

            UpdateMomenta(Gluon, Momentum, mu);
            UpdateFields(Gluon, Momentum, nu);
            UpdateMomenta(Gluon, Momentum, mu);

            UpdateFields(Gluon, Momentum, delta);
            UpdateMomenta(Gluon, Momentum, gamma);
            UpdateFields(Gluon, Momentum, beta);
            UpdateMomenta(Gluon, Momentum, alpha);
        }
    }
    //-----
    // Omelyan-Mryglod-Folk fourth order minimum norm integrator
    // cf. hep-lat/0505020
    void OMF_4(Gl_Lattice& Gluon, Gl_Lattice& Momentum, const int n_step)
    {
        // Calculate stepsize epsilon from n_step
        floatT epsilon {static_cast<floatT>(1.0)/n_step};
        // Integrator constants
        double alpha {0.08398315262876693 * epsilon};
        double beta  {0.2539785108410595 * epsilon};
        double gamma {0.6822365335719091 * epsilon};
        double delta {-0.03230286765269967 * epsilon};
        double mu    {(0.5 - 0.6822365335719091 - 0.08398315262876693) * epsilon};
        double nu    {(1.0 - 2.0 * -0.03230286765269967 - 2.0 * 0.2539785108410595) * epsilon};
        // Perform integration
        // Momentum updates are merged in the loop
        UpdateMomenta(Gluon, Momentum, alpha);
        UpdateFields(Gluon, Momentum, beta);
        UpdateMomenta(Gluon, Momentum, gamma);
        UpdateFields(Gluon, Momentum, delta);

        UpdateMomenta(Gluon, Momentum, mu);
        UpdateFields(Gluon, Momentum, nu);
        UpdateMomenta(Gluon, Momentum, mu);

        UpdateFields(Gluon, Momentum, delta);
        UpdateMomenta(Gluon, Momentum, gamma);
        UpdateFields(Gluon, Momentum, beta);
        for (int step_count = 0; step_count < n_step - 1; ++step_count)
        {
            UpdateMomenta(Gluon, Momentum, 2.0 * alpha);
            UpdateFields(Gluon, Momentum, beta);
            UpdateMomenta(Gluon, Momentum, gamma);
            UpdateFields(Gluon, Momentum, delta);

            UpdateMomenta(Gluon, Momentum, mu);
            UpdateFields(Gluon, Momentum, nu);
            UpdateMomenta(Gluon, Momentum, mu);

            UpdateFields(Gluon, Momentum, delta);
            UpdateMomenta(Gluon, Momentum, gamma);
            UpdateFields(Gluon, Momentum, beta);
        }
        UpdateMomenta(Gluon, Momentum, alpha);
    }
}

//-----
// HMC for pure gauge theory
// TODO: Constraint on FuncT?
// TODO: Add tau parameter (trajectory time) and pass to integrator functions

template<typename FuncT>
bool HMCGauge(Gl_Lattice& Gluon, Gl_Lattice& Gluon_copy, Gl_Lattice& Momentum, uint_fast64_t& acceptance_count_hmc, FuncT&& Integrator, const int n_step, bool metropolis_step, std::uniform_real_distribution<floatT>& distribution_prob)
{
    // Copy old field so we can restore it in case the update gets rejected
    Gluon_copy = Gluon;
    // Generate random momenta and calculate energy before time evolution
    RandomMomentum(Momentum);
    double energy_old {Hamiltonian(Gluon, Momentum)};
    // Perform integration with chosen integrator
    Integrator(Gluon, Momentum, n_step);
    //-----
    // Reversibility test
    // ReverseMomenta(Momentum);
    // UpdateMomenta(Gluon, Momentum, 0.5 * epsilon);
    // for (int step_count = 0; step_count < n_step - 1; ++step_count)
    // {
    //     UpdateFields(Gluon, Momentum, epsilon);
    //     UpdateMomenta(Gluon, Momentum, epsilon);
    // }
    // UpdateFields(Gluon, Momentum, epsilon);
    // UpdateMomenta(Gluon, Momentum, 0.5 * epsilon);
    //-----
    // Calculate energy after time evolution
    double energy_new {Hamiltonian(Gluon, Momentum)};
    // Metropolis accept-reject step
    double p {std::exp(-energy_new + energy_old)};
    #if defined(_OPENMP)
    double q {distribution_prob(prng_vector[omp_get_thread_num()])};
    #else
    double q {distribution_prob(generator_rand)};
    #endif
    DeltaH = energy_new - energy_old;
    if (metropolis_step)
    {
        // datalog << "DeltaH: " << DeltaH << std::endl;
        if (q <= p)
        {
            acceptance_count_hmc += 1;
            return true;
        }
        else
        {
            Gluon = Gluon_copy;
            return false;
        }
    }
    else
    {
        datalog << "DeltaH: " << DeltaH << std::endl;
        return true;
    }
}

//-----
// TODO: Metadynamics

template<typename FuncT>
void MetadynamicsLocal(Gl_Lattice& Gluon, Gl_Lattice& Gluon1, Gl_Lattice& Gluon2, MetaBiasPotential& Metapotential, FuncT&& CV_function, const int n_sweep_heatbath, const int n_sweep_orelax, std::uniform_real_distribution<floatT>& distribution_prob, std::uniform_real_distribution<floatT>& distribution_uniform)
{
    // Copy old field so we can restore it in case the update gets rejected
    // In contrast to HMC, we expect the acceptance rates to be quite low, so always perform updates using Gluon1 instead of Gluon
    // TODO: Is that true? Better check to be sure
    Gluon1 = Gluon;
    // Get old value of collective variable
    double CV_old {CV_function(Gluon1, Gluon2, rho_stout, 15)};
    // Perform update sweeps
    HeatbathSU3(Gluon1, n_sweep_heatbath, distribution_uniform);
    OverrelaxationSubgroup(Gluon1, n_sweep_orelax);
    // Get new value of collective variable
    double CV_new {CV_function(Gluon1, Gluon2, rho_stout, 15)};
    //-----
    // TODO: Calculate difference in metapotential
    double DeltaV {Metapotential.ReturnPotential(CV_new) - Metapotential.ReturnPotential(CV_old)};
    // Metropolis accept-reject step
    double p {std::exp(-DeltaV)};
    #if defined(_OPENMP)
    double q {distribution_prob(prng_vector[omp_get_thread_num()])};
    #else
    double q {distribution_prob(generator_rand)};
    #endif
    if (q <= p)
    {
        // std::cout << "Accepted!" << std::endl;
        Gluon = Gluon1;
        // TODO: Track acceptance rate
        // acceptance_count_metadyn += 1;
        // TODO: Update metapotential
        if constexpr(metapotential_updated)
        {
            Metapotential.UpdatePotential(CV_new);
        }
    }
    else
    {
        // std::cout << "Rejected" << std::endl;
    }
}

double MetaCharge(Gl_Lattice& Gluon, Gl_Lattice& Gluon_copy, const double epsilon, const int n_flow)
{
    /*StoutSmearing4D(Gluon, Gluon1);*/
    // Gluon1 = Gluon;
    // StoutSmearingN(Gluon1, Gluon2, n_smear_meta, rho_stout);
    // // TODO: Probably need to rewrite StoutSmearingN so we don't have to manually keep track
    // // For even N, we need to use Gluon1, for odd N we need to use Gluon2!
    // // See description of StoutSmearingN()
    // if (n_smear_meta % 2 == 0)
    // {
    //     return TopChargeGluonicSymm(Gluon1);
    // }
    // else
    // {
    //     return TopChargeGluonicSymm(Gluon2);
    // }
    Gluon_copy = Gluon;
    WilsonFlowForward(Gluon_copy, epsilon, n_flow);
    return TopChargeGluonicSymm(Gluon_copy);
}

//-----
// Instanton update?
// TODO: Rearrange for-loops so we don't unnecessarily loop over the lattice

void MultiplyInstanton(Gl_Lattice& Gluon, const int Q)
{
    double action_old {GaugeAction(Gluon)};
    // Embedded commuting SU(2) matrices
    // Generally, any traceless, hermitian matrix works here
    // The condition that the matrices commute enables us to construct charge +/- 1 instantons
    Matrix_3x3 sig;
    sig << 1,  0, 0,
           0, -1, 0,
           0,  0, 0;
    Matrix_3x3 tau;
    tau << 1, 0,  0,
           0, 0,  0,
           0, 0, -1;
    // Unit matrices in sigma and tau subspace 
    Matrix_3x3 id_sig {sig * sig};
    Matrix_3x3 id_tau {tau * tau};
    // Orthogonal complement
    Matrix_3x3 comp_sig {Matrix_3x3::Identity() - id_sig};
    Matrix_3x3 comp_tau {Matrix_3x3::Identity() - id_tau};
    // Consider a two-dimensional slice in the t-x plane
    floatT Field_t {static_cast<floatT>(2.0) * pi<floatT> * std::abs(Q) / (Nt * Nx)};
    floatT Field_x {static_cast<floatT>(2.0) * pi<floatT> * std::abs(Q) / Nx};
    // Assign link values in t-direction
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[t][x][y][z][1] *= comp_sig + std::cos(Field_t * t) * id_sig + i<floatT> * std::sin(Field_t * t) * sig;
    }
    // Assign link values on last time-slice in x-direction
    #pragma omp parallel for
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[Nt - 1][x][y][z][0] *= comp_sig + std::cos(Field_x * x) * id_sig - i<floatT> * std::sin(Field_x * x) * sig;
    }
    //-----
    // Consider a two-dimensional slice in the y-z plane
    floatT Field_y, Field_z;
    if (Q == 0)
    {
        Field_y = static_cast<floatT>(0.0);
        Field_z = static_cast<floatT>(0.0);
    }
    else
    {
        Field_y = static_cast<floatT>(2.0) * pi<floatT> * Q / (std::abs(Q) * Ny * Nz);
        Field_z = static_cast<floatT>(2.0) * pi<floatT> * Q / (std::abs(Q) * Nz);
    }
    // Assign link values in y-direction
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[t][x][y][z][3] *= comp_tau + std::cos(Field_y * y) * id_tau + i<floatT> * std::sin(Field_y * y) * tau;
    }
    // Assign link values on last y-slice in z-direction
    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int z = 0; z < Nz; ++z)
    {
        Gluon[t][x][Ny - 1][z][2] *= comp_tau + std::cos(Field_z * z) * id_tau - i<floatT> * std::sin(Field_z * z) * tau;
    }
    double action_new {GaugeAction(Gluon)};
    double action_diff {action_new - action_old};
    std::cout << "Action (old): " << action_old << "\n";
    std::cout << "Action (new): " << action_new << "\n";
    std::cout << "Action difference: " << action_diff << "\n";
    std::cout << "Acceptance probability: " << std::exp(-action_diff) << "\n" << std::endl;
}

void MultiplyLocalInstanton(Gl_Lattice& Gluon)
{
    double action_old {GaugeAction(Gluon)};
    // Generators of SU(2)/Pauli matrices embedded into SU(3) (up to negative determinant)
    // Generally, any traceless, hermitian matrix works here
    // The condition that the matrices commute enables us to construct charge +/- 1 instantons
    // TODO: This is incorrect, the factors only apply to the stuff inside SU(2), the last entry is always 1!
    // TODO: Directly initialize?
    Matrix_3x3 sig1;
    sig1 << 0, 1, 0,
            1, 0, 0,
            0, 0, 1;
    Matrix_3x3 sig2;
    sig2 << 0        , -i<floatT>, 0,
            i<floatT>,  0        , 0,
            0        ,  0        , 1;
    Matrix_3x3 sig3;
    sig3 << 1,  0, 0,
            0, -1, 0,
            0,  0, 1;
    // Only links in the elementary hypercube between (0,0,0,0) and (1,1,1,1) take on non-trivial values
    // TODO: See above!
    Matrix_SU3 tmp;
    for (int t = 0; t < 2; ++t)
    for (int x = 0; x < 2; ++x)
    for (int y = 0; y < 2; ++y)
    for (int z = 0; z < 2; ++z)
    {
        int coord_sum {t + x + y + z};
        // We only consider ther 32 links INSIDE the hypercube between (0,0,0,0) and (1,1,1,1)
        // Do not go into mu direction if x_mu != 0
        if (x == 0)
        {
            tmp = i<floatT> * std::pow(-1, coord_sum) * sig1;
            tmp(2, 2) = 1.0;
            Gluon[t][x][y][z][1] = Gluon[t][x][y][z][1] * tmp;
        }
        if (y == 0)
        {
            tmp = i<floatT> * std::pow(-1, coord_sum) * sig2;
            tmp(2, 2) = 1.0;
            Gluon[t][x][y][z][2] = Gluon[t][x][y][z][2] * tmp;
        }
        if (z == 0)
        {
            tmp = i<floatT> * std::pow(-1, coord_sum) * sig3;
            tmp(2, 2) = 1.0;
            Gluon[t][x][y][z][3] = Gluon[t][x][y][z][3] * tmp;
        }
    }
    double action_new {GaugeAction(Gluon)};
    double action_diff {action_new - action_old};
    std::cout << "Action (old): " << action_old << "\n";
    std::cout << "Action (new): " << action_new << "\n";
    std::cout << "Action difference: " << action_diff << "\n";
    std::cout << "Acceptance probability: " << std::exp(-action_diff) << "\n" << std::endl;
}

//-----
// Calculates and writes observables to logfile

void Observables(const Gl_Lattice& Gluon, Gl_Lattice& Gluonchain, std::ofstream& wilsonlog, const int n_count, const int n_smear)
{
    //ProjectionSU3(Gluon);
    // double Action = GaugeActionNormalized(Gluon);

    vector<double> Action(n_smear + 1);
    vector<double> WLoop2(n_smear + 1);
    vector<double> WLoop4(n_smear + 1);
    vector<double> WLoop8(n_smear + 1);
    vector<double> PLoopRe(n_smear + 1);
    vector<double> PLoopIm(n_smear + 1);
    vector<std::complex<double>> PLoop(n_smear + 1);
    // vector<double> TopologicalCharge(n_smear + 1);
    vector<double> TopologicalChargeSymm(n_smear + 1);
    vector<double> TopologicalChargeUnimproved(n_smear + 1);

    // Unsmeared observables
    // auto start_action = std::chrono::system_clock::now();
    Action[0] = GaugeActionNormalized(Gluon);
    // auto end_action = std::chrono::system_clock::now();
    // std::chrono::duration<double> action_time = end_action - start_action;
    // cout << "Time for calculating action: " << action_time.count() << endl;

    // auto start_wilson = std::chrono::system_clock::now();
    WLoop2[0] = WilsonLoop<0, 2,  true>(Gluon, Gluonchain);
    // auto end_wilson = std::chrono::system_clock::now();
    // std::chrono::duration<double> wilson_time = end_wilson - start_wilson;
    // cout << "Time for calculating wilson 2: " << wilson_time.count() << endl;

    // start_wilson = std::chrono::system_clock::now();
    WLoop4[0] = WilsonLoop<2, 4, false>(Gluon, Gluonchain);
    // end_wilson = std::chrono::system_clock::now();
    // wilson_time = end_wilson - start_wilson;
    // cout << "Time for calculating wilson 4: " << wilson_time.count() << endl;

    // start_wilson = std::chrono::system_clock::now();
    WLoop8[0] = WilsonLoop<4, 8, false>(Gluon, Gluonchain);
    // end_wilson = std::chrono::system_clock::now();
    // wilson_time = end_wilson - start_wilson;
    // cout << "Time for calculating wilson 8: " << wilson_time.count() << endl;

    // auto start_polyakov = std::chrono::system_clock::now();
    PLoop[0] = PolyakovLoop(Gluon);
    // auto end_polyakov = std::chrono::system_clock::now();
    // std::chrono::duration<double> polyakov_time = end_polyakov - start_polyakov;
    // cout << "Time for calculating Polyakov: " << polyakov_time.count() << endl;

    // auto start_topcharge = std::chrono::system_clock::now();
    // TopologicalCharge[0] = TopChargeGluonic(Gluon);
    // auto end_topcharge = std::chrono::system_clock::now();
    // std::chrono::duration<double> topcharge_time = end_topcharge - start_topcharge;
    // cout << "Time for calculating topcharge: " << topcharge_time.count() << endl;
    // auto start_topcharge_symm = std::chrono::system_clock::now();
    TopologicalChargeSymm[0] = TopChargeGluonicSymm(Gluon);
    // auto end_topcharge_symm = std::chrono::system_clock::now();
    // std::chrono::duration<double> topcharge_symm_time = end_topcharge_symm - start_topcharge_symm;
    // cout << "Time for calculating topcharge (symm): " << topcharge_symm_time.count() << endl;
    TopologicalChargeUnimproved[0] = TopChargeGluonicUnimproved(Gluon);

    //-----
    // Begin smearing
    if (n_smear > 0)
    {
        // Apply smearing
        // auto start_smearing = std::chrono::system_clock::now();
        StoutSmearing4D(Gluon, *Gluonsmeared1, rho_stout);
        // auto end_smearing = std::chrono::system_clock::now();
        // std::chrono::duration<double> smearing_time = end_smearing - start_smearing;
        // cout << "Time for calculating smearing: " << smearing_time.count() << endl;
        // Calculate observables
        Action[1] = GaugeActionNormalized(*Gluonsmeared1);
        WLoop2[1] = WilsonLoop<0, 2,  true>(*Gluonsmeared1, Gluonchain);
        WLoop4[1] = WilsonLoop<2, 4, false>(*Gluonsmeared1, Gluonchain);
        WLoop8[1] = WilsonLoop<4, 8, false>(*Gluonsmeared1, Gluonchain);
        PLoop[1]  = PolyakovLoop(*Gluonsmeared1);
        // TopologicalCharge[1] = TopChargeGluonic(*Gluonsmeared1);
        TopologicalChargeSymm[1] = TopChargeGluonicSymm(*Gluonsmeared1);
        TopologicalChargeUnimproved[1] = TopChargeGluonicUnimproved(*Gluonsmeared1);
    }

    //-----
    // Further smearing steps
    for (int smear_count = 2; smear_count <= n_smear; ++smear_count)
    {
        // Even
        if (smear_count % 2 == 0)
        {
            // Apply smearing
            // StoutSmearing4D(*Gluonsmeared1, *Gluonsmeared2, rho_stout);
            StoutSmearingN(*Gluonsmeared1, *Gluonsmeared2, n_smear_skip, rho_stout);
            // TODO: FIX THIS, INCORRECT IF n_smear_skip is even!
            // if (n_smear_skip % 2 == 0)
            // {
            //     // placeholder
            // }
            // else
            // {
            //     // placeholder
            // }
            // Calculate observables
            Action[smear_count] = GaugeActionNormalized(*Gluonsmeared2);
            WLoop2[smear_count] = WilsonLoop<0, 2,  true>(*Gluonsmeared2, Gluonchain);
            WLoop4[smear_count] = WilsonLoop<2, 4, false>(*Gluonsmeared2, Gluonchain);
            WLoop8[smear_count] = WilsonLoop<4, 8, false>(*Gluonsmeared2, Gluonchain);
            PLoop[smear_count]  = PolyakovLoop(*Gluonsmeared2);
            // TopologicalCharge[smear_count] = TopChargeGluonic(*Gluonsmeared2);
            TopologicalChargeSymm[smear_count] = TopChargeGluonicSymm(*Gluonsmeared2);
            TopologicalChargeUnimproved[smear_count] = TopChargeGluonicUnimproved(*Gluonsmeared2);
        }
        // Odd
        else
        {
            // Apply smearing
            // StoutSmearing4D(*Gluonsmeared2, *Gluonsmeared1, rho_stout);
            StoutSmearingN(*Gluonsmeared2, *Gluonsmeared1, n_smear_skip, rho_stout);
            // Calculate observables
            Action[smear_count] = GaugeActionNormalized(*Gluonsmeared1);
            WLoop2[smear_count] = WilsonLoop<0, 2,  true>(*Gluonsmeared1, Gluonchain);
            WLoop4[smear_count] = WilsonLoop<2, 4, false>(*Gluonsmeared1, Gluonchain);
            WLoop8[smear_count] = WilsonLoop<4, 8, false>(*Gluonsmeared1, Gluonchain);
            PLoop[smear_count]  = PolyakovLoop(*Gluonsmeared1);
            // TopologicalCharge[smear_count] = TopChargeGluonic(*Gluonsmeared1);
            TopologicalChargeSymm[smear_count] = TopChargeGluonicSymm(*Gluonsmeared1);
            TopologicalChargeUnimproved[smear_count] = TopChargeGluonicUnimproved(*Gluonsmeared1);
        }
    }

    //-----
    std::transform(PLoop.begin(), PLoop.end(), PLoopRe.begin(), [](const auto& element){return std::real(element);});
    std::transform(PLoop.begin(), PLoop.end(), PLoopIm.begin(), [](const auto& element){return std::imag(element);});

    //-----
    // Write to logfile
    datalog << "[Step " << n_count << "]\n";
    //-----
    if constexpr(n_hmc != 0)
    {
        datalog << "DeltaH: " << DeltaH << "\n";
    }
    //-----
    datalog << "Wilson_Action: ";
    // std::copy(Action.cbegin(), std::prev(Action.cend()), std::ostream_iterator<double>(datalog, " "));
    std::copy(std::cbegin(Action), std::prev(std::cend(Action)), std::ostream_iterator<double>(datalog, " "));
    datalog << Action.back() << "\n";
    //-----
    datalog << "Wilson_loop(L=2): ";
    // std::copy(WLoop2.cbegin(), std::prev(WLoop2.cend()), std::ostream_iterator<double>(datalog, " "));
    std::copy(std::cbegin(WLoop2), std::prev(std::cend(WLoop2)), std::ostream_iterator<double>(datalog, " "));
    datalog << WLoop2.back() << "\n";
    //-----
    datalog << "Wilson_loop(L=4): ";
    // std::copy(WLoop4.cbegin(), std::prev(WLoop4.cend()), std::ostream_iterator<double>(datalog, " "));
    std::copy(std::cbegin(WLoop4), std::prev(std::cend(WLoop4)), std::ostream_iterator<double>(datalog, " "));
    datalog << WLoop4.back() << "\n";
    //-----
    datalog << "Wilson_loop(L=8): ";
    // std::copy(WLoop8.cbegin(), std::prev(WLoop8.cend()), std::ostream_iterator<double>(datalog, " "));
    std::copy(std::cbegin(WLoop8), std::prev(std::cend(WLoop8)), std::ostream_iterator<double>(datalog, " "));
    datalog << WLoop8.back() << "\n"; //<< endl;
    //-----
    datalog << "Polyakov_loop(Re): ";
    std::copy(std::cbegin(PLoopRe), std::prev(std::cend(PLoopRe)), std::ostream_iterator<double>(datalog, " "));
    datalog << PLoopRe.back() << "\n";
    //-----
    datalog << "Polyakov_loop(Im): ";
    std::copy(std::cbegin(PLoopIm), std::prev(std::cend(PLoopIm)), std::ostream_iterator<double>(datalog, " "));
    datalog << PLoopIm.back() << "\n";


    // datalog << "TopChargeClov: ";
    // std::copy(std::cbegin(TopologicalCharge), std::prev(std::cend(TopologicalCharge)), std::ostream_iterator<double>(datalog, " "));
    // datalog << TopologicalCharge.back() << "\n"; //<< endl;

    datalog << "TopChargeClov: ";
    std::copy(std::cbegin(TopologicalChargeSymm), std::prev(std::cend(TopologicalChargeSymm)), std::ostream_iterator<double>(datalog, " "));
    datalog << TopologicalChargeSymm.back() << "\n"; //<< endl;

    datalog << "TopChargePlaq: ";
    std::copy(std::cbegin(TopologicalChargeUnimproved), std::prev(std::cend(TopologicalChargeUnimproved)), std::ostream_iterator<double>(datalog, " "));
    datalog << TopologicalChargeUnimproved.back() << "\n" << endl;
}

//-----

int main()
{
    // iostream not synchronized with corresponding C streams, might cause a problem with C libraries and might not be thread safe
    std::ios_base::sync_with_stdio(false);
    cout << std::setprecision(12) << std::fixed;
    datalog << std::setprecision(12) << std::fixed;

    Configuration();
    prng_vector = CreatePRNGs();
    if constexpr(n_hmc != 0)
    {
        ndist_vector = CreateNormal();
    }

    // acceptance_count = 0;
    // acceptance_count_or = 0;
    // Default width of random numbers used in Metropolis update is 0.5
    // floatT epsilon {0.001}; // Used to test stability of instanton configurations
    floatT epsilon {0.5};

    std::uniform_real_distribution<floatT> distribution_prob(0.0, 1.0);
    std::uniform_real_distribution<floatT> distribution_uniform(0.0, 1.0);
    std::uniform_int_distribution<int> distribution_choice(1, 8);

    CreateFiles();
    SetGluonToOne(*Gluon);

    auto startcalc {std::chrono::system_clock::now()};
    datalog.open(logfilepath, std::fstream::out | std::fstream::app);
    wilsonlog.open(wilsonfilepath, std::fstream::out | std::fstream::app);

    // Instanton multiplication test
    // std::uniform_real_distribution<floatT> distribution_unitary(-epsilon, epsilon);
    // InstantonStart(*Gluon, 2);
    // for (int n_count = 0; n_count < 20; ++n_count)
    // {
    //     // MetropolisUpdate(*Gluon, n_metro, acceptance_count, epsilon, distribution_prob, distribution_choice, distribution_unitary);
    //     HeatbathSU3(*Gluon, 1, distribution_uniform);
    // }

    // Observables(*Gluon, *Gluonchain, wilsonlog, 0, n_smear);
    // // MultiplyInstanton(*Gluon, 1);
    // MultiplyLocalInstanton(*Gluon);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 1, n_smear);
    // std::exit(0);
    // Instanton start test
    // for (int Q = 0; Q < 10; ++Q)
    // {
    //     InstantonStart(*Gluon, Q);
    //     Observables(*Gluon, *Gluonchain, wilsonlog, Q, n_smear);
    //     cout << "This configuration should have charge " << Q <<". The charge is: " << TopChargeGluonic(*Gluon) << endl;
    //     cout << "This configuration should have charge " << Q <<". The charge is: " << TopChargeGluonicUnimproved(*Gluon) << endl;
    //     MultiplyInstanton(*Gluon, 1);
    //     Observables(*Gluon, *Gluonchain, wilsonlog, Q, n_smear);
    //     cout << "After multiplication the charge is: " << TopChargeGluonic(*Gluon) << endl;
    //     cout << "After multiplication the charge is: " << TopChargeGluonicUnimproved(*Gluon) << endl;
    //     // InstantonStart(*Gluon, -Q);
    //     // cout << "This configuration should have charge " << -Q <<". The charge is: " << TopChargeGluonic(*Gluon) << endl;
    //     // cout << "This configuration should have charge " << -Q <<". The charge is: " << TopChargeGluonicUnimproved(*Gluon) << endl;
    // }

    // Generate 1 instanton
    // LocalInstantonStart(*Gluon);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 0, n_smear);

    // InstantonStart(*Gluon, 1);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 0, n_smear);
    // for (int n_count = 0; n_count < 20; ++n_count)
    // {
    //     MetropolisUpdate(*Gluon, n_metro, acceptance_count, epsilon, distribution_prob, distribution_choice, distribution_unitary);
    // }
    // Observables(*Gluon, *Gluonchain, wilsonlog, -1, 0);
    // for (int flow_count = 0; flow_count < 10; ++flow_count)
    // {
    //     WilsonFlowForward(*Gluon, 0.06, 1);
    //     Observables(*Gluon, *Gluonchain, wilsonlog, flow_count, 0);
    // }
    // // MultiplyInstanton(*Gluon, 1);
    // MultiplyLocalInstanton(*Gluon);
    // Observables(*Gluon, *Gluonchain, wilsonlog, -1, 0);
    // for (int flow_count = 0; flow_count < 10; ++flow_count)
    // {
    //     WilsonFlowBackward(*Gluon, *Gluonsmeared1, -0.06, 1);
    //     Observables(*Gluon, *Gluonchain, wilsonlog, flow_count, 0);
    // }
    // std::exit(0);

    // auto end = std::chrono::system_clock::now();
    // std::chrono::duration<double> elapsed_seconds = end - startcalc;
    // std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    // PrintFinal(datalog, acceptance_count, acceptance_count_or, epsilon, end_time, elapsed_seconds);
    // datalog.close();
    // datalog.clear();

    // datalog.open(parameterfilepath, std::fstream::out | std::fstream::app);
    // PrintFinal(datalog, acceptance_count, acceptance_count_or, epsilon, end_time, elapsed_seconds);
    // datalog.close();
    // datalog.clear();

    // PrintFinal(wilsonlog, acceptance_count, acceptance_count_or, epsilon, end_time, elapsed_seconds);
    // wilsonlog.close();
    // wilsonlog.clear();

    // std::exit(0);

    // MetropolisUpdate(*Gluon, n_metro, acceptance_count, epsilon, distribution_prob, distribution_choice, distribution_unitary);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 0, n_smear);
    // // Multiply with 1 instanton, resulting configuration should have charge (Q_1 + 1) * (1 + 1) = (1 + 1) * (1 + 1) = 4
    // InstantonStart(*Gluon, 1);
    // MultiplyInstanton(*Gluon, 1);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 1, n_smear);

    // MetropolisUpdate(*Gluon, n_metro, acceptance_count, epsilon, distribution_prob, distribution_choice, distribution_unitary);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 1, n_smear);
    // // Generate 4 instanton
    // InstantonStart(*Gluon, 4);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 2, n_smear);

    // MetropolisUpdate(*Gluon, n_metro, acceptance_count, epsilon, distribution_prob, distribution_choice, distribution_unitary);
    // Observables(*Gluon, *Gluonchain, wilsonlog, 2, n_smear);

    // TODO: Rewrite this, maybe keep metadynamics updates in separate main?
    // if constexpr(metadynamics_enabled)
    // {
        // CV_min, CV_max, bin_number, weight, threshold_weight
        MetaBiasPotential TopBiasPotential{-8, 8, 800, 0.02, 1000.0};
        TopBiasPotential.SaveMetaParameters(metapotentialfilepath);
        // auto CV_function = [](){MetaCharge(*Gluon, *Gluonsmeared1, *Gluonsmeared2, 15);};
    // }

    // When using HMC, the thermalization is done without accept-reject step
    if constexpr(n_hmc != 0)
    {
        datalog << "[HMC start thermalization]\n";
        for (int n_count = 0; n_count < 20; ++n_count)
        {
            // std::cout << "HMC accept/reject (therm): " << HMCGauge(*Gluon, *Gluonsmeared1, *Gluonsmeared2, HMC::Leapfrog, 100, false, distribution_prob) << std::endl;
            HMCGauge(*Gluon, *Gluonsmeared1, *Gluonsmeared2, acceptance_count_hmc, HMC::OMF_4, 10, false, distribution_prob);
        }
        datalog << "[HMC end thermalization]\n" << std::endl;
    }

    for (int n_count = 0; n_count < n_run; ++n_count)
    {
        // InstantonStart(*Gluon, 2);
        // auto start_update_metro {std::chrono::system_clock::now()};
        if constexpr(n_metro != 0 && multi_hit != 0)
        {
            std::uniform_real_distribution<floatT> distribution_unitary(-epsilon, epsilon);
            MetropolisUpdate(*Gluon, n_metro, acceptance_count, epsilon, distribution_prob, distribution_choice, distribution_unitary);
        }
        // auto end_update_metro {std::chrono::system_clock::now()};
        // std::chrono::duration<double> update_time_metro {end_update_metro - start_update_metro};
        // cout << "Time for " << n_metro << " Metropolis updates: " << update_time_metro.count() << endl;
        //-----
        // auto start_update_heatbath {std::chrono::system_clock::now()};
        if constexpr(n_heatbath != 0)
        {
            HeatbathSU3(*Gluon, n_heatbath, distribution_uniform);
        }
        // auto end_update_heatbath {std::chrono::system_clock::now()};
        // std::chrono::duration<double> update_time_heatbath {end_update_heatbath - start_update_heatbath};
        // cout << "Time for " << n_heatbath << " heatbath updates: " << update_time_heatbath.count() << endl;
        //-----
        // auto start_update_hmc {std::chrono::system_clock::now()};
        if constexpr(n_hmc != 0)
        {
            // std::cout << "HMC accept/reject: " << HMCGauge(*Gluon, *Gluonsmeared1, *Gluonsmeared2, HMC::OMF_4, n_hmc, true, distribution_prob) << std::endl;
            HMCGauge(*Gluon, *Gluonsmeared1, *Gluonsmeared2, acceptance_count_hmc, HMC::OMF_4, n_hmc, true, distribution_prob);
        }
        // auto end_update_hmc {std::chrono::system_clock::now()};
        // std::chrono::duration<double> update_time_hmc {end_update_hmc - start_update_hmc};
        // cout << "Time for one HMC trajectory: " << update_time_hmc.count() << endl;
        //-----
        // auto start_update_or = std::chrono::system_clock::now();
        if constexpr(n_orelax != 0)
        {
            OverrelaxationDirect(*Gluon, n_orelax, acceptance_count_or, distribution_prob);
            // OverrelaxationSubgroup(*Gluon, n_orelax);
        }
        // auto end_update_or = std::chrono::system_clock::now();
        // std::chrono::duration<double> update_time_or {end_update_or - start_update_or};
        // cout << "Time for " << n_orelax << " OR updates: " << update_time_or.count() << endl;
        //-----
        // auto start_update_meta = std::chrono::system_clock::now();
        if constexpr(metadynamics_enabled)
        {
            MetadynamicsLocal(*Gluon, *Gluonsmeared1, *Gluonsmeared2, TopBiasPotential, MetaCharge, 1, 4, distribution_prob, distribution_uniform);
        }
        // auto end_update_meta = std::chrono::system_clock::now();
        // std::chrono::duration<double> update_time_meta {end_update_meta - start_update_meta};
        // cout << "Time for meta update: " << update_time_meta.count() << endl;
        if (n_count % expectation_period == 0)
        {
            // ProjectionSU3(*Gluon);
            // auto start_observable = std::chrono::system_clock::now();
            Observables(*Gluon, *Gluonchain, wilsonlog, n_count, n_smear);
            // auto end_observable = std::chrono::system_clock::now();
            // std::chrono::duration<double> observable_time {end_observable - start_observable};
            // cout << "Time for calculating observables: " << observable_time.count() << endl;
            if constexpr(metadynamics_enabled)
            {
                TopBiasPotential.SaveMetaPotential(metapotentialfilepath);
            }
        }
    }

    auto end {std::chrono::system_clock::now()};
    std::chrono::duration<double> elapsed_seconds {end - startcalc};
    std::time_t end_time {std::chrono::system_clock::to_time_t(end)};

    //-----
    // Print acceptance rates, PRNG width, and required time to terminal and to files

    cout << "\n";
    PrintFinal(cout, acceptance_count, acceptance_count_or, acceptance_count_hmc, epsilon, end_time, elapsed_seconds);

    PrintFinal(datalog, acceptance_count, acceptance_count_or, acceptance_count_hmc, epsilon, end_time, elapsed_seconds);
    datalog.close();
    datalog.clear();

    datalog.open(parameterfilepath, std::fstream::out | std::fstream::app);
    PrintFinal(datalog, acceptance_count, acceptance_count_or, acceptance_count_hmc, epsilon, end_time, elapsed_seconds);
    datalog.close();
    datalog.clear();

    PrintFinal(wilsonlog, acceptance_count, acceptance_count_or, acceptance_count_hmc, epsilon, end_time, elapsed_seconds);
    wilsonlog.close();
    wilsonlog.clear();
}
