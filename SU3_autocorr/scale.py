import math
import numpy as np


# Original Necco Sommer scale parameters
c0 = 1.6804
c1 = 1.7331
c2 = -0.7849
c3 = 0.4428

# Rational approximation (Stephan, Christian, Fodor, Kurth) parameters
d1 = -8.2384
d2 = 15.310
d3 = -2.7395
d4 = -11.526

beta = float(input("Please enter beta: "))
print("beta =", beta)

L = float(input("Please enter lattice length: "))
print("L =", L)

# Returns r_0/a (r_0 is approximately 0.5fm)
def polynomial_approx(beta, c0, c1, c2, c3):
    return np.exp(c0 + c1 * (beta - 6) + c2 * (beta - 6)**2 + c3 * (beta - 6)**3)

# Returns r_0/a (r_0 is approximately 0.5fm)
def rational_approx(beta, d1, d2, d3, d4):
    return np.exp(4 * math.pi**2/33 * beta * (1 + d1/beta + d2/beta**2)/(1 + d3/beta + d4/beta**2))

# Compute lattice spacing in fm
spacing1 = 0.5 / polynomial_approx(beta, c0, c1, c2, c3)
spacing2 = 0.5 / rational_approx(beta, d1, d2, d3, d4)
length1 = L * spacing1
length2 = L * spacing2
# \hbar * c taken from PDG booklet (2021)
hbar_c = 197.3269804
temp1 = hbar_c/length1
temp2 = hbar_c/length2

print("Necco Sommer based:")
print("Lattice spacing a in fm:", spacing1)
print("Lattice length in fm:", length1)
print("Corresponding temperature in MeV:", temp1)
print("\nWuppertal based:")
print("Lattice spacing a in fm:", spacing2)
print("Lattice length in fm:", length2)
print("Corresponding temperature in MeV:", temp2)
