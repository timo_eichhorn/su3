import math
import random
import time
import sys
import os
import shutil
import numpy as np
from scipy import special, optimize
import matplotlib.pyplot as plt
import matplotlib as mpl
#from mpl_toolkits.axes_grid1 import make_axes_locatable
import seaborn as sbn
import re

#-----
#Current version
current_version = "SU(3)_version_1.1"

#Thermalization phase (discard all expectation values before therm)
therm = 40

#Random seed
seed = 12345
#random.seed(seed)
np.random.seed(seed)

#Number or bootstrap resamples
n_resample = 1000

#Size of bootstrap blocks
blocksize = 1

#Number of reweight steps between two simulates betas
#reweight_steps = 10

#Matplotlib rc settings
mpl.rcParams["text.usetex"] = "True"
mpl.rcParams["font.family"] = "Computer Modern"
mpl.rcParams["xtick.direction"] = "in"
mpl.rcParams["ytick.direction"] = "in"

#Colormap for plots
#plt.set_cmap("jet")            #Red and blue
#plt.set_cmap("viridis")        #Yellow and purple
plt.set_cmap("cividis")         #Yellow and blue

#-----
#Get user input on what to do

print("\nData analysis program for " + current_version + "\n")

#-----
#Reasonable sorting (not in ASCII order)

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

#-----
#Go through all subdirectories

main_dir = os.getcwd()
directories = [os.path.abspath(x[0]) for x in os.walk(main_dir) if "log.txt" in x[2]]
directories.sort(key = natural_keys)
n_subdirectories = len(directories) #might be possible to read from parameters file, however then limited to fixed number of simulations

#-----
#Get values for n_run and expectation_period from parameters.txt

lattice_size_x_list = []
lattice_size_y_list = []
lattice_size_z_list = []
lattice_size_t_list = []
n_beta_increments_list = []
n_run_list = []
expectation_period_list = []

for a in directories:
    if os.path.isfile(os.path.join(a, "parameters.txt")) == True:
        with open(os.path.join(a, "parameters.txt")) as paramfile:
            first_line = paramfile.readline().strip()
            if first_line == current_version:
                print("Compatible file found in " + a + "\n")
                n_run = None
                expectation_period = None
                for line in paramfile:
                    if not (len(line.strip()) == 0):
                        if line.split()[0] == "Nx":
                            lattice_size_x = int(line.split()[2])
                            lattice_size_x_list.append(lattice_size_x)
                        if line.split()[0] == "Ny":
                            lattice_size_y = int(line.split()[2])
                            lattice_size_y_list.append(lattice_size_y)
                        if line.split()[0] == "Nz":
                            lattice_size_z = int(line.split()[2])
                            lattice_size_z_list.append(lattice_size_z)
                        if line.split()[0] == "Nt":
                            lattice_size_t = int(line.split()[2])
                            lattice_size_t_list.append(lattice_size_t)
                        if line.split()[0] == "n_beta_increments":
                            n_beta_increments = int(line.split()[2])
                            n_beta_increments_list.append(n_beta_increments)
                        if line.split()[0] == "n_run":
                            n_run = int(line.split()[2])
                            n_run_list.append(n_run)
                        if line.split()[0] == "expectation_period":
                            expectation_period = int(line.split()[2])
                            expectation_period_list.append(expectation_period)
                        if (not n_run is None) and (not expectation_period is None):
                            break
            else:
                print("Incompatible file found in " + a)
                print("File version: " + first_line)
                print("Program version: " + current_version + "\n")
    else:
        print("Could not find file \"log.txt\" in the current directory " + a)

#-----
#Exit program if differing values for n_run or expectation_period are found

abort = False

if len(set(lattice_size_x_list)) != 1:
    lattice_size_x = lattice_size_x_list[0]
    print("Differing values for lattice_size_x found")
    abort = True
if len(set(lattice_size_y_list)) != 1:
    lattice_size_y = lattice_size_y_list[0]
    print("Differing values for lattice_size_y found")
    abort = True
if len(set(lattice_size_z_list)) != 1:
    lattice_size_z = lattice_size_z_list[0]
    print("Differing values for lattice_size_z found")
    abort = True
if len(set(lattice_size_t_list)) != 1:
    lattice_size_t = lattice_size_t_list[0]
    print("Differing values for lattice_size_t found")
    abort = True
# if len(set(n_beta_increments_list)) != 1:
#   print("Differing values for n_beta_increments found")
#   abort = True
elif n_beta_increments_list[0] != n_subdirectories:
    print("Differing values for n_beta_increments and n_subdirectories found")
    user_choice_abort = input("Keep going (1) or abort program (2): ")
    if user_choice_abort == "2":
        abort = True
if len(set(n_run_list)) != 1:
    print("Differing values for n_run found")
    abort = True
if len(set(expectation_period_list)) != 1:
    print("Differing values for expectation_period found")
    abort = True

if abort:
    print("Exiting program")
    sys.exit()

#Number of expectation values with and without thermalization phase

n_expectation_values_therm = int(n_run/expectation_period)
n_expectation_values = n_expectation_values_therm - therm

if n_expectation_values % blocksize != 0:
    print("Number of expectation values is not a multiple of blocksize")
    print("Exiting program")
    sys.exit()

#Create directories for output

print("Creating directories for analysis results")

analysis_directory = "analysis_results"
raw_data_path = os.path.join(analysis_directory, "raw_data")
final_data_path = os.path.join(analysis_directory, "final_results")
#histogram_data_path = os.path.join(analysis_directory, "histograms")
#reweighted_data_path = os.path.join(analysis_directory, "reweighted")

os.mkdir(analysis_directory)
os.mkdir(raw_data_path)
os.mkdir(final_data_path)
#os.mkdir(histogram_data_path)
#os.mkdir(reweighted_data_path)

print("\n" + "-"*min(50, shutil.get_terminal_size().columns) + "\n")

#-----

#Setup arrays for all observables read directly from logfile

action_raw = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared1 = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared2 = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared3 = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared4 = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared5 = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared6 = np.zeros((n_subdirectories, n_expectation_values_therm))
action_smeared7 = np.zeros((n_subdirectories, n_expectation_values_therm))
beta_list = np.zeros(n_subdirectories)

#-----
#Bootstrap
#Generates bootstrap_array which can be used as weight

bootstrap_array = np.zeros((n_resample, n_expectation_values))

n_blocks = n_expectation_values // blocksize
bootstrap_array[0,] = 1
for i in range(1, n_resample):
    for j in range(n_blocks):
        bs_index = np.random.randint(0, n_expectation_values)
        for k in range(blocksize):
            bootstrap_array[i, (bs_index + k) % n_expectation_values] += 1

#Function for bootstrapping observable
#input consists of raw data of observable and the already calculated expectation values of the observable
#works with observables of variable length (e.g. correlation functions)
#TODO: might not yet work with multiple betas

def bootstrap(observable_raw, observable_expectation_value):    
    try:
        observable_length = np.size(observable_raw, axis = 2)
        observable_resampled = np.zeros((n_resample, n_subdirectories, observable_length))
    except:
        observable_length = 1
        print("Observable length along axis 2 not defined, using 1 as observable length instead.")
        observable_resampled = np.zeros((n_resample, n_subdirectories))
    for i in range(0, n_resample):
        observable_resampled[i] = np.average(observable_raw, axis = 1, weights = bootstrap_array[i])
    observable_error = np.sqrt(np.var(observable_resampled, axis = 0))
    observable_final = np.array([observable_expectation_value, observable_error])
    return observable_final, observable_resampled

#-----
#Get all values for observables and plot first and final configurations

for count_sim, a in enumerate(directories):

    if os.path.isfile(os.path.join(a, "log.txt")):
        with open(os.path.join(a, "log.txt")) as logfile:
            if first_line == current_version:
                print("Compatible file found in " + a)
                print("Reading observables from logfile")
                count_mc = -1
                for line in logfile:
                    if not (len(line.strip()) == 0):
                        if (len(line.strip()) > 2):
                            if line.split()[0] == "beta":
                                beta_list[count_sim] = float(line.split()[2])
                        if line.split()[0] == "Wilson_Action:":
                            action_raw[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared1:":
                            action_smeared1[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared2:":
                            action_smeared2[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared3:":
                            action_smeared3[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared4:":
                            action_smeared4[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared5:":
                            action_smeared5[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared6:":
                            action_smeared6[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Smeared7:":
                            action_smeared7[count_sim][count_mc] = float(line.split()[1])
                        if line.split()[0] == "Step":
                            count_mc += 1
            else:
                print("Incompatible version in " + a)
    else:
        print("Could not find file \"log.txt\" in the current directory " + a)

    #-----
    #Plot thermalization phase

    plt.plot(np.arange(0, 2*therm), action_raw[count_sim,0:2*therm], color = "#469FE5", linewidth = 0.2)
    plt.xlabel("Monte Carlo time")
    plt.ylabel(r"$S_{g}$")
    plt.title(r"Wilson action time series (%d $\times$ %d $\times$ %d $\times$ %d lattice)" % (lattice_size_x, lattice_size_y, lattice_size_z, lattice_size_t))
    plt.savefig(os.path.join(a, "action_thermalization.pdf"), bbox_inches = "tight")
    plt.close()

    #-----
    #Calculate and plot spatial correlation function

    # correlation_raw = np.loadtxt(os.path.join(a, "correlation.txt"), dtype = int, delimiter = ",", skiprows = 13 + therm)
    # correlation_function = np.mean(correlation_raw, axis = 0)
    # correlation_function_final, correlation_function_resampled = bootstrap(correlation_raw, correlation_function)

    # plt.fill_between(x = np.arange(0, lattice_size), y1 = correlation_function_final[0,] + correlation_function_final[1,], y2 = correlation_function_final[0,] - correlation_function_final[1,], color = "#469FE5", alpha = 0.2)
    # plt.plot(np.arange(0, lattice_size), correlation_function_final[0,], color = "#469FE5", linewidth = 0.2)#, label = "Correlation function")
    # plt.ylim((-0.1, 1.2))
    # plt.xlabel(r"$r$")
    # plt.ylabel(r"$C_{r}$")
    # plt.title(r"Correlation function (%d $\times$ %d lattice)" % (lattice_size, lattice_size))
    # #plt.legend()
    # plt.savefig(os.path.join(a, "correlation_function.pdf"), bbox_inches = "tight")
    # plt.close()

    # np.savetxt(os.path.join(a, "correlation_function.txt"), correlation_function_final, delimiter = ",")

#-----
#Output raw data as .txt or .csv

np.savetxt(os.path.join(raw_data_path, "action_raw.txt"), action_raw.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared1.txt"), action_smeared1.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared2.txt"), action_smeared2.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared3.txt"), action_smeared3.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared4.txt"), action_smeared4.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared5.txt"), action_smeared5.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared6.txt"), action_smeared6.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_smeared7.txt"), action_smeared7.T, delimiter = ",")

#-----
#Remove thermalization phase from observables

action_raw = action_raw[:,therm:]
action_smeared1 = action_smeared1[:,therm:]
action_smeared2 = action_smeared2[:,therm:]
action_smeared3 = action_smeared3[:,therm:]
action_smeared4 = action_smeared4[:,therm:]
action_smeared5 = action_smeared5[:,therm:]
action_smeared6 = action_smeared6[:,therm:]
action_smeared7 = action_smeared7[:,therm:]

#-----
#Reweighting
#Initial approximation of logarithm of partition functions

start = time.time()

#Calculate observables and errors of observables

action_ev, action_resampled = bootstrap(action_raw, np.mean(action_raw, axis = 1))
action_s1_ev, action_s1_resampled = bootstrap(action_smeared1, np.mean(action_smeared1, axis = 1))
action_s2_ev, action_s2_resampled = bootstrap(action_smeared2, np.mean(action_smeared2, axis = 1))
action_s3_ev, action_s3_resampled = bootstrap(action_smeared3, np.mean(action_smeared3, axis = 1))
action_s4_ev, action_s4_resampled = bootstrap(action_smeared4, np.mean(action_smeared4, axis = 1))
action_s5_ev, action_s5_resampled = bootstrap(action_smeared5, np.mean(action_smeared5, axis = 1))
action_s6_ev, action_s6_resampled = bootstrap(action_smeared6, np.mean(action_smeared6, axis = 1))
action_s7_ev, action_s7_resampled = bootstrap(action_smeared7, np.mean(action_smeared7, axis = 1))

#Create final arrays with errors

# action_final = np.array([beta_list, action_ev]).T

# print("beta_list.shape", beta_list.shape)
# print("action_ev[0,].shape", action_ev[0,].shape)
# print("action_ev[1,].shape", action_ev[1,].shape)

action_final = np.array([beta_list, action_ev[0,], action_ev[1]]).T
action_s1_final = np.array([beta_list, action_s1_ev[0,], action_s1_ev[1]]).T
action_s2_final = np.array([beta_list, action_s2_ev[0,], action_s2_ev[1]]).T
action_s3_final = np.array([beta_list, action_s3_ev[0,], action_s3_ev[1]]).T
action_s4_final = np.array([beta_list, action_s4_ev[0,], action_s4_ev[1]]).T
action_s5_final = np.array([beta_list, action_s5_ev[0,], action_s5_ev[1]]).T
action_s6_final = np.array([beta_list, action_s6_ev[0,], action_s6_ev[1]]).T
action_s7_final = np.array([beta_list, action_s7_ev[0,], action_s7_ev[1]]).T

#-----
#Save final arrays with errors

np.savetxt(os.path.join(final_data_path, "action.txt"), action_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s1.txt"), action_s1_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s2.txt"), action_s2_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s3.txt"), action_s3_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s4.txt"), action_s4_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s5.txt"), action_s5_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s6.txt"), action_s6_final, delimiter = ",")
np.savetxt(os.path.join(final_data_path, "action_s7.txt"), action_s7_final, delimiter = ",")

#-----
#Plot final results

def plot_final(observable_array, file_name, title_name, observable_name):

    plt.fill_between(x = observable_array[:,0], y1 = observable_array[:,1] + observable_array[:,2], y2 = observable_array[:,1] - observable_array[:,2], color = "#469FE5", alpha = 0.2)
    #plt.plot(observable_array[:,0], observable_array[:,1], color = "#469FE5", linewidth = 0.2, label = title_name)
    plt.plot(observable_array[:,0], observable_array[:,1], color = "#469FE5", linewidth = 0.2)
    plt.xlabel(r"$\beta$")
    plt.ylabel(observable_name)
    #plt.title(title_name + r" (%d $\times$ %d lattice)" % (lattice_size, lattice_size))
    plt.savefig(os.path.join(final_data_path, file_name + "_final.pdf"), bbox_inches = "tight")
    plt.close()

plot_final(action_final, "action", "Action", r"$S_{g}$")
plot_final(action_s1_final, "action_s1", "Action (smeared 1)", r"$S_{g}$")
plot_final(action_s2_final, "action_s2", "Action (smeared 2)", r"$S_{g}$")
plot_final(action_s3_final, "action_s3", "Action (smeared 3)", r"$S_{g}$")
plot_final(action_s4_final, "action_s4", "Action (smeared 4)", r"$S_{g}$")
plot_final(action_s5_final, "action_s5", "Action (smeared 5)", r"$S_{g}$")
plot_final(action_s6_final, "action_s6", "Action (smeared 6)", r"$S_{g}$")
plot_final(action_s7_final, "action_s7", "Action (smeared 7)", r"$S_{g}$")

print("Naive error:", np.std(action_raw))
print("Length of action_raw:", action_raw.shape)

end = time.time()
print("\n")
print("Total time:", end - start)
