// Pure SU(3) theory
// Debug Flags: -DDEBUG_MODE_TERMINAL, -DFIXED_SEED

#include <utility>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <filesystem>
#include <string>
#include <ctime>
#include <chrono>
#include <complex>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <unsupported/Eigen/MatrixFunctions>
#include <Eigen/Dense>
#include <random>
#include "pcg_random.hpp"

//-----

std::string program_version = "SU(3)_version_1.1";

//-----

#define Nt 32
#define Nx Nt
#define Ny Nt
#define Nz Nt
#define Nmu_max 4

const std::complex<float> I(0, 1);

//-----

std::string LatticeSizeString;                              // Lattice size (string)
int n_run;                                                  // Number of runs
double n_run_inverse;                                       // Inverse number of runs
//int n_count {0};                                          // Current run
int expectation_period;                                     // Number of updates between calculation of expectation values
int multi_hit {8};                                          // Number of hits per site in Metropolis algorithm
int n_smear_max {7};                                        // Maximum number of smearing steps
double update_norm = 1.0 / (Nt * Nx * Ny * Nz * 4.0 * multi_hit);
int append;                                                 // Directory name appendix
std::string appendString;                                   // Directory name appendix (string)
std::string directoryname_pre;                              // Directory name (prefix)
std::string directoryname;                                  // Directory name
std::string logfilepath;                                    // Filepath (log)
std::string parameterfilepath;                              // Filepath (parameters)
std::string energydensityfilepath;                          // Filepath (energydensity)
std::string correlationfilepath;                            // Filepath (correlation function)
std::string correlationfilepath2;                           // Filepath (correlation function 2)
auto start = std::chrono::system_clock::now();              // Start time
float beta;                                                 // Coupling
std::string betaString;                                     // Coupling (string)
int n_beta_increments;                                      // How often beta is incremented
float beta_increments_distance;                             // Distance between each beta increment
std::ofstream datalog;                                      // Output stream to save data
std::ofstream energydensitylog;                             // Output stream to save data
std::ofstream correlationlog;                               // Output stream to save correlation function
std::ofstream correlationlog2;                              // Output stream to save correlation function 2
//std::random_device randomdevice;                          // Creates random device to seed PRNGs
pcg_extras::seed_seq_from<std::random_device> seed_source;  // Seed source to seed PRNGs
#ifdef FIXED_SEED                                           // PRNG for random coordinates and probabilites
pcg64 generator_rand(1);
#else
pcg64 generator_rand(seed_source);
#endif
double acceptance_rate;                                     // Acceptance rate for new configurations

//-----

using std::cin;
using std::cout;
using std::endl;
using std::to_string;
using std::conj;
using std::array;

typedef Eigen::Matrix<std::complex<float>, 3, 3> SU3_matrix;
typedef array<array<float, Nt>, Nmu_max> Correlation_Vector;
typedef array<array<array<float, Nt>, Nmu_max>, Nmu_max> Correlation_Matrix;
typedef array<array<array<array<array<SU3_matrix, 4>, Nz>, Ny>, Nx>, Nt> Gl_Lattice;
typedef array<array<array<array<array<array<SU3_matrix, 3>, 3>, Nz>, Ny>, Nx>, Nt> Spatial_tensor;
typedef array<array<array<array<array<array<SU3_matrix, 4>, 4>, Nz>, Ny>, Nx>, Nt> Full_tensor;

//-----

Gl_Lattice* Gluon = new Gl_Lattice;
Gl_Lattice* Gluonsmeared1 = new Gl_Lattice;
Gl_Lattice* Gluonsmeared2 = new Gl_Lattice;
Full_tensor* F = new Full_tensor;
Full_tensor* Q = new Full_tensor;
Correlation_Vector CV;
Correlation_Matrix CM;

//-----
// Better complex numbers?

// Trick to allow type promotion below
// template <typename T>
// struct identity_t { typedef T type; };

// // Make working with std::complex<> numbers suck less... allow promotion.
// #define COMPLEX_OPS(OP)                                                 \
//   template <typename _Tp>                                               \
//   std::complex<_Tp>                                                     \
//   operator OP(std::complex<_Tp> lhs, const typename identity_t<_Tp>::type & rhs) \
//   {                                                                     \
//     return lhs OP rhs;                                                  \
//   }                                                                     \
//   template <typename _Tp>                                               \
//   std::complex<_Tp>                                                     \
//   operator OP(const typename identity_t<_Tp>::type & lhs, const std::complex<_Tp> & rhs) \
//   {                                                                     \
//     return lhs OP rhs;                                                  \
//   }
// COMPLEX_OPS(+)
// COMPLEX_OPS(-)
// COMPLEX_OPS(*)
// COMPLEX_OPS(/)
// #undef COMPLEX_OPS

//-----
// Overload << for vectors and arrays?

// template <typename T>
// std::ostream& operator<<(ostream& out, const std::vector<T>& container)
// {
//     out << "Container dump begins: ";
//     std::copy(container.cbegin(), container.cend(), std::ostream_iterator<T>(out, " "));
//     out << "\n";
//     return out;
// }

// template <typename T>
// std::ostream& operator<<(ostream& out, const std::array<T>& container)
// {
//     out << "Container dump begins: ";
//     std::copy(container.cbegin(), container.cend(), std::ostream_iterator<T>(out, " "));
//     out << "\n";
//     // std::copy(container.cbegin(), std::prev(container.cend()), std::ostream_iterator<T>(correlationlog, ","));
//     // correlationlog << Correlation_Function.back() << "\n";
//     return out;
// }

//-------------------------------------------------------------------------------------

//-----
// Receives simulation parameters from user input

void Configuration()
{
    cout << "\n" << "Please enter beta (initial value): ";
    cin >> beta;
    cout << "\n" << "Please enter n_beta_increments: ";
    cin >> n_beta_increments;
    cout << "\n" << "Please enter beta_increments_distance: ";
    cin >> beta_increments_distance;
    cout << "\n" << "Please enter n_run: ";
    cin >> n_run;
    n_run_inverse = 1 / double (n_run);
    cout << "\n" << "Please enter expectation_period: ";
    cin >> expectation_period;
    cout << "\n" << "beta is " << beta << ".\n";
    cout << "n_run is " << n_run << " and expectation_period is " << expectation_period << ".\n";
    cout << "multi_hit is " << multi_hit << ".\n";
    //cout << "Overall, there are " <<  << " lattice sites.\n\n";
}

//-----
// Writes simulation parameters to files

void SaveParameters(std::string filename, const std::string& starttimestring)
{
    datalog.open(filename, std::fstream::out | std::fstream::app);
    datalog << program_version << "\n";
    datalog << "logfile\n\n";
    #ifdef DEBUG_MODE_TERMINAL
    datalog << "DEBUG_MODE_TERMINAL\n";
    #endif
    datalog << starttimestring << "\n";
    datalog << "Nx = " << Nx << "\n";
    datalog << "Ny = " << Ny << "\n";
    datalog << "Nz = " << Nz << "\n";
    datalog << "Nt = " << Nt << "\n";
    datalog << "beta = " << beta << "\n";
    datalog << "n_beta_increments = " << n_beta_increments << "\n";
    datalog << "n_run = " << n_run << "\n";
    datalog << "expectation_period = " << expectation_period << "\n\n";
    datalog.close();
}

//-----
// Creates directories and files to store data

void CreateFiles()
{
    LatticeSizeString = to_string(Nx) + "x" + to_string(Ny) + "x" + to_string(Nz) + "x" + to_string(Nt);
    betaString = to_string(beta);
    directoryname_pre = "SU(3)_N=" + LatticeSizeString + "_beta=" + betaString;
    directoryname = directoryname_pre;
    append = 1;

    while (std::filesystem::exists(directoryname) == true)
    {
        appendString = to_string(append);
        directoryname = directoryname_pre + " (" + appendString + ")";
        ++append;
    }

    std::filesystem::create_directory(directoryname);
    cout << "\n\n" << "Created directory \"" << directoryname << "\".\n";
    logfilepath = directoryname + "/log.txt";
    parameterfilepath = directoryname + "/parameters.txt";
    energydensityfilepath = directoryname + "/energydensity.txt";
    //correlationfilepath = directoryname + "/correlation.txt";
    cout << "Filepath (log):\t\t" << logfilepath << "\n";
    cout << "Filepath (parameters):\t\t" << parameterfilepath << "\n";
    cout << "Filepath (energydensity):\t\t" << energydensityfilepath << "\n";
    //cout << "Filepath (correlation):\t" << correlationfilepath << "\n";
    #ifdef DEBUG_MODE_TERMINAL
    cout << "DEBUG_MODE_TERMINAL\n\n";
    #endif

    //-----
    // Writes parameters to files

    std::time_t start_time = std::chrono::system_clock::to_time_t(start);
    std::string start_time_string = std::ctime(&start_time);

    // logfile

    SaveParameters(logfilepath, start_time_string);

    // parameterfile

    SaveParameters(parameterfilepath, start_time_string);

    // energydensityfile

    SaveParameters(energydensityfilepath, start_time_string);

    // correlationfile

    SaveParameters(correlationfilepath, start_time_string);

    // correlationfile2

    SaveParameters(correlationfilepath2, start_time_string);
}

//-----
// Set gauge fields to identity

void SetGluonToOne(Gl_Lattice& Gluon)
{
    for (auto &ind0 : Gluon)
    for (auto &ind1 : ind0)
    for (auto &ind2 : ind1)
    for (auto &ind3 : ind2)
    for (auto &GluonMatrix : ind3)
    {
        GluonMatrix.setIdentity();
    }
    cout << "Gauge Fields set to identity!" << endl;
}

//-----
// Generates random 3 x 3 matrices

inline SU3_matrix RandomSU3(const float epsilon, std::uniform_int_distribution<int>& distribution_choice, std::uniform_real_distribution<float>& distribution_unitary)
{
    SU3_matrix A;

    int choice = distribution_choice(generator_rand);
    float phi = distribution_unitary(generator_rand);
    float s_phi = sin(phi);
    float c_phi = cos(phi);

    switch(choice)
    {
        case 1:
        {
            A << c_phi, I * s_phi, 0.0,
                 I * s_phi, c_phi, 0.0,
                 0.0, 0.0, 1.0;
        }
        break;
        case 2:
        {
            A << c_phi, -s_phi, 0.0,
                 s_phi, c_phi, 0.0,
                 0.0, 0.0, 1.0;
        }
        break;
        case 3:
        {
            std::complex<float> exp_I_phi = exp(I * phi);
            A << exp_I_phi, 0.0, 0.0,
                 0.0, conj(exp_I_phi), 0.0,
                 0.0, 0.0, 1.0;
        }
        break;
        case 4:
        {
            A << c_phi, 0.0, I * s_phi,
                 0.0, 1.0, 0.0,
                 I * s_phi, 0.0, c_phi;
        }
        break;
        case 5:
        {
            A << c_phi, 0.0, s_phi,
                 0.0, 1.0, 0.0,
                 -s_phi, 0.0, c_phi;
        }
        break;
        case 6:
        {
            A << 1.0, 0.0, 0.0,
                 0.0, c_phi, I * s_phi,
                 0.0, I * s_phi, c_phi;
        }
        break;
        case 7:
        {
            A << 1.0, 0.0, 0.0,
                 0.0, c_phi, -s_phi,
                 0.0, s_phi, c_phi;
        }
        break;
        case 8:
        {
            float phi_tilde = phi / sqrt(3.0);
            std::complex<float> exp_phi_tilde = exp(I * phi_tilde);
            A << exp_phi_tilde, 0.0, 0.0,
                 0.0, exp_phi_tilde, 0.0,
                 0.0, 0.0, (std::complex<float>(1.0, 0))/(exp_phi_tilde * exp_phi_tilde);
        }
        break;
    }
    return A;
}

//-----
// Calculates staple at given coordinates

SU3_matrix Staple(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu)
{
    SU3_matrix st;

    int tp = (t + 1)%Nt;
    int tm = (t - 1 + Nt)%Nt;

    int xp = (x + 1)%Nx;
    int xm = (x - 1 + Nx)%Nx;

    int yp = (y + 1)%Ny;
    int ym = (y - 1 + Ny)%Ny;

    int zp = (z + 1)%Nz;
    int zm = (z - 1 + Nz)%Nz;

    switch(mu)
    {
        case 0:
        {
            st = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][0] * Gluon[tp][x][y][z][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][0] * Gluon[tp][xm][y][z][1]
               + Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][0] * Gluon[tp][x][y][z][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][0] * Gluon[tp][x][ym][z][2]
               + Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][0] * Gluon[tp][x][y][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][0] * Gluon[tp][x][y][zm][3];
        }
        break;

        case 1:
        {
            st = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][1] * Gluon[tm][xp][y][z][0]
               + Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][1] * Gluon[t][xp][y][z][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][1] * Gluon[t][xp][ym][z][2]
               + Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][1] * Gluon[t][xp][y][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][1] * Gluon[t][xp][y][zm][3];
        }
        break;

        case 2:
        {
            st = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][2] * Gluon[tm][x][yp][z][0]
               + Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][2] * Gluon[t][xm][yp][z][1]
               + Gluon[t][x][y][z][3] * Gluon[t][x][y][zp][2] * Gluon[t][x][yp][z][3].adjoint() + Gluon[t][x][y][zm][3].adjoint() * Gluon[t][x][y][zm][2] * Gluon[t][x][yp][zm][3];
        }
        break;

        case 3:
        {
            st = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() + Gluon[tm][x][y][z][0].adjoint() * Gluon[tm][x][y][z][3] * Gluon[tm][x][y][zp][0]
               + Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() + Gluon[t][xm][y][z][1].adjoint() * Gluon[t][xm][y][z][3] * Gluon[t][xm][y][zp][1]
               + Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() + Gluon[t][x][ym][z][2].adjoint() * Gluon[t][x][ym][z][3] * Gluon[t][x][ym][zp][2];
        }
        break;
    }
    return st;
}

//-----

float SLocal(const SU3_matrix& U, SU3_matrix& st)
{
    //return beta/3.0 * std::real((SU3_matrix::Identity() - U * st.adjoint()).trace());
    return beta/3.0 * (3 - std::real((U * st.adjoint()).trace()));
}

//-----
// Calculates plaquette at given coordinates

SU3_matrix Plaquette(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu, const int nu)
{
    SU3_matrix pl;

    int tp = (t + 1)%Nt;
    int xp = (x + 1)%Nx;
    int yp = (y + 1)%Ny;
    int zp = (z + 1)%Nz;

    switch(nu)
    {
        case 0:
        {
            pl << 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0;
        }
        break;

        case 1:
        {
            pl = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][1] * Gluon[t][xp][y][z][0].adjoint() * Gluon[t][x][y][z][1].adjoint();
        }
        break;

        case 2:
        {
            if (mu == 0)
            {
                pl = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][2] * Gluon[t][x][yp][z][0].adjoint() * Gluon[t][x][y][z][2].adjoint();
            }
            if (mu == 1)
            {
                pl = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][2] * Gluon[t][x][yp][z][1].adjoint() * Gluon[t][x][y][z][2].adjoint();
            }
        }
        break;

        case 3:
        {
            if(mu == 0)
            {
                pl = Gluon[t][x][y][z][0] * Gluon[tp][x][y][z][3] * Gluon[t][x][y][zp][0].adjoint() * Gluon[t][x][y][z][3].adjoint();
            }
            if(mu == 1)
            {
                pl = Gluon[t][x][y][z][1] * Gluon[t][xp][y][z][3] * Gluon[t][x][y][zp][1].adjoint() * Gluon[t][x][y][z][3].adjoint();
            }
            if(mu == 2)
            {
                pl = Gluon[t][x][y][z][2] * Gluon[t][x][yp][z][3] * Gluon[t][x][y][zp][2].adjoint() * Gluon[t][x][y][z][3].adjoint();
            }
        }
        break;
    }
    return pl;    
}

//-----
// Calculates Plaquette of size Nmu x Nmu at given coordinates

SU3_matrix BigPlaquette(const Gl_Lattice& Gluon, const int t, const int x, const int y, const int z, const int mu, const int nu, const int Nmu)
{
    SU3_matrix pl1;
    SU3_matrix pl2;
    SU3_matrix pl3;
    SU3_matrix pl4;

    if (mu == 1 && nu == 2)
    {
        for (int i = 0; i < Nmu; ++i)
        {
            pl1 *= Gluon[t][(x + i)%Nx][y][z][mu];
            pl2 *= Gluon[t][(x + Nmu)%Nx][(y + i)%Ny][z][nu];
            pl3 *= Gluon[t][(x + i)%Nx][(y + Nmu)%Ny][z][mu];
            pl4 *= Gluon[t][x][(y + i)%Ny][z][nu];
        }
    }
    else if (mu == 2 && nu == 3)
    {
        for (int i = 0; i < Nmu; ++i)
        {
            pl1 *= Gluon[t][x][(y + i)%Ny][z][mu];
            pl2 *= Gluon[t][x][(y + Nmu)%Ny][(z + i)%Nz][nu];
            pl3 *= Gluon[t][x][(y + i)%Ny][(z + Nmu)%Nz][mu];
            pl4 *= Gluon[t][x][y][(z + i)%Nz][nu];
        }
    }
    else if (mu == 3 && nu == 1)
    {
        for (int i = 0; i < Nmu; ++i)
        {
            pl1 *= Gluon[t][x][y][(z + i)%Nz][mu];
            pl2 *= Gluon[t][(x + i)%Nx][y][(z + Nmu)%Nz][nu];
            pl3 *= Gluon[t][(x + Nmu)%Nx][y][(z + i)%Nz][mu];
            pl4 *= Gluon[t][(x + i)%Nx][y][z][nu];
        }
    }
    else
    {
        cout << "Inorrect parameters in BigPlaquette funtion.\n";
        std::exit(1);
    }
    return pl1 * pl2 * pl3.adjoint() * pl4.adjoint();
}

//-----
// Calculates timeslice average

Correlation_Vector PlaquetteTimeslice(const Gl_Lattice& Gluon)
{
    Correlation_Vector P_timeslice_average;
    float spatial_norm = 1.0 / (Nx * Ny * Nz);
    for (int Nmu = 1; Nmu < Nmu_max + 1; ++Nmu)
    {
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int y = 0; y < Ny; ++y)
        for (int z = 0; z < Nz; ++z)
        {
            P_timeslice_average[Nmu - 1][t] += std::real((BigPlaquette(Gluon, t, x, y, z, 1, 2, Nmu) + BigPlaquette(Gluon, t, x, y, z, 2, 3, Nmu) + BigPlaquette(Gluon, t, x, y, z, 3, 1, Nmu)).trace());
        }
    }
    for (int Nmu = 1; Nmu < Nmu_max + 1; ++Nmu)
    for (int t = 0; t < Nt; ++t)
    {
        P_timeslice_average[Nmu - 1][t] *= spatial_norm;
    }
    // std::transform(P_timeslice_average.begin(), P_timeslice_average.end(), P_timeslice_average.begin(), [](const auto& element){return element * spatial_norm / 3.0;});
    // std::transform(P_timeslice_average.begin(), P_timeslice_average.end(), P_timeslice_average.begin(), [](const auto& element){return element * spatial_norm;});
    // for (auto& element : P_timeslice_average)
    // {
    //     element /= Nx * Ny * Nz;
    // }
    return P_timeslice_average;
}

//-----
// Calculates correlation function

void CorrelationFunction(const Gl_Lattice& Gluon, Correlation_Vector& CV, Correlation_Matrix& CM)
{
    cout << "test1 in corrfunc" << endl;
    Correlation_Vector TimesliceAverages = PlaquetteTimeslice(Gluon);
    cout << "test2 in corrfunc" << endl;
    for (int a = 0; a < Nmu_max + 1; ++a)
    for (int b = 0; b < Nmu_max + 1; ++b)
    for (int t = 0; t < Nt; ++t)
    {
        CM[a][b][t] = 0.0;
        for (int tau = 0; tau < Nt; ++tau)
        {
            CM[a][b][t] += TimesliceAverages[a][tau] * TimesliceAverages[b][(t + tau)%Nt];
        }
    }
    for (int a = 0; a < Nmu_max; ++a)
    for (int t = 0; t < Nt; ++t)
    {
        CV[a][t] = TimesliceAverages[a][t];
    }
}

void PrintCM(const Correlation_Matrix& CM, std::ofstream& correlationlog)
{
    for (int a = 0; a < Nmu_max; ++a)
    {
        for (int b = 0; b < Nmu_max; ++b)
        {
            for (int c = 0; c < Nt; ++c)
            {
                if (c < Nt - 1)
                {
                    correlationlog << CM[a][b][c] << ", ";
                }
                if (c == Nt - 1)
                {
                    correlationlog << CM[a][b][c] << "\n";
                }
            }
        }
        correlationlog << "\n";
    }
}

void PrintCV(const Correlation_Vector& CV, std::ofstream& correlationlog2)
{
    for (int a = 0; a < Nmu_max; ++a)
    {
        for (int b = 0; b < Nt; ++b)
        {
            if (b < Nt - 1)
            {
                correlationlog2 << CV[a][b] << ", ";
            }
            if (b == Nt - 1)
            {
                correlationlog2 << CV[a][b] << "\n";
            }
        }
    }
}


//-----
// Returns Wilson gauge action

float GaugeAction(const Gl_Lattice& Gluon)
{
    float S {0.0};
    SU3_matrix pl;

    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int nu = 1; nu < 4; ++nu)
    {
        for (int mu = 0; mu < nu; ++mu)
        {
            pl = Plaquette(Gluon, t, x, y, z, mu, nu);
            S += 1.0/3.0 * std::real((SU3_matrix::Identity() - pl).trace());
        }
    }
    S = S/(6 * Nt * Nx * Ny * Nz);
    return S;
}

//-----
// Projects matrices back on SU(3) via Gram-Schmidt

void ProjectionSU3(Gl_Lattice& Gluon)
{
    for (auto& ind0 : Gluon)
    for (auto& ind1 : ind0)
    for (auto& ind2 : ind1)
    for (auto& ind3 : ind2)
    for (auto& GluonMatrix : ind3)
    {
        float norm0 = 1.0/sqrt(std::real(GluonMatrix(0, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(0, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(0, 2) * conj(GluonMatrix(0, 2))));
        for (int i = 0; i < 3; ++i)
        {
            GluonMatrix(0, i) = norm0 * GluonMatrix(0, i);
        }
        std::complex<float> psi = GluonMatrix(1, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(0, 2));
        for (int i = 0; i < 3; ++i)
        {
            GluonMatrix(1, i) =  GluonMatrix(1, i) - psi * GluonMatrix(0, i);
        }
        float norm1 = 1.0/sqrt(std::real(GluonMatrix(1, 0) * conj(GluonMatrix(1, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(1, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(1, 2))));
        for (int i = 0; i < 3; ++i)
        {
            GluonMatrix(1, i) = norm1 * GluonMatrix(1, i);
        }
        GluonMatrix(2, 0) = conj(GluonMatrix(0, 1) * GluonMatrix(1, 2) - GluonMatrix(0, 2) * GluonMatrix(1, 1));
        GluonMatrix(2, 1) = conj(GluonMatrix(0, 2) * GluonMatrix(1, 0) - GluonMatrix(0, 0) * GluonMatrix(1, 2));
        GluonMatrix(2, 2) = conj(GluonMatrix(0, 0) * GluonMatrix(1, 1) - GluonMatrix(0, 1) * GluonMatrix(1, 0));
    }
}

// Projects a single link back on SU(3) via Gram-Schmidt

void ProjectionSU3Single(SU3_matrix& GluonMatrix)
{
    float norm0 = 1.0/sqrt(std::real(GluonMatrix(0, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(0, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(0, 2) * conj(GluonMatrix(0, 2))));
    for (int i = 0; i < 3; ++i)
    {
        GluonMatrix(0, i) = norm0 * GluonMatrix(0, i);
    }
    std::complex<float> psi = GluonMatrix(1, 0) * conj(GluonMatrix(0, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(0, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(0, 2));
    for (int i = 0; i < 3; ++i)
    {
        GluonMatrix(1, i) =  GluonMatrix(1, i) - psi * GluonMatrix(0, i);
    }
    float norm1 = 1.0/sqrt(std::real(GluonMatrix(1, 0) * conj(GluonMatrix(1, 0)) + GluonMatrix(1, 1) * conj(GluonMatrix(1, 1)) + GluonMatrix(1, 2) * conj(GluonMatrix(1, 2))));
    for (int i = 0; i < 3; ++i)
    {
        GluonMatrix(1, i) = norm1 * GluonMatrix(1, i);
    }
    GluonMatrix(2, 0) = conj(GluonMatrix(0, 1) * GluonMatrix(1, 2) - GluonMatrix(0, 2) * GluonMatrix(1, 1));
    GluonMatrix(2, 1) = conj(GluonMatrix(0, 2) * GluonMatrix(1, 0) - GluonMatrix(0, 0) * GluonMatrix(1, 2));
    GluonMatrix(2, 2) = conj(GluonMatrix(0, 0) * GluonMatrix(1, 1) - GluonMatrix(0, 1) * GluonMatrix(1, 0));
}

//-----
// Metropolis update routine

void Update(Gl_Lattice& Gluon, const float epsilon, double& acceptance_rate, std::uniform_real_distribution<float>& distribution_prob, std::uniform_int_distribution<int>& distribution_choice, std::uniform_real_distribution<float>& distribution_unitary)
{
    SU3_matrix st;
    SU3_matrix RU;
    SU3_matrix old_conf;
    SU3_matrix new_conf;
    float s;
    float sprime;
    acceptance_rate = 0.0;

    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    {
        st = Staple(Gluon, t, x, y, z, mu);
        old_conf = Gluon[t][x][y][z][mu];
        s = SLocal(old_conf, st);
        for (int n_hit = 0; n_hit < multi_hit; ++n_hit)
        {
            RU = RandomSU3(epsilon, distribution_choice, distribution_unitary);

            new_conf = old_conf * RU;

            sprime = SLocal(new_conf, st);

            if (exp(-sprime + s) > distribution_prob(generator_rand))
            {
                Gluon[t][x][y][z][mu] = new_conf;
                old_conf = new_conf;
                s = sprime;
                // acceptance_rate += 1.0 * update_norm;
                acceptance_rate += 1.0;
            }
        }
    }
}

//-----
// Stout smearing of gluon fields in spatial directions

void StoutSmearing3D(const Gl_Lattice& Gluon_unsmeared, Gl_Lattice& Gluon_smeared)
{
    // Gl_Lattice Gluon_smeared;

    SU3_matrix Sigma;
    SU3_matrix A;
    SU3_matrix B;
    SU3_matrix C;
    SU3_matrix D;

    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        for (int mu = 1; mu < 4; ++mu)
        {
            Sigma = Staple(Gluon_unsmeared, t, x, y, z, mu);
            A = Sigma * Gluon_unsmeared[t][x][y][z][mu].adjoint();
            B = A.adjoint() - A;
            C = std::complex<float>(0.0, 0.5) * (B - std::complex<float>(1.0/3.0, 0.0) * B.trace() * SU3_matrix::Identity());
            D = (std::complex<float>(0.0, 0.12) * C).exp();
            Gluon_smeared[t][x][y][z][mu] = D * Gluon_unsmeared[t][x][y][z][mu];
        }
        Gluon_smeared[t][x][y][z][0] = Gluon_unsmeared[t][x][y][z][0];
    }
    ProjectionSU3(Gluon_smeared);
}

// Stout smearing of gluon fields in all 4 directions

void StoutSmearing4D(const Gl_Lattice& Gluon_unsmeared, Gl_Lattice& Gluon_smeared)
{
    // Gl_Lattice Gluon_smeared;

    SU3_matrix Sigma;
    SU3_matrix A;
    SU3_matrix B;
    SU3_matrix C;
    SU3_matrix D;

    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        for (int mu = 0; mu < 4; ++mu)
        {
            Sigma = Staple(Gluon_unsmeared, t, x, y, z, mu);
            A = Sigma * Gluon_unsmeared[t][x][y][z][mu].adjoint();
            B = A.adjoint() - A;
            C = std::complex<float>(0.0, 0.5) * (B - std::complex<float>(1.0/3.0, 0.0) * B.trace() * SU3_matrix::Identity());
            D = (std::complex<float>(0.0, 0.12) * C).exp();
            Gluon_smeared[t][x][y][z][mu] = D * Gluon_unsmeared[t][x][y][z][mu];
        }
    }
    ProjectionSU3(Gluon_smeared);
}

//-----
// Calculates clover term

void Clover(const Gl_Lattice& Gluon, Full_tensor& Q, const int Nmu)
{
    Gl_Lattice Gluonchain;
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        for (int mu = 0; mu < 4; ++mu)
        {
            Gluonchain[t][x][y][z][mu].setIdentity();
        }
        for (int i = 0; i < Nmu; ++ i)
        {
            Gluonchain[t][x][y][z][0] *= Gluon[(t + i)%Nt][x][y][z][0];
            Gluonchain[t][x][y][z][1] *= Gluon[t][(x + i)%Nx][y][z][1];
            Gluonchain[t][x][y][z][2] *= Gluon[t][x][(y + i)%Ny][z][2];
            Gluonchain[t][x][y][z][3] *= Gluon[t][x][y][(z + i)%Nz][3];
        }
    }

    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    {
        int tm = (t - Nmu + Nt)%Nt;
        int xm = (x - Nmu + Nx)%Nx;
        int ym = (y - Nmu + Ny)%Ny;
        int zm = (z - Nmu + Nz)%Nz;
        int tp = (t + Nmu)%Nt;
        int xp = (x + Nmu)%Nx;
        int yp = (y + Nmu)%Ny;
        int zp = (z + Nmu)%Nz;

        Q[t][x][y][z][0][0].setIdentity();
        Q[t][x][y][z][0][1] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][1] * Gluonchain[t][xp][y][z][0].adjoint() * Gluonchain[t][x][y][z][1].adjoint()
                + Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][0].adjoint() * Gluonchain[tm][x][y][z][1].adjoint() * Gluonchain[tm][x][y][z][0]
                + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][xm][y][z][1].adjoint() * Gluonchain[tm][xm][y][z][0] * Gluonchain[t][xm][y][z][1]
                + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][y][z][0] * Gluonchain[tp][xm][y][z][1] * Gluonchain[t][x][y][z][0].adjoint();
        Q[t][x][y][z][1][0] = Q[t][x][y][z][0][1].adjoint();

        Q[t][x][y][z][0][2] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][2] * Gluonchain[t][x][yp][z][0].adjoint() * Gluonchain[t][x][y][z][2].adjoint()
                + Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][0].adjoint() * Gluonchain[tm][x][y][z][2].adjoint() * Gluonchain[tm][x][y][z][0]
                + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][x][ym][z][2].adjoint() * Gluonchain[tm][x][ym][z][0] * Gluonchain[t][x][ym][z][2]
                + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][z][0] * Gluonchain[tp][x][ym][z][2] * Gluonchain[t][x][y][z][0].adjoint();
        Q[t][x][y][z][2][0] = Q[t][x][y][z][0][2].adjoint();

        Q[t][x][y][z][0][3] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][3] * Gluonchain[t][x][y][zp][0].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
                + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][0].adjoint() * Gluonchain[tm][x][y][z][3].adjoint() * Gluonchain[tm][x][y][z][0]
                + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][x][y][zm][3].adjoint() * Gluonchain[tm][x][y][zm][0] * Gluonchain[t][x][y][zm][3]
                + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][0] * Gluonchain[tp][x][y][zm][3] * Gluonchain[t][x][y][z][0].adjoint();
        Q[t][x][y][z][3][0] = Q[t][x][y][z][0][3].adjoint();

        Q[t][x][y][z][1][1].setIdentity();
        Q[t][x][y][z][1][2] = Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][2] * Gluonchain[t][x][yp][z][1].adjoint() * Gluonchain[t][x][y][z][2].adjoint()
                + Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][1].adjoint() * Gluonchain[t][xm][y][z][2].adjoint() * Gluonchain[t][xm][y][z][1]
                + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][ym][z][2].adjoint() * Gluonchain[t][xm][ym][z][1] * Gluonchain[t][x][ym][z][2]
                + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][z][1] * Gluonchain[t][xp][ym][z][2] * Gluonchain[t][x][y][z][1].adjoint();
        Q[t][x][y][z][2][1] = Q[t][x][y][z][1][2].adjoint();

        Q[t][x][y][z][1][3] = Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][3] * Gluonchain[t][x][y][zp][1].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
                + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][1].adjoint() * Gluonchain[t][xm][y][z][3].adjoint() * Gluonchain[t][xm][y][z][1]
                + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][y][zm][3].adjoint() * Gluonchain[t][xm][y][zm][1] * Gluonchain[t][x][y][zm][3]
                + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][1] * Gluonchain[t][xp][y][zm][3] * Gluonchain[t][x][y][z][1].adjoint();
        Q[t][x][y][z][3][1] = Q[t][x][y][z][1][3].adjoint();

        Q[t][x][y][z][2][2].setIdentity();
        Q[t][x][y][z][2][3] = Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][3] * Gluonchain[t][x][y][zp][2].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
                + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][2].adjoint() * Gluonchain[t][x][ym][z][3].adjoint() * Gluonchain[t][x][ym][z][2]
                + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][zm][3].adjoint() * Gluonchain[t][x][ym][zm][2] * Gluonchain[t][x][y][zm][3]
                + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][2] * Gluonchain[t][x][yp][zm][3] * Gluonchain[t][x][y][z][2].adjoint();
        Q[t][x][y][z][3][2] = Q[t][x][y][z][2][3].adjoint();
        Q[t][x][y][z][3][3].setIdentity();
    }
}

// void Clover(const Gl_Lattice& Gluon, Full_tensor& Q, const int Nmu)
// {
//     Gl_Lattice Gluonchain;
//     for (int t = 0; t < Nt; ++t)
//     for (int x = 0; x < Nx; ++x)
//     for (int y = 0; y < Ny; ++y)
//     for (int z = 0; z < Nz; ++z)
//     {
//         for (int mu = 0; mu < 4; ++mu)
//         {
//             Gluonchain[t][x][y][z][mu].setIdentity();
//         }
//         for (int i = 0; i < Nmu; ++ i)
//         {
//             Gluonchain[t][x][y][z][0] *= Gluon[(t + i)%Nt][x][y][z][0];
//             Gluonchain[t][x][y][z][1] *= Gluon[t][(x + i)%Nx][y][z][1];
//             Gluonchain[t][x][y][z][2] *= Gluon[t][x][(y + i)%Ny][z][2];
//             Gluonchain[t][x][y][z][3] *= Gluon[t][x][y][(z + i)%Nz][3];
//         }
//     }

//     #pragma omp parallel for
//     for (int t = 0; t < Nt; ++t)
//     for (int x = 0; x < Nx; ++x)
//     for (int y = 0; y < Ny; ++y)
//     for (int z = 0; z < Nz; ++z)
//     {
//         int tm = (t - Nmu + Nt)%Nt;
//         int xm = (x - Nmu + Nx)%Nx;
//         int ym = (y - Nmu + Ny)%Ny;
//         int zm = (z - Nmu + Nz)%Nz;
//         int tmm = (t - 2 * (Nmu + Nt))%Nt;
//         int xmm = (x - 2 * (Nmu + Nx))%Nx;
//         int ymm = (y - 2 * (Nmu + Ny))%Ny;
//         int zmm = (z - 2 * (Nmu + Nz))%Nz;
//         int tp = (t + Nmu)%Nt;
//         int xp = (x + Nmu)%Nx;
//         int yp = (y + Nmu)%Ny;
//         int zp = (z + Nmu)%Nz;

//         Q[t][x][y][z][0][0] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][0] * Gluonchain[tp][x][y][z][0].adjoint() * Gluonchain[t][x][y][z][0].adjoint()
//                 + Gluonchain[t][x][y][z][0] * Gluonchain[t][x][y][z][0].adjoint() * Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][x][y][z][0]
//                 + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tmm][x][y][z][0].adjoint() * Gluonchain[tmm][x][y][z][0] * Gluonchain[tm][x][y][z][0]
//                 + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][x][y][z][0] * Gluonchain[t][x][y][z][0] * Gluonchain[t][x][y][z][0].adjoint();
//         Q[t][x][y][z][0][1] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][1] * Gluonchain[t][xp][y][z][0].adjoint() * Gluonchain[t][x][y][z][1].adjoint()
//                 + Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][0].adjoint() * Gluonchain[tm][x][y][z][1].adjoint() * Gluonchain[tm][x][y][z][0]
//                 + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][xm][y][z][1].adjoint() * Gluonchain[tm][xm][y][z][0] * Gluonchain[t][xm][y][z][1]
//                 + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][y][z][0] * Gluonchain[tp][xm][y][z][1] * Gluonchain[t][x][y][z][0].adjoint();
//         Q[t][x][y][z][1][0] = Q[t][x][y][z][0][1].adjoint();

//         Q[t][x][y][z][0][2] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][2] * Gluonchain[t][x][yp][z][0].adjoint() * Gluonchain[t][x][y][z][2].adjoint()
//                 + Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][0].adjoint() * Gluonchain[tm][x][y][z][2].adjoint() * Gluonchain[tm][x][y][z][0]
//                 + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][x][ym][z][2].adjoint() * Gluonchain[tm][x][ym][z][0] * Gluonchain[t][x][ym][z][2]
//                 + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][z][0] * Gluonchain[tp][x][ym][z][2] * Gluonchain[t][x][y][z][0].adjoint();
//         Q[t][x][y][z][2][0] = Q[t][x][y][z][0][2].adjoint();

//         Q[t][x][y][z][0][3] = Gluonchain[t][x][y][z][0] * Gluonchain[tp][x][y][z][3] * Gluonchain[t][x][y][zp][0].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
//                 + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][0].adjoint() * Gluonchain[tm][x][y][z][3].adjoint() * Gluonchain[tm][x][y][z][0]
//                 + Gluonchain[tm][x][y][z][0].adjoint() * Gluonchain[tm][x][y][zm][3].adjoint() * Gluonchain[tm][x][y][zm][0] * Gluonchain[t][x][y][zm][3]
//                 + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][0] * Gluonchain[tp][x][y][zm][3] * Gluonchain[t][x][y][z][0].adjoint();
//         Q[t][x][y][z][3][0] = Q[t][x][y][z][0][3].adjoint();

//         Q[t][x][y][z][1][1] = Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][1] * Gluonchain[t][xp][y][z][1].adjoint() * Gluonchain[t][x][y][z][1].adjoint()
//                 + Gluonchain[t][x][y][z][1] * Gluonchain[t][x][y][z][1].adjoint() * Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][y][z][1]
//                 + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xmm][y][z][1].adjoint() * Gluonchain[t][xmm][y][z][1] * Gluonchain[t][xm][y][z][1]
//                 + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][y][z][1] * Gluonchain[t][x][y][z][1] * Gluonchain[t][x][y][z][1].adjoint();
//         Q[t][x][y][z][1][2] = Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][2] * Gluonchain[t][x][yp][z][1].adjoint() * Gluonchain[t][x][y][z][2].adjoint()
//                 + Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][1].adjoint() * Gluonchain[t][xm][y][z][2].adjoint() * Gluonchain[t][xm][y][z][1]
//                 + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][ym][z][2].adjoint() * Gluonchain[t][xm][ym][z][1] * Gluonchain[t][x][ym][z][2]
//                 + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][z][1] * Gluonchain[t][xp][ym][z][2] * Gluonchain[t][x][y][z][1].adjoint();
//         Q[t][x][y][z][2][1] = Q[t][x][y][z][1][2].adjoint();

//         Q[t][x][y][z][1][3] = Gluonchain[t][x][y][z][1] * Gluonchain[t][xp][y][z][3] * Gluonchain[t][x][y][zp][1].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
//                 + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][1].adjoint() * Gluonchain[t][xm][y][z][3].adjoint() * Gluonchain[t][xm][y][z][1]
//                 + Gluonchain[t][xm][y][z][1].adjoint() * Gluonchain[t][xm][y][zm][3].adjoint() * Gluonchain[t][xm][y][zm][1] * Gluonchain[t][x][y][zm][3]
//                 + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][1] * Gluonchain[t][xp][y][zm][3] * Gluonchain[t][x][y][z][1].adjoint();
//         Q[t][x][y][z][3][1] = Q[t][x][y][z][1][3].adjoint();

//         Q[t][x][y][z][2][2] = Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][2] * Gluonchain[t][x][yp][z][2].adjoint() * Gluonchain[t][x][y][z][2].adjoint()
//                 + Gluonchain[t][x][y][z][2] * Gluonchain[t][x][y][z][2].adjoint() * Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][z][2]
//                 + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ymm][z][2].adjoint() * Gluonchain[t][x][ymm][z][2] * Gluonchain[t][x][ym][z][2]
//                 + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][z][2] * Gluonchain[t][x][y][z][2] * Gluonchain[t][x][y][z][2].adjoint();
//         Q[t][x][y][z][2][3] = Gluonchain[t][x][y][z][2] * Gluonchain[t][x][yp][z][3] * Gluonchain[t][x][y][zp][2].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
//                 + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][2].adjoint() * Gluonchain[t][x][ym][z][3].adjoint() * Gluonchain[t][x][ym][z][2]
//                 + Gluonchain[t][x][ym][z][2].adjoint() * Gluonchain[t][x][ym][zm][3].adjoint() * Gluonchain[t][x][ym][zm][2] * Gluonchain[t][x][y][zm][3]
//                 + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][2] * Gluonchain[t][x][yp][zm][3] * Gluonchain[t][x][y][z][2].adjoint();
//         Q[t][x][y][z][3][2] = Q[t][x][y][z][2][3].adjoint();
//         Q[t][x][y][z][3][3] = Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][zp][3] * Gluonchain[t][x][y][zp][3].adjoint() * Gluonchain[t][x][y][z][3].adjoint()
//                 + Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][z][3].adjoint() * Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][3]
//                 + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zmm][3].adjoint() * Gluonchain[t][x][y][zmm][3] * Gluonchain[t][x][y][zm][3]
//                 + Gluonchain[t][x][y][zm][3].adjoint() * Gluonchain[t][x][y][zm][3] * Gluonchain[t][x][y][z][3] * Gluonchain[t][x][y][z][3].adjoint();
//     }
// }

//-----
// Calculates the field strength tensor using clover definition

// Full_tensor Fieldstrengthtensor(const Gl_Lattice& Gluon, const int Nmu)
void Fieldstrengthtensor(const Gl_Lattice& Gluon, Full_tensor& F, Full_tensor& Q, const int Nmu)
{
    Clover(Gluon, Q, Nmu);

    #pragma omp parallel for
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    for (int nu = 0; nu < 4; ++nu)
    {
        // F[t][x][y][z][mu][nu] = std::complex<float> (0.0, - 0.125) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
        F[t][x][y][z][mu][nu] = std::complex<float> (0.0, - 1.0 / (8.0 * Nmu * Nmu)) * (Q[t][x][y][z][mu][nu] - Q[t][x][y][z][nu][mu]);
    }
}

//-----
// Calculates energy density from field strength tensor

float Energy_density(const Gl_Lattice& Gluon, Full_tensor& F, Full_tensor& Q, const int Nmu)
{
    cout << "\nBeginning of function" << endl;
    float e_density = {0.0};

    cout << "\nCalculating field strength tensor" << endl;
    Fieldstrengthtensor(Gluon, F, Q, Nmu);
    cout << "\nCalculated field strength tensor" << endl;

    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int y = 0; y < Ny; ++y)
    for (int z = 0; z < Nz; ++z)
    for (int mu = 0; mu < 4; ++mu)
    for (int nu = 0; nu < 4; ++nu)
    {
        e_density += std::real((F[t][x][y][z][mu][nu] * F[t][x][y][z][mu][nu]).trace());
    }
    // e_density *= 1.0/(2.0 * Nx * Ny * Nz * Nt * Nmu * Nmu);
    e_density *= 1.0/(2.0 * 9.0 * Nx * Ny * Nz * Nt);
    // cout << e_density << "\n";
    return e_density;
}

//-----
// Calculates and writes observables to logfile

inline void Observables(const Gl_Lattice& Gluon, std::ofstream& datalog, std::ofstream& energydensitylog, std::ofstream& correlationlog, std::ofstream& correlationlog2, int n_count, const int n_smear)
{
    //ProjectionSU3(Gluon);

    float Action =  GaugeAction(Gluon);
    if (n_smear == 0)
    {
        datalog << "Step " << n_count << "\n";
        datalog << "Wilson_Action: " << Action << "\n";

        // CorrelationFunction(Gluon, CV, CM);
        // PrintCM(CM, correlationlog);
        // PrintCV(CV, correlationlog2);

        for (int Nmu = 1; Nmu < Nmu_max + 1; ++Nmu)
        {
            if (Nmu < Nmu_max)
            {
                energydensitylog << Energy_density(Gluon, *F, *Q, Nmu) << ", ";
            }
            else
            {
                energydensitylog << Energy_density(Gluon, *F, *Q, Nmu) << "\n";
            }
        }
    }
    else if (n_smear != n_smear_max)
    {
        datalog << "Smeared" + to_string(n_smear) + ": " << Action << "\n";

        // CorrelationFunction(Gluon, CV, CM);
        // PrintCM(CM, correlationlog);
        // PrintCV(CV, correlationlog2);

        for (int Nmu = 1; Nmu < Nmu_max + 1; ++Nmu)
        {
            if (Nmu < Nmu_max)
            {
                energydensitylog << Energy_density(Gluon, *F, *Q, Nmu) << ", ";
            }
            else
            {
                energydensitylog << Energy_density(Gluon, *F, *Q, Nmu) << "\n";
            }
        }
    }
    else
    {
        datalog << "Smeared" + to_string(n_smear) + ": " << Action << "\n" << endl;

        // CorrelationFunction(Gluon, CV, CM);
        // PrintCM(CM, correlationlog);
        // PrintCV(CV, correlationlog2);

        for (int Nmu = 1; Nmu < Nmu_max + 1; ++Nmu)
        {
            if (Nmu < Nmu_max)
            {
                energydensitylog << Energy_density(Gluon, *F, *Q, Nmu) << ", ";
            }
            else
            {
                energydensitylog << Energy_density(Gluon, *F, *Q, Nmu) << endl;
            }
        }
    }

    // std::string Gluon_smeared_N;

    // cout << "\nCalculating energy densities\n" << endl;

    // Calculate field strength tensor for all lattice sites

    // F_tensor_array = Fieldstrengthtensor(Gluonsmeared3, all_plaquettes, 1)

    // Gluonsmeared3: Gluon fields after three smearing steps
    // F_tensor_array: Nx * Ny * Nz * Nt * 3 array, where the three independent values of the field strength tensor for each lattice site are stored
    // all_plaquettes: Nx * Ny * Nz * Nt * 4 * 4 array where all plaquettes are stored
}

//-----

int main()
{
    std::ios_base::sync_with_stdio(false);  // iostream not synchronized with corresponding C streams, might cause a problem with C libraries and might not be thread safe
    cout << std::setprecision(12) << std::fixed;
    datalog << std::setprecision(12) << std::fixed;

    cout << "SU(3) gauge theory simulation\n";
    cout << "Currently using EIGEN version: " << EIGEN_WORLD_VERSION << "." << EIGEN_MAJOR_VERSION << "." << EIGEN_MINOR_VERSION << endl;

    Configuration();

    acceptance_rate = 0.0;
    float epsilon {0.5};

    std::uniform_real_distribution<float> distribution_prob(0, 1);
    std::uniform_int_distribution<int> distribution_choice(1, 8);

    for (int i = 0; i < n_beta_increments; ++i)
    {
        CreateFiles();
        SetGluonToOne(*Gluon);
        auto startcalc = std::chrono::system_clock::now();
        datalog.open(logfilepath, std::fstream::out | std::fstream::app);
        energydensitylog.open(energydensityfilepath, std::fstream::out | std::fstream::app);
        correlationlog.open(correlationfilepath, std::fstream::out | std::fstream::app);
        correlationlog2.open(correlationfilepath2, std::fstream::out | std::fstream::app);

        for (int n_count = 0; n_count < n_run; ++n_count)
        {
            std::uniform_real_distribution<float> distribution_unitary(-epsilon, epsilon);
            Update(*Gluon, epsilon, acceptance_rate, distribution_prob, distribution_choice, distribution_unitary);

            if (n_count % expectation_period == 0)
            {
                Observables(*Gluon, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 0);

                StoutSmearing3D(*Gluon, *Gluonsmeared1);
                Observables(*Gluonsmeared1, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 1);

                StoutSmearing3D(*Gluonsmeared1, *Gluonsmeared2);
                Observables(*Gluonsmeared2, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 2);

                StoutSmearing3D(*Gluonsmeared2, *Gluonsmeared1);
                Observables(*Gluonsmeared1, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 3);

                StoutSmearing3D(*Gluonsmeared1, *Gluonsmeared2);
                Observables(*Gluonsmeared2, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 4);

                StoutSmearing3D(*Gluonsmeared2, *Gluonsmeared1);
                Observables(*Gluonsmeared1, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 5);

                StoutSmearing3D(*Gluonsmeared1, *Gluonsmeared2);
                Observables(*Gluonsmeared2, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 6);

                StoutSmearing3D(*Gluonsmeared2, *Gluonsmeared1);
                Observables(*Gluonsmeared1, datalog, energydensitylog, correlationlog, correlationlog2, n_count, 7);
            }

            // epsilon += (acceptance_rate/(Nt * Nx * Ny * Nz * 4.0 * multi_hit) - 0.80) * 0.1;
            epsilon += (acceptance_rate * update_norm - 0.8) * 0.2;
            // epsilon *= acceptance_rate/0.8;
            cout << "epsilon: " << epsilon << "\n";
            // cout << "acceptance_rate: " << acceptance_rate/(Nt * Nx * Ny * Nz * 4.0 * multi_hit) << endl;
            cout << "acceptance_rate: " << acceptance_rate * update_norm << endl;
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - startcalc;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);

        cout << std::ctime(&end_time) << "\n";
        cout << "Required time: " << elapsed_seconds.count() << "s\n";

        datalog << std::ctime(&end_time) << "\n";
        datalog << "Required time: " << elapsed_seconds.count() << "s\n";
        datalog.close();

        energydensitylog << std::ctime(&end_time) << "\n";
        energydensitylog << "Required time: " << elapsed_seconds.count() << "s\n";
        energydensitylog.close();

        correlationlog << std::ctime(&end_time) << "\n";
        correlationlog << "Required time: " << elapsed_seconds.count() << "s\n";
        correlationlog.close();

        correlationlog2 << std::ctime(&end_time) << "\n";
        correlationlog2 << "Required time: " << elapsed_seconds.count() << "s\n";
        correlationlog2.close();

        beta += beta_increments_distance;
    }
    delete Gluon;
    delete Gluonsmeared1;
    delete Gluonsmeared2;
    delete F;
    delete Q;
}
