import time
import sys
import os
import shutil
import natsort
import numpy as np
import cupy as cp
import numexpr as ne
import fastfunc
import scipy as sci
from numba import jit
import pandas as pd
import statsmodels.api as sm
from statsmodels.tsa.stattools import acf
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation
from matplotlib.pyplot import cm
import seaborn as sbn

#-----
# Current version
# current_version = "SU(3)_version_1.2_metro"
current_version = "SU(3)_version_1.3"

# Filepath
# dirpath = os.path.join("Lattice 2021", "Proceedings", "Schwinger_N=32x32_beta=12.800000 (59)")
dirpath = os.path.join("PloopTest", "SU(3)_N=12x12x12x10_beta=6.000000")

# Thermalization phase (discard all expectation values before therm)
therm = 10

# Random seed for numpy (not for normal random)
# seed = 12345
seed = np.random.randint(0, 2**31)
np.random.seed(seed)

# Number or bootstrap resamples
n_resample = 1000

# Default size of bootstrap blocks
blocksize = 2

# Matplotlib rc settings
mpl.rcParams["text.usetex"] = "True"
mpl.rcParams["font.family"] = "Computer Modern"
#mpl.rcParams["axes.prop_cycle"] = mpl.cycler(color = ["#3e65c9", "#3e65c9", "#cc3667", "#cc3667", "#cc3667", "#cc3667"])

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size = SMALL_SIZE)           # Default text size
plt.rc('axes', titlesize = SMALL_SIZE)      # Fontsize of the axes title
plt.rc('axes', labelsize = MEDIUM_SIZE)     # Fontsize of the x and y labels
plt.rc('xtick', labelsize = SMALL_SIZE)     # Fontsize of the tick labels
plt.rc('ytick', labelsize = SMALL_SIZE)     # Fontsize of the tick labels
plt.rc('legend', fontsize = SMALL_SIZE)     # Legend fontsize
plt.rc('figure', titlesize = BIGGER_SIZE)   # Fontsize of the figure title

# mpl.rcParams["xtick.direction"] = "in"
# mpl.rcParams["ytick.direction"] = "in"
# mpl.rcParams["xtick.direction"] = "out"
# mpl.rcParams["ytick.direction"] = "out"
# mpl.rcParams["axes.formatter.limits"] = "-1, 1"

# ffmpeg path for animations, change as needed
plt.rcParams['animation.ffmpeg_path'] = 'C:\\ffmpeg\\bin\\ffmpeg.exe'

#-----
# Get user input on what to do

print("\nData analysis program for " + current_version + "\n")

#-----
# Get values for n_run and expectation_period from parameters.txt

if os.path.isfile(os.path.join(dirpath, "parameters.txt")) == True:
    with open(os.path.join(dirpath, "parameters.txt")) as paramfile:
        first_line = paramfile.readline().strip()
        if first_line == current_version:
            print("Compatible parameter file found in " + dirpath + "\n")
            n_run = None
            expectation_period = None
            for line in paramfile:
                if not (len(line.strip()) == 0):
                    token = line.split()[0]
                    if token == "Nt":
                        Nt = int(line.split()[2])
                    if token == "Nx":
                        Nx = int(line.split()[2])
                    if token == "Ny":
                        Ny = int(line.split()[2])
                    if token == "Nz":
                        Nz = int(line.split()[2])
                    if token == "beta":
                        beta = float(line.split()[2])
                    if token == "n_run":
                        n_run = int(line.split()[2])
                    if token == "expectation_period":
                        expectation_period = int(line.split()[2])
                    if token == "n_smear":
                        n_smear = int(line.split()[2])
                    if token == "multi_hit":
                        multi_hit = int(line.split()[2])
                    # if token == "top_update":
                    #     top_update = int(line.split()[2])
                    # if token == "n_smooth":
                    #     n_smooth = int(line.split()[2])
                    if token == "END_PARAMS":
                        break
        else:
            print("Incompatible file found in " + dirpath)
            print("File version: " + first_line)
            print("Program version: " + current_version + "\n")
else:
    print("Could not find file \"parameters.txt\" in the directory " + dirpath)

V = Nt * Nx * Ny * Nz

#-----
# Number of expectation values with and without thermalization phase

n_expectation_values_therm = int(n_run/expectation_period)
n_expectation_values = n_expectation_values_therm - therm

if n_expectation_values % blocksize != 0:
    print("Number of expectation values is not a multiple of blocksize")
    print("Exiting program")
    sys.exit()

#-----
# Create directories for output

print("Creating directories for analysis results.")

analysis_path_pre = os.path.join(dirpath, "analysis_results")
analysis_path = analysis_path_pre
append = 1

while os.path.exists(analysis_path):
    analysis_path = analysis_path_pre + " (" + str(append) + ")"
    append += 1

os.mkdir(analysis_path)

raw_data_path = os.path.join(analysis_path, "raw_data")
thermalization_path = os.path.join(analysis_path, "thermalization")
timeseries_path = os.path.join(analysis_path, "timeseries")
histogram_data_path = os.path.join(analysis_path, "histograms")

os.mkdir(raw_data_path)
os.mkdir(thermalization_path)
os.mkdir(timeseries_path)
os.mkdir(histogram_data_path)

print("Created directories for analysis results:", analysis_path)
print("\n" + "-"*min(50, shutil.get_terminal_size().columns) + "\n")

# Write parameters to analysis_parameters.txt

parameters_file = open(os.path.join(analysis_path, "analysis_parameters_file.txt"), "a")
parameters_file.writelines([current_version, "\n\nSimulation parameters:\n"])
parameters_file.write("\nNt = " + str(Nt))
parameters_file.write("\nNx = " + str(Nx))
parameters_file.write("\nNy = " + str(Ny))
parameters_file.write("\nNz = " + str(Nz))
parameters_file.write("\nbeta = " + str(beta))
parameters_file.write("\nn_run = " + str(n_run))
parameters_file.write("\nexpectation_period = " + str(expectation_period))
parameters_file.write("\nn_smear = " + str(n_smear))
parameters_file.write("\nmulti_hit = " + str(multi_hit))
# parameters_file.write("\ntop_update = " + str(top_update))
# parameters_file.write("\nn_smooth = " + str(n_smooth))
#-----
parameters_file.write("\n\nAnalysis parameters:\n")
parameters_file.write("\ntherm = " + str(therm))
parameters_file.write("\nn_resample = " + str(n_resample))
parameters_file.write("\nblocksize = " + str(blocksize))
parameters_file.write("\nseed = " + str(seed))
parameters_file.close()

#-----
# Setup arrays for all observables read directly from logfile

_action_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)
# _plaquette_re_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)
# _plaquette_im_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)
_ploop_re_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)
_ploop_im_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)
_top_charge_clov_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)
_top_charge_plaq_raw = np.zeros((n_expectation_values_therm, n_smear + 1), dtype = float)

#-----
# Bootstrap
# Generates bootstrap_array which can be used as weight

bootstrap_array = np.zeros((n_resample, n_expectation_values))

@jit(nopython = True)
def create_bootstrap(bootstrap_array, blocksize = 1):
    bootstrap_array.fill(0)
    n_blocks = n_expectation_values // blocksize
    bootstrap_array[0,] = 1
    for i in range(1, n_resample):
        for j in range(n_blocks):
            bs_index = np.random.randint(0, n_expectation_values)
            for k in range(blocksize):
                bootstrap_array[i, (bs_index + k) % n_expectation_values] += 1

# create_bootstrap(bootstrap_array, blocksize)

# Function for bootstrapping observable
# input consists of raw data of observable and the already calculated expectation values of the observable
# works with observables of variable length (e.g. correlation functions)
# TODO: might not yet work with multiple betas

# def bootstrap(observable_raw, observable_expectation_value, return_type = 0):
#     if observable_raw.ndim >= 2:
#         observable_length = np.size(observable_raw, axis = 1)
#         observable_resampled = np.zeros((n_resample, observable_length))
#     else:
#         observable_length = 1
#         observable_resampled = np.zeros(n_resample)
#     for i in range(0, n_resample):
#         observable_resampled[i] = np.average(observable_raw, axis = 0, weights = bootstrap_array[i])
#     observable_error = np.std(observable_resampled, axis = 0, ddof = 1)
#     observable_final = np.array([observable_expectation_value, observable_error])
#     if return_type == 0:
#         return observable_final, observable_resampled
#     if return_type == 1:
#         return observable_final
#     if return_type == 2:
#         return observable_resampled

def bootstrap(observable_raw, observable_expectation_value, bootstrap_array, return_resampled = False, additional_weight = None):
    '''
    Bootstrap function that supports blocked bootstrap.

    Parameters
    ----------
    observable : array_like
        Array with raw observable that is supposed to be resampled.
    observable_expectation_value : array_like
        The expectation value of the observable.
    bootstrap_array : array_like
        Array containing the bootstrap weights generated by the create_bootstrap() function.
    return_resampled : bool
        Return averages on bootstrap samples or not.
    additional_weight : array_like
        Optional additional weight.

    Returns
    -------
    observable_final : ndarray
        Array containing the expectation value of the observable and the standard deviation obtained by bootstrapping.
    observable_resampled : ndarray
        Array with all averages on the bootstrap samples.
    '''
    observable_resampled = np.zeros(n_resample)
    if additional_weight is not None:
        for i in range(0, n_resample):
            observable_resampled[i] = np.average(observable_raw * additional_weight[i], weights = bootstrap_array[i])
    else:
        for i in range(0, n_resample):
            observable_resampled[i] = np.average(observable_raw, weights = bootstrap_array[i])
    observable_error = np.std(observable_resampled, ddof = 1)
    observable_final = np.array([observable_expectation_value, observable_error])
    if return_resampled:
        return observable_final, observable_resampled
    else:
        return observable_final

#-----
# Faster logsumexp using numexpr

def logsumexp_numexpr(a, axis = None, b = None, use_numexpr = True):
    a = np.asarray(a)

    a_max = np.amax(a, axis = axis, keepdims=True)

    if a_max.ndim > 0:
        a_max[~np.isfinite(a_max)] = 0
    elif not np.isfinite(a_max):
        a_max = 0

    if b is not None:
        b = np.asarray(b)
        if use_numexpr:
            # out = np.log(ne.evaluate("b*exp(a - a_max)").sum(axis)) #original expression
            out = np.log((b * ne.evaluate("exp(a - a_max)")).sum(axis))
        else:
            out = np.log(np.sum(b * np.exp(a - a_max), axis = axis))
    else:
        if use_numexpr:
            out = np.log(ne.evaluate("exp(a - a_max)").sum(axis))
        else:
            out = np.log(np.sum(np.exp(a - a_max), axis = axis))

    a_max = np.squeeze(a_max, axis = axis)
    out += a_max

    return out

#-----
# Get all values for observables from logfile if they haven't been read in before, otherwise load from .npy files

if os.path.isdir(os.path.join(dirpath, "raw_data")) == True:
    print("Reading observables from existing .npy files")
    _action_raw = np.load(os.path.join(dirpath, "raw_data", "action_raw.npy"))
    _ploop_re_raw = np.load(os.path.join(dirpath, "raw_data", "ploop_re_raw.npy"))
    _ploop_im_raw = np.load(os.path.join(dirpath, "raw_data", "ploop_im_raw.npy"))
    _top_charge_clov_raw = np.load(os.path.join(dirpath, "raw_data", "_top_charge_clov_raw.npy"))
    _top_charge_plaq_raw = np.load(os.path.join(dirpath, "raw_data", "_top_charge_plaq_raw.npy"))
elif os.path.isfile(os.path.join(dirpath, "log.txt")):
    with open(os.path.join(dirpath, "log.txt")) as logfile:
        if first_line == current_version:
            print("Compatible logfile found in " + dirpath)
            print("Reading observables from logfile")
            meas_count = -1
            for line in logfile:
                if not (len(line.strip()) == 0):
                    token = line.split()[0]
                    # if (len(line.strip()) > 2):
                    #     if line.split()[0] == "beta":
                    #         beta_list[smear_count] = float(line.split()[2])
                    if token == "Wilson_Action:":
                        _action_raw[meas_count, :] = line.split()[1:]
                        continue
                    # if token == "Plaquette_Re:":
                    #     _plaquette_re_raw[meas_count, :] = line.split()[1:]
                    #     continue
                    # if token == "Plaquette_Im:":
                    #     _plaquette_im_raw[meas_count, :] = line.split()[1:]
                    #     continue
                    if token == "Polyakov_loop(Re):":
                        _ploop_re_raw[meas_count, :] = line.split()[1:]
                        continue
                    if token == "Polyakov_loop(Im):":
                        _ploop_im_raw[meas_count, :] = line.split()[1:]
                        continue
                    if token == "TopChargeClov:":
                        _top_charge_clov_raw[meas_count, :] = line.split()[1:]
                        continue
                    if token == "TopChargePlaq:":
                        _top_charge_plaq_raw[meas_count, :] = line.split()[1:]
                        continue
                    if token == "[Step":
                        meas_count += 1
        else:
            print("Incompatible version in " + dirpath)
    os.mkdir(os.path.join(dirpath, "raw_data"))
    np.save(os.path.join(dirpath, "raw_data", "action_raw.npy"), _action_raw)
    np.save(os.path.join(dirpath, "raw_data", "ploop_re_raw.npy"), _ploop_re_raw)
    np.save(os.path.join(dirpath, "raw_data", "ploop_im_raw.npy"), _ploop_im_raw)
    np.save(os.path.join(dirpath, "raw_data", "_top_charge_clov_raw.npy"), _top_charge_clov_raw)
    np.save(os.path.join(dirpath, "raw_data", "_top_charge_plaq_raw.npy"), _top_charge_plaq_raw)
else:
    print("Could not find file \"log.txt\" in the current directory " + dirpath)

#-----
# Transpose arrays to shape (n_smear, n_expectation_values_therm)

# TODO: Remove this for correct topological charge
# _top_charge_plaq_raw *= 0.25

action_raw = np.ascontiguousarray(_action_raw.T)
# plaquette_re_raw = np.ascontiguousarray(_plaquette_re_raw.T)
# plaquette_im_raw = np.ascontiguousarray(_plaquette_im_raw.T)
ploop_re_raw = np.ascontiguousarray(_ploop_re_raw.T)
ploop_im_raw = np.ascontiguousarray(_ploop_im_raw.T)
top_charge_clov_raw = np.ascontiguousarray(_top_charge_clov_raw.T)
top_charge_plaq_raw = np.ascontiguousarray(_top_charge_plaq_raw.T)

#-----
# Plot thermalization phase and timeseries

def plot_timeseries(observable, ylabel, directory, filename, plot_therm = False):
    if plot_therm:
        for smear_count in range(n_smear + 1):
            plt.plot(np.arange(0, 2*therm), observable[smear_count, 0:2*therm], color = "#469FE5", linewidth = 0.2)
            plt.xlabel("Monte Carlo time")
            plt.ylabel(ylabel)
            plt.savefig(os.path.join(directory, filename + "_thermalization_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
            plt.close()
    else:
        for smear_count in range(n_smear + 1):
            plt.plot(np.arange(0,len(observable[smear_count, :])), observable[smear_count, :], color = "#469FE5", linewidth = 0.2)
            plt.xlabel("Monte Carlo time")
            plt.ylabel(ylabel)
            plt.savefig(os.path.join(directory, "timeseries_" + filename + "_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
            plt.close()

# Plot thermalization phase

plot_timeseries(action_raw, r"$S$", thermalization_path, "action", plot_therm = True)
# plot_timeseries(plaquette_re_raw, r"$\Re{P}$", thermalization_path, "plaquette_re", plot_therm = True)
# plot_timeseries(plaquette_im_raw, r"$\Im{P}$", thermalization_path, "plaquette_im", plot_therm = True)
plot_timeseries(ploop_re_raw, r"$\Re{P}$", thermalization_path, "ploop_re", plot_therm = True)
plot_timeseries(ploop_im_raw, r"$\Im{P}$", thermalization_path, "ploop_im", plot_therm = True)
plot_timeseries(top_charge_clov_raw, r"$Q_{c}$", thermalization_path, "top_charge_clov", plot_therm = True)
plot_timeseries(top_charge_plaq_raw, r"$Q_{p}$", thermalization_path, "top_charge_plaq", plot_therm = True)

# Plot entire timeseries

plot_timeseries(action_raw, r"$S$", timeseries_path, "action", plot_therm = False)
# plot_timeseries(plaquette_re_raw, r"$\Re{P}$", timeseries_path, "plaquette_re", plot_therm = False)
# plot_timeseries(plaquette_im_raw, r"$\Im{P}$", timeseries_path, "plaquette_im", plot_therm = False)
plot_timeseries(ploop_re_raw, r"$\Re{P}$", timeseries_path, "ploop_re", plot_therm = False)
plot_timeseries(ploop_im_raw, r"$\Im{P}$", timeseries_path, "ploop_im", plot_therm = False)
plot_timeseries(top_charge_clov_raw, r"$Q_{c}$", timeseries_path, "top_charge_clov", plot_therm = False)
plot_timeseries(top_charge_plaq_raw, r"$Q_{p}$", timeseries_path, "top_charge_plaq", plot_therm = False)

for smear_count in range(n_smear + 1):
    # Plot histogram of topological charge defined via clover term

    scaling_factor = 0.05

    max_charge_clov = np.max(np.abs(top_charge_clov_raw[smear_count, :]))
    hist_bins_clov = np.arange(-max_charge_clov, max_charge_clov + (1.5 * scaling_factor), step = scaling_factor) - (0.5 * scaling_factor)

    plt.hist(top_charge_clov_raw[smear_count, :], bins = hist_bins_clov)
    plt.xlabel(r"$Q (n_{\textrm{smear}} =$ " + str(smear_count) + r"$)$")
    plt.ylabel("Frequency")
    plt.savefig(os.path.join(histogram_data_path, "histogram_top_charge_clov_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    plt.close()

    # Plot histogram of topological charge defined via plaquette

    max_charge_plaq = np.max(np.abs(top_charge_plaq_raw[smear_count, :]))
    hist_bins_plaq = np.arange(-max_charge_plaq, max_charge_plaq + (1.5 * scaling_factor), step = scaling_factor) - (0.5 * scaling_factor)

    plt.hist(top_charge_plaq_raw[smear_count, :], bins = hist_bins_plaq)
    plt.xlabel(r"$Q (n_{\textrm{smear}} =$ " + str(smear_count) + r"$)$")
    plt.ylabel("Frequency")
    plt.savefig(os.path.join(histogram_data_path, "histogram_top_charge_plaq_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    plt.close()

    # Plot 2D histogram of Q vs Q_meta

    # sbn.JointGrid(x = top_charge_raw[smear_count, :], y = top_charge_meta_raw[smear_count, :])
    # plt.hist2d(x = top_charge_raw[smear_count, :], y = top_charge_meta_raw[smear_count, :], bins = np.array([hist_bins, hist_bins_meta]))
    # plt.xlabel(r"$Q$")
    # plt.ylabel(r"$Q_{\textrm{meta}}$")
    # plt.title(r"Comparison of topological charges $(n_{\textrm{smear}} =$ " + str(smear_count) + r"$)$")
    # plt.savefig(os.path.join(histogram_data_path, "histogram_top_correlation_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    # plt.close()

    # Plot "histogram" of Polyakov loops
    # plt.scatter(ploop_re_raw[smear_count, :], ploop_im_raw[smear_count, :])
    # plt.xlabel(r"$\Re{P}$")
    # plt.ylabel(r"$\Im{P}$")
    # plt.savefig(os.path.join(histogram_data_path, "histogram_ploop_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    # plt.close()

#-----
# Output raw data as .npy (doesn't need to be human-readable)

np.save(os.path.join(raw_data_path, "action_raw"), action_raw.T)
# np.save(os.path.join(raw_data_path, "plaquette_re_raw"), plaquette_re_raw.T)
# np.save(os.path.join(raw_data_path, "plaquette_im_raw"), plaquette_im_raw.T)
np.save(os.path.join(raw_data_path, "ploop_re_raw"), ploop_re_raw.T)
np.save(os.path.join(raw_data_path, "ploop_im_raw"), ploop_im_raw.T)
np.save(os.path.join(raw_data_path, "top_charge_clov_raw"), top_charge_clov_raw.T)
np.save(os.path.join(raw_data_path, "top_charge_plaq_raw"), top_charge_plaq_raw.T)

#-----
# Load and plot metapotential

if True:
    if os.path.isfile(os.path.join(dirpath, "metapotential.txt")):
        metafile_line_count = sum(1 for _ in open(os.path.join(dirpath, "metapotential.txt")))
        with open(os.path.join(dirpath, "metapotential.txt")) as metapotentialfile:
            skipcount = 1
            for line in metapotentialfile:
                if not (len(line.strip()) == 0):
                    token = line.split()[0]
                    if token == "Q_min:":
                        meta_Q_min = float(line.split()[1])
                    if token == "Q_max:":
                        meta_Q_max = float(line.split()[1])
                    if token == "bin_number:":
                        meta_bin_number = int(line.split()[1]) + 1
                    if token == "weight:":
                        meta_weight = float(line.split()[1])
                    if token == "threshold_weight:":
                        meta_threshold_weight = float(line.split()[1])
                    if token == "exceeded_count:":
                        meta_exceeded_count = int(line.split()[1])
                if "END_METADYN_PARAMS" in line:
                    # metapotential = np.genfromtxt(os.path.join(dirpath, "metapotential.txt"), dtype = float, delimiter = ",", skip_header = skipcount)
                    skip = np.unique(np.concatenate((np.arange(skipcount), np.arange(skipcount - 1, metafile_line_count, 2))))
                    metapotential = pd.read_csv(os.path.join(dirpath, "metapotential.txt"), dtype = float, delimiter = ",", skiprows = skip, header = None).to_numpy()
                    break
                else:
                    skipcount += 1

    if os.path.isfile(os.path.join(dirpath, "penaltypotential.txt")):
        penaltypotential = np.loadtxt(os.path.join(dirpath, "penaltypotential.txt"), delimiter = ",")
        metapotential -= penaltypotential

    x_range = np.linspace(meta_Q_min, meta_Q_max, meta_bin_number)

    plt.plot(x_range, metapotential[-1, :])
    plt.xlabel(r"$Q_{\textrm{meta}}$")
    plt.ylabel(r"$V(Q_{\textrm{meta}})$")
    plt.savefig(os.path.join(histogram_data_path, "metapotential.pdf"), bbox_inches = "tight")
    plt.close()

    plt.plot(x_range, metapotential[-1, :])
    plt.xlabel(r"$Q_{\textrm{meta}}$")
    plt.ylabel(r"$V(Q_{\textrm{meta}})$")
    plt.xlim(meta_Q_min * 0.5, meta_Q_max * 0.5)
    plt.savefig(os.path.join(histogram_data_path, "metapotential_small_range.pdf"), bbox_inches = "tight")
    plt.close()

    plt.plot(x_range, metapotential[-1, :])
    plt.xlabel(r"$Q_{\textrm{meta}}$")
    plt.ylabel(r"$V(Q_{\textrm{meta}})$")
    plt.xlim(-7, 7)
    plt.savefig(os.path.join(histogram_data_path, "metapotential_small_range2.pdf"), bbox_inches = "tight")
    plt.close()

    fig, ax = plt.subplots()
    # colormap = iter(cm.coolwarm(np.linspace(0, 1, np.size(metapotential, 0))))
    # colormap = iter(cm.plasma(np.linspace(0, 1, np.size(metapotential, 0))))

    def anim_func(t):
        ax.clear()
        plt.ylim(-np.max(metapotential), 5)
        # ax.set_xlabel(r"$Q_{\textrm{meta}}$")
        # ax.set_ylabel(r"$V(Q_{\textrm{meta}})$")
        # current_color = next(colormap)
        # ax.plot(x_range, metapotential[t, :] - np.max(metapotential[t, :]), color = current_color)
        ax.plot(x_range, metapotential[t, :] - np.max(metapotential[t, :]))

    if metapotential_updated:
        meta_animation = matplotlib.animation.FuncAnimation(fig, anim_func, frames = np.size(metapotential, 0), interval = 0.2, save_count = sys.maxsize)
        writervideo = matplotlib.animation.FFMpegWriter(fps = 30, bitrate = 5000) # changing the bitrate doesn't seem to make a difference except in filesize
        # writervideo = matplotlib.animation.FFMpegWriter(fps = 30, bitrate = -1, codec = "libx264", extra_args = ['-pix_fmt', 'yuv420p'])
        fig.set_size_inches(19.2, 10.8)
        dpi = 100
        meta_animation.save(os.path.join(histogram_data_path, "metapotential_anim.mp4"), writer = writervideo, dpi = dpi)
        # meta_animation.save(os.path.join(histogram_data_path, "metapotential_anim.gif"))
        plt.close()

    plt.savefig(os.path.join(histogram_data_path, "metapotential_anim.pdf"), bbox_inches = "tight")

#-----
# Autocorrelation functions
# TODO: move this past removal of thermalization?
# if yes use n_expectation_values, otherwise use n_expectation_values_therm

print("Calculating autocorrelation functions:\n")

action_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
action_integrated_ac = np.zeros(n_smear + 1)
# plaquette_re_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
# plaquette_re_integrated_ac = np.zeros(n_smear + 1)
# plaquette_im_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
# plaquette_im_integrated_ac = np.zeros(n_smear + 1)
ploop_re_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
ploop_re_integrated_ac = np.zeros(n_smear + 1)
ploop_im_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
ploop_im_integrated_ac = np.zeros(n_smear + 1)
top_charge_clov_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
top_charge_clov_integrated_ac = np.zeros(n_smear + 1)
top_charge_plaq_autocorrelation = np.zeros((n_smear + 1, n_expectation_values + 1))
top_charge_plaq_integrated_ac = np.zeros(n_smear + 1)

# TODO: If the autocorrelation function never crosses zero, np.argmax(acf < 0) returns 0, and the integrated autocorrelation time is set to 0.5
#       Need to check if that is the case and sum all entries of the acf

for smear_count in range(n_smear + 1):
    action_autocorrelation[smear_count, :] = acf(action_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    action_integrated_ac[smear_count] = 0.5 + np.sum(action_autocorrelation[smear_count, 1:np.argmax(action_autocorrelation[smear_count, :] < 0)])
    #-----
    # plaquette_re_autocorrelation[smear_count, :] = acf(plaquette_re_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    # plaquette_re_integrated_ac[smear_count] = 0.5 + np.sum(plaquette_re_autocorrelation[smear_count, 1:np.argmax(plaquette_re_autocorrelation[smear_count, :] < 0)])
    #-----
    # plaquette_im_autocorrelation[smear_count, :] = acf(plaquette_im_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    # plaquette_im_integrated_ac[smear_count] = 0.5 + np.sum(plaquette_im_autocorrelation[smear_count, 1:np.argmax(plaquette_im_autocorrelation[smear_count, :] < 0)])
    #-----
    ploop_re_autocorrelation[smear_count, :] = acf(ploop_re_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    ploop_re_integrated_ac[smear_count] = 0.5 + np.sum(ploop_re_autocorrelation[smear_count, 1:np.argmax(ploop_re_autocorrelation[smear_count, :] < 0)])
    #-----
    ploop_im_autocorrelation[smear_count, :] = acf(ploop_im_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    ploop_im_integrated_ac[smear_count] = 0.5 + np.sum(ploop_im_autocorrelation[smear_count, 1:np.argmax(ploop_im_autocorrelation[smear_count, :] < 0)])
    #-----
    top_charge_clov_autocorrelation[smear_count, :] = acf(top_charge_clov_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    top_charge_clov_integrated_ac[smear_count] = 0.5 + np.sum(top_charge_clov_autocorrelation[smear_count, 1:np.argmax(top_charge_clov_autocorrelation[smear_count, :] < 0)])
    #-----
    # top_charge_plaq_squared_autocorrelation[smear_count, :] = acf(top_charge_plaq_raw[smear_count, :], nlags = n_expectation_values, fft = True)
    # top_charge_plaq_squared_integrated_ac[smear_count] = 0.5 + np.sum(top_charge_plaq_autocorrelation[smear_count, 1:np.argmax(top_charge_plaq_autocorrelation[smear_count, :] < 0)])

np.savetxt(os.path.join(raw_data_path, "action_autocorrelation_acf.txt"), action_autocorrelation[0].T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "action_integrated_ac.txt"), action_integrated_ac.T, delimiter = ",")
# np.savetxt(os.path.join(raw_data_path, "plaquette_re_autocorrelation_acf.txt"), plaquette_re_autocorrelation[0].T, delimiter = ",")
# np.savetxt(os.path.join(raw_data_path, "plaquette_re_integrated_ac.txt"), plaquette_re_integrated_ac.T, delimiter = ",")
# np.savetxt(os.path.join(raw_data_path, "plaquette_im_autocorrelation_acf.txt"), plaquette_im_autocorrelation[0].T, delimiter = ",")
# np.savetxt(os.path.join(raw_data_path, "plaquette_im_integrated_ac.txt"), plaquette_im_integrated_ac.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "ploop_re_autocorrelation_acf.txt"), ploop_re_autocorrelation[0].T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "ploop_re_integrated_ac.txt"), ploop_re_integrated_ac.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "ploop_im_autocorrelation_acf.txt"), ploop_im_autocorrelation[0].T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "ploop_im_integrated_ac.txt"), ploop_im_integrated_ac.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "top_charge_clov_autocorrelation_acf.txt"), top_charge_clov_autocorrelation[0].T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "top_charge_clov_integrated_ac.txt"), top_charge_clov_integrated_ac.T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "top_charge_plaq_autocorrelation_acf.txt"), top_charge_plaq_autocorrelation[0].T, delimiter = ",")
np.savetxt(os.path.join(raw_data_path, "top_charge_plaq_integrated_ac.txt"), top_charge_plaq_integrated_ac.T, delimiter = ",")

integrated_ac_time = np.max(np.maximum.reduce([action_integrated_ac, ploop_re_integrated_ac, ploop_im_integrated_ac, top_charge_clov_integrated_ac, top_charge_plaq_integrated_ac]))

# TODO: Choose blocksize depending on integrated_ac_time

print("Currently chosen blocksize:", blocksize)
print("Largest integrated autocorrelation time:", integrated_ac_time)

parameters_file = open(os.path.join(analysis_path, "analysis_parameters_file.txt"), "a")
parameters_file.write("\nIntegrated autocorrelation time = " + str(integrated_ac_time))
parameters_file.close()

if blocksize < 2 * integrated_ac_time:
    print("Warning: Currently chosen default blocksize is smaller than two times the largest integrated autocorrelation time!")
    print("2 * tau_int =", 2 * integrated_ac_time)
else:
    print("Currently chosen default blocksize is greater than two times the largest integrated autocorrelation time.")

blocksize = int(np.ceil(2 * integrated_ac_time))
while n_expectation_values % blocksize != 0:
    blocksize += 1

create_bootstrap(bootstrap_array, blocksize)
print("Blocksize has been set to: ", blocksize)

# All autocorrelation functions in one figure?

# for smear_count, a in enumerate(directories):
#     sm.graphics.tsa.plot_acf(energy_autocorrelation[smear_count, :], lags = np.arange(len(energy_autocorrelation)))
#     plt.savefig(os.path.join(a, "energy_autocorrelation.pdf"), bbox_inches = "tight")
#     plt.close()

#     sm.graphics.tsa.plot_acf(magnetization_autocorrelation[smear_count, :], lags = np.arange(len(magnetization_autocorrelation)))
#     plt.savefig(os.path.join(a, "magnetization_autocorrelation.pdf"), bbox_inches = "tight")
#     plt.close()

# plt.acorr(energy_autocorrelation[0, :])
# plt.show()
# plt.close()
# sys.exit()

#-----
# Remove thermalization phase from observables

action_raw = action_raw[:, therm:]
# plaquette_re_raw = plaquette_re_raw[:, therm:]
# plaquette_im_raw = plaquette_im_raw[:, therm:]
ploop_re_raw = ploop_re_raw[:, therm:]
ploop_im_raw = ploop_im_raw[:, therm:]
top_charge_clov_raw = top_charge_clov_raw[:, therm:]
top_charge_plaq_raw = top_charge_plaq_raw[:, therm:]

for smear_count in range(n_smear + 1):
    plt.scatter(ploop_re_raw[smear_count, :], ploop_im_raw[smear_count, :])
    plt.xlabel(r"$\Re{P}$")
    plt.ylabel(r"$\Im{P}$")
    plt.savefig(os.path.join(histogram_data_path, "histogram_ploop_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    plt.close()

#-----
# Calculate observables and uncertainties

start = time.time()

# Arrays containing the observables and their estimated statistical uncertainties

action_final = np.zeros((n_smear + 1, 2))
# plaquette_re_final = np.zeros((n_smear + 1, 2))
# plaquette_im_final = np.zeros((n_smear + 1, 2))
ploop_re_final = np.zeros((n_smear + 1, 2))
ploop_im_final = np.zeros((n_smear + 1, 2))
top_charge_clov_final = np.zeros((n_smear + 1, 2))
top_charge_plaq_final = np.zeros((n_smear + 1, 2))
renorm_charge_clov_final = np.zeros((n_smear + 1, 2))
renorm_suscep_clov_final = np.zeros((n_smear + 1, 2))
renorm_charge_plaq_final = np.zeros((n_smear + 1, 2))
renorm_suscep_plaq_final = np.zeros((n_smear + 1, 2))

# Non-logsumexp version
print("Action:", np.average(action_raw[0, :]))
# print("Plaquette_Re:", np.average(plaquette_re_raw[0, :]))
# print("Plaquette_Im:", np.average(plaquette_im_raw[0, :]))
print("Ploop (Re):", np.average(ploop_re_raw[0, :]))
print("Ploop (Im):", np.average(ploop_im_raw[0, :]))
print("Top_Charge (Clov):", np.average(top_charge_clov_raw[0, :]))
print("Top_Charge (Plaq):", np.average(top_charge_plaq_raw[0, :]))
# print("Top_Suscept:", np.average(top_charge_plaq_raw[0, :]))

# Renormalization constants for topological charge

# TODO: Correct layout/order?
Z_clov = np.zeros((n_resample + 1, n_smear + 1))
Z_plaq = np.zeros((n_resample + 1, n_smear + 1))

def chi_2_top(Z_clov_in, top_charge_timeseries, bootstrap_weights = None):
    # Since there is a trivial global minimum at Z = 0, we impose the condition Z > 1
    # On second thought, this function should be trivial to minimize, so let´s first try imposing this condition through the minimizer
    if bootstrap_weights is not None:
        return np.dot((Z_clov_in * top_charge_timeseries - np.around(Z_clov_in * top_charge_timeseries))**2, bootstrap_weights)
    else:
        return np.sum((Z_clov_in * top_charge_timeseries - np.around(Z_clov_in * top_charge_timeseries))**2)

def chi_2_top_gpu(Z_clov_cpu, top_charge_timeseries_cpu, bootstrap_weights_cpu = None):
    # Since there is a trivial global minimum at Z = 0, we impose the condition Z > 1
    # On second thought, this function should be trivial to minimize, so let´s first try imposing this condition through the minimizer
    Z_clov_in = cp.asarray(Z_clov_cpu)
    top_charge_timeseries = cp.asarray(top_charge_timeseries_cpu)
    if bootstrap_weights_cpu is not None:
        bootstrap_weights = cp.asarray(bootstrap_weights_cpu)
        return cp.dot(cp.square(cp.subtract(cp.multiply(Z_clov_in, top_charge_timeseries), cp.around(cp.multiply(Z_clov_in, top_charge_timeseries)))), bootstrap_weights)
    else:
        return cp.sum(cp.square(cp.subtract(cp.multiply(Z_clov_in, top_charge_timeseries), cp.around(cp.multiply(Z_clov_in, top_charge_timeseries)))))

# Perform the first iteration manually, afterwards use values from central sample as initial conditions for minimizer
# For the unsmeared charges, the renormalization factor can be very large if there are no bounds
# However, in those cases the renormalization doesn't make much sense anyways since the charge is too far away from integer values
for smear_count in range(n_smear + 1):
    # res_clov = sci.optimize.minimize(chi_2_top_gpu, x0 = 1.0, args = (top_charge_clov_raw[smear_count, :]), method = 'Powell', bounds = (1.0, 2.0))
    # res_plaq = sci.optimize.minimize(chi_2_top_gpu, x0 = 1.0, args = (top_charge_plaq_raw[smear_count, :]), method = 'Powell', bounds = (1.0, 2.0))
    res_clov = sci.optimize.minimize_scalar(chi_2_top_gpu, args = (top_charge_clov_raw[smear_count, :]), method = "Brent", bounds = (1.0, 1.4))
    res_plaq = sci.optimize.minimize_scalar(chi_2_top_gpu, args = (top_charge_plaq_raw[smear_count, :]), method = "Brent", bounds = (1.0, 1.4))
    Z_clov[0, smear_count] = res_clov.x
    Z_plaq[0, smear_count] = res_plaq.x

for bootstrap_count in range(n_resample):
    # Afterwards do the optimizations for the bootstrap samples using the previous results as initial conditions
    if bootstrap_count % 100 == 0:
        print("Currently optimizing bootstrap sample ", bootstrap_count)
    for smear_count in range(n_smear + 1):
        # res_clov = sci.optimize.minimize(chi_2_top_gpu, x0 = Z_clov[0], args = (top_charge_clov_raw[smear_count, :], bootstrap_array[bootstrap_count, :]), method = 'Powell', bounds = (1.0, 2.0))
        # res_plaq = sci.optimize.minimize(chi_2_top_gpu, x0 = Z_plaq[0], args = (top_charge_plaq_raw[smear_count, :], bootstrap_array[bootstrap_count, :]), method = 'Powell', bounds = (1.0, 2.0))
        res_clov = sci.optimize.minimize_scalar(chi_2_top_gpu, args = (top_charge_clov_raw[smear_count, :], bootstrap_array[bootstrap_count, :]), method = "Brent", bounds = (1.0, 1.4))
        res_plaq = sci.optimize.minimize_scalar(chi_2_top_gpu, args = (top_charge_plaq_raw[smear_count, :], bootstrap_array[bootstrap_count, :]), method = "Brent", bounds = (1.0, 1.4))
        # Add 1 to bootstrap_count for Z_clov and Z_plaq, since we do not need the central sample here
        Z_clov[bootstrap_count + 1, smear_count] = res_clov.x
        Z_plaq[bootstrap_count + 1, smear_count] = res_plaq.x

Z_clov_final = np.array([np.mean(Z_clov, axis = 0), np.std(Z_clov, axis = 0, ddof = 1)])
Z_plaq_final = np.array([np.mean(Z_plaq, axis = 0), np.std(Z_plaq, axis = 0, ddof = 1)])

# Renormalize topological charge

# renorm_charge_clov = np.zeros((n_smear + 1, n_expectation_values))
# renorm_charge_plaq = np.zeros((n_smear + 1, n_expectation_values))

# renorm_charge_clov = np.around(Z_clov[0, :, np.newaxis] * top_charge_clov_raw)
renorm_charge_clov = Z_clov[0, :, np.newaxis] * top_charge_clov_raw
renorm_suscep_clov = renorm_charge_clov**2
# renorm_charge_plaq = np.around(Z_plaq[0, :, np.newaxis] * top_charge_plaq_raw)
renorm_charge_plaq = Z_plaq[0, :, np.newaxis] * top_charge_plaq_raw
renorm_suscep_plaq = renorm_charge_plaq**2

# Manually perform bootstrap

renorm_charge_clov_resampled = np.zeros((n_resample, n_smear + 1))
renorm_suscep_clov_resampled = np.zeros((n_resample, n_smear + 1))
renorm_charge_plaq_resampled = np.zeros((n_resample, n_smear + 1))
renorm_suscep_plaq_resampled = np.zeros((n_resample, n_smear + 1))

# Add 1 to bootstrap_count for Z_clov and Z_plaq, since we do not need the central sample here
print("Bootstrapping renormalized charges")
for bootstrap_count in range(n_resample):
    for smear_count in range(n_smear + 1):
        tmp_clov = np.around(Z_clov[bootstrap_count + 1, smear_count] * top_charge_clov_raw[smear_count, :])
        renorm_charge_clov_resampled[bootstrap_count, smear_count] = np.average(tmp_clov, weights = bootstrap_array[bootstrap_count])
        renorm_suscep_clov_resampled[bootstrap_count, smear_count] = np.average(tmp_clov**2, weights = bootstrap_array[bootstrap_count])
        #renorm_charge_clov_resampled[bootstrap_count, smear_count]**2
        tmp_plaq = np.around(Z_plaq[bootstrap_count + 1, smear_count] * top_charge_plaq_raw[smear_count, :])
        renorm_charge_plaq_resampled[bootstrap_count, smear_count] = np.average(tmp_plaq, weights = bootstrap_array[bootstrap_count])
        renorm_suscep_plaq_resampled[bootstrap_count, smear_count] = np.average(tmp_plaq**2, weights = bootstrap_array[bootstrap_count])
        #renorm_charge_plaq_resampled[bootstrap_count, smear_count]**2

np.save(os.path.join(raw_data_path, "renorm_charge_clov_resampled"), renorm_charge_clov_resampled)
np.save(os.path.join(raw_data_path, "renorm_suscep_clov_resampled"), renorm_suscep_clov_resampled)
np.save(os.path.join(raw_data_path, "renorm_charge_plaq_resampled"), renorm_charge_plaq_resampled)
np.save(os.path.join(raw_data_path, "renorm_suscep_plaq_resampled"), renorm_suscep_plaq_resampled)

# Final values

renorm_charge_clov_final[:, 0] = np.average(renorm_charge_clov, axis = 1)
renorm_suscep_clov_final[:, 0] = np.average(renorm_suscep_clov, axis = 1)
renorm_charge_plaq_final[:, 0] = np.average(renorm_charge_plaq, axis = 1)
renorm_suscep_plaq_final[:, 0] = np.average(renorm_suscep_plaq, axis = 1)

renorm_charge_clov_final[:, 1] = np.std(renorm_charge_clov_resampled, axis = 0, ddof = 1)
renorm_suscep_clov_final[:, 1] = np.std(renorm_suscep_clov_resampled, axis = 0, ddof = 1)
renorm_charge_plaq_final[:, 1] = np.std(renorm_charge_plaq_resampled, axis = 0, ddof = 1)
renorm_suscep_plaq_final[:, 1] = np.std(renorm_suscep_plaq_resampled, axis = 0, ddof = 1)

for smear_count in range(n_smear + 1):
    print("Plotting renormalized histograms for smearing level ", smear_count)
    # Plot histogram of renormalized topological charge defined via clover term

    scaling_factor = 0.05

    max_charge_clov = np.max(np.abs(Z_clov[0, smear_count] * top_charge_clov_raw[smear_count, :]))
    hist_bins_clov = np.arange(-max_charge_clov, max_charge_clov + (1.5 * scaling_factor), step = scaling_factor) - (0.5 * scaling_factor)

    plt.hist(Z_clov[0, smear_count] * top_charge_clov_raw[smear_count, :], bins = hist_bins_clov)
    plt.xlabel(r"$Q (n_{\textrm{smear}} =$ " + str(smear_count) + r"$)$")
    plt.ylabel("Frequency")
    plt.savefig(os.path.join(histogram_data_path, "histogram_renormalized_charge_clov_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    plt.close()

    # Plot histogram of renormalized topological charge defined via plaquette

    max_charge_plaq = np.max(np.abs(Z_plaq[0, smear_count] * top_charge_plaq_raw[smear_count, :]))
    hist_bins_plaq = np.arange(-max_charge_plaq, max_charge_plaq + (1.5 * scaling_factor), step = scaling_factor) - (0.5 * scaling_factor)

    plt.hist(Z_plaq[0, smear_count] * top_charge_plaq_raw[smear_count, :], bins = hist_bins_plaq)
    plt.xlabel(r"$Q (n_{\textrm{smear}} =$ " + str(smear_count) + r"$)$")
    plt.ylabel("Frequency")
    plt.savefig(os.path.join(histogram_data_path, "histogram_renormalized_charge_plaq_" + str(smear_count) + ".pdf"), bbox_inches = "tight")
    plt.close()

# Bootstrap for other simple observables

for smear_count in range(n_smear + 1):
    action_final[smear_count, :] = bootstrap(action_raw[smear_count, :], np.average(action_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    # plaquette_re_final[smear_count, :] = bootstrap(plaquette_re_raw[smear_count, :], np.average(plaquette_re_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    # plaquette_im_final[smear_count, :] = bootstrap(plaquette_im_raw[smear_count, :], np.average(plaquette_im_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    ploop_re_final[smear_count, :] = bootstrap(ploop_re_raw[smear_count, :], np.average(ploop_re_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    ploop_im_final[smear_count, :] = bootstrap(ploop_im_raw[smear_count, :], np.average(ploop_im_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    top_charge_clov_final[smear_count, :] = bootstrap(top_charge_clov_raw[smear_count, :], np.average(top_charge_clov_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    top_charge_plaq_final[smear_count, :] = bootstrap(top_charge_plaq_raw[smear_count, :], np.average(top_charge_plaq_raw[smear_count, :]), bootstrap_array, return_resampled = False)
    # #-----
    # renorm_charge_clov[smear_count, :], renorm_charge_clov_resampled = bootstrap(top_charge_clov_raw[smear_count, :], np.average(renorm_charge_clov[smear_count, :], weights = Z_clov), bootstrap_array, return_resampled = True)
    # renorm_charge_clov[smear_count, :], renorm_charge_clov_resampled = bootstrap(top_charge_clov_raw[smear_count, :], np.average(renorm_charge_clov[smear_count, :], weights = Z_clov), bootstrap_array, return_resampled = True)
    # top_suscept_final[smear_count, :], top_charge_squared_resampled = bootstrap(top_charge_squared_raw[smear_count, :], np.average(top_charge_squared_raw[smear_count, :]), bootstrap_array, return_resampled = True)

    # top_suscept_final[smear_count, :] = bootstrap(top_charge_squared_raw[smear_count] - (top_charge_raw**2).T, np.mean(top_charge_squared_raw) - np.mean(top_charge_raw)**2, bootstrap_array)
    # top_suscept_final[smear_count, 0] = top_charge_squared_final[smear_count, 0] - top_charge_final[smear_count, 0]**2
    # top_suscept_final[smear_count, 1] = np.std(top_charge_squared_resampled - top_charge_resampled**2)

np.savetxt(os.path.join(analysis_path, "action_final.txt"), action_final, delimiter = ",")
# np.savetxt(os.path.join(analysis_path, "plaquette_re_final.txt"), plaquette_re_final, delimiter = ",")
# np.savetxt(os.path.join(analysis_path, "plaquette_im_final.txt"), plaquette_im_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "ploop_re_final.txt"), ploop_re_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "ploop_im_final.txt"), ploop_im_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "top_charge_clov_final.txt"), top_charge_clov_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "top_charge_plaq_final.txt"), top_charge_plaq_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "renormalized_charge_clov_final.txt"), renorm_charge_clov_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "renormalized_suscep_clov_final.txt"), renorm_suscep_clov_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "renormalized_charge_plaq_final.txt"), renorm_charge_plaq_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "renormalized_suscep_plaq_final.txt"), renorm_suscep_plaq_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "Z_clov.txt"), Z_clov, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "Z_plaq.txt"), Z_plaq, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "Z_clov_final.txt"), Z_clov_final, delimiter = ",")
np.savetxt(os.path.join(analysis_path, "Z_plaq_final.txt"), Z_plaq_final, delimiter = ",")

#-----
# Plot final results

# def plot_final(observable_array, file_name, observable_name, observable_unreweighted = None, ylim_lower = None, ylim_upper = None):
#     plt.fill_between(x = observable_array[:, 0], y1 = observable_array[:, 1] + observable_array[:, 2], y2 = observable_array[:, 1] - observable_array[:, 2], color = "#469FE5", alpha = 0.2)
#     if observable_unreweighted is not None:
#         #plt.errorbar(x = beta_list, y = observable_unreweighted[:,0], yerr = observable_unreweighted[:,1], fmt = "none", capsize = 1, linewidth = 0.2, color = "#469FE5", alpha = 0.7)
#         plt.errorbar(x = beta_list, y = observable_unreweighted[:,0], yerr = observable_unreweighted[:,1], fmt = "o", mfc = "none", markersize = 2, linewidth = 0.2, color = "#469FE5", alpha = 0.7)
#     plt.plot(observable_array[:, 0], observable_array[:, 1], color = "#469FE5", linewidth = 0.2)
#     plt.ylim(ylim_lower, ylim_upper)
#     plt.xlabel(r"$\beta$")
#     plt.ylabel(observable_name)
#     plt.savefig(os.path.join(reweighted_data_path, file_name + "_reweighted.pdf"), bbox_inches = "tight")
#     plt.close()

# plot_final(energy_reweighted_final, "energy", r"$E$", observable_unreweighted = energy_unreweighted_final)
# plot_final(energy_2_reweighted_final, "energy_2", r"$E^2$", observable_unreweighted = energy_2_unreweighted_final)
# plot_final(magnetization_reweighted_final, "magnetization", r"$m$", observable_unreweighted = magnetization_unreweighted_final, ylim_lower = 0, ylim_upper = 1)
# plot_final(magnetization_2_reweighted_final, "magnetization_2", r"$m^2$", observable_unreweighted = magnetization_2_unreweighted_final, ylim_lower = 0, ylim_upper = 1)
# plot_final(magnetization_4_reweighted_final, "magnetization_4", r"$m^4$", observable_unreweighted = magnetization_4_unreweighted_final, ylim_lower = 0, ylim_upper = 1)

#-----
# Correlation between action and absolute value of charge
# TODO: Include bootstrap to get error estimates

# correlation = np.corrcoef(action_raw, np.abs(top_charge_clov_raw), rowvar=False)

# # Version with symmetrization (this should work, but somehow it gives different results??)
# n_sector = 5
# action_sector = np.zeros((n_smear + 1, n_sector))
# for smear_count in range(n_smear + 1):
#     action_sector[smear_count, 0] = np.mean(action_raw[smear_count, np.where(np.abs(top_charge_clov_raw[-1,:]) < 0.5)])
#     for sector_count in range(1, n_sector):
#         action_sector[smear_count, sector_count] = np.mean(action_raw[np.where(np.abs(top_charge_clov_raw[-1,:]) >= (sector_count - 0.5)) and np.where(np.abs(top_charge_clov_raw[-1,:]) < (sector_count + 0.5)), smear_count])

# action_sector_relative = action_sector / action_sector[:, 0, np.newaxis]

# #-----
# # Plots
# # colormap = iter(cm.coolwarm(np.linspace(0, 1, n_subdirectories)))
# colormap = iter(cm.Blues(np.linspace(0.4, 1, n_smear + 1)))

# for smear_count in range(n_smear + 1):
#     current_color = next(colormap)
#     plt.scatter(np.arange(n_sector), action_sector[smear_count, :], label = r"$n_{\textrm{smear}}$ = " + str(smear_count), color = current_color)
# plt.xticks(np.arange(n_sector))
# plt.xlabel("$|Q|$")
# plt.ylabel("$S$")
# # plt.yscale("log")
# plt.legend()
# plt.savefig(os.path.join(histogram_data_path, "action_sector.pdf"))
# plt.close()

# colormap = iter(cm.Blues(np.linspace(0.4, 1, n_smear + 1)))

# plt.axhline(y = 1.0, color = "k", linestyle = "-", linewidth = 0.8, alpha = 0.5)
# for smear_count in range(n_smear + 1):
#     current_color = next(colormap)
#     plt.scatter(np.arange(n_sector), action_sector_relative[smear_count, :], label = r"$n_{\textrm{smear}}$ = " + str(smear_count), color = current_color)
# plt.xticks(np.arange(n_sector))
# plt.xlabel("$|Q|$")
# plt.ylabel(r"$S_{\textrm{relative}}$")
# plt.legend()
# plt.savefig(os.path.join(histogram_data_path, "action_sector_relative.pdf"))
# plt.close()

#-----
# Version without symmetrization (seems to be more in line with old results in 2112.05188)
# n_sector_min = np.round(np.min(top_charge_clov_raw, axis = 0))
# n_sector_max = np.round(np.max(top_charge_clov_raw, axis = 0))
n_sector_min = round(np.min(top_charge_clov_raw))
n_sector_max = round(np.max(top_charge_clov_raw))
sector_array = np.arange(n_sector_min, n_sector_max + 1)
n_sector = len(sector_array)
action_sector = np.zeros((n_smear + 1, len(sector_array)))
center_index = int(np.where(sector_array == 0)[0])

for smear_count in range(n_smear + 1):
    # DO NOT ITERATE OVER RANGE WITH NEGATIVE VALUE, OBVIOUSLY INCORRECT
    for sector_count in range(n_sector):
        # print(np.where(np.around(top_charge_clov_raw[-1,:]) == sector_array[sector_count]))
        action_sector[smear_count, sector_count] = np.mean(action_raw[smear_count, np.where(np.around(top_charge_clov_raw[-1,:]) == sector_array[sector_count])])

# TODO: How to find center where Q == 0?
action_sector_relative = action_sector / action_sector[:, center_index, np.newaxis]
# action_sector_relative = np.zeros_like(action_sector)
# for smear_count in range(n_smear + 1):
#     action_sector_relative[smear_count, :] = action_sector[smear_count, :] / action_sector[smear_count, center_index]

#-----
# Plots
colormap = iter(cm.Blues(np.linspace(0.4, 1, n_smear + 1)))

for smear_count in range(n_smear + 1):
    current_color = next(colormap)
    plt.scatter(np.arange(n_sector_min, n_sector_max + 1), action_sector[smear_count, :], label = r"$n_{\textrm{smear}}$ = " + str(smear_count), color = current_color)
plt.xticks(np.arange(n_sector_min, n_sector_max + 1))
plt.xlabel("$Q$")
plt.ylabel("$S$")
# plt.yscale("log")
plt.legend()
# plt.savefig("action_sector.pdf")
plt.savefig(os.path.join(histogram_data_path, "action_sector.pdf"))
plt.close()

colormap = iter(cm.Blues(np.linspace(0.4, 1, n_smear + 1)))

plt.axhline(y = 1.0, color = "k", linestyle = "-", linewidth = 0.8, alpha = 0.5)
for smear_count in range(n_smear + 1):
    current_color = next(colormap)
    plt.scatter(np.arange(n_sector_min, n_sector_max + 1), action_sector_relative[smear_count, :], label = r"$n_{\textrm{smear}}$ = " + str(smear_count), color = current_color)
plt.xticks(np.arange(n_sector_min, n_sector_max + 1))
plt.xlabel("$Q$")
plt.ylabel(r"$S_{\textrm{relative}}$")
plt.legend()
# plt.savefig("action_sector_relative.pdf")
plt.savefig(os.path.join(histogram_data_path, "action_sector_relative.pdf"))
plt.close()

#-----

end = time.time()
print("\n")
print("Total time:", end - start)
